#!/bin/sh

json_escape () {
    printf '%s' "$1" | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))'
}

AUTHOR=`git var GIT_COMMITTER_IDENT | sed 's/>.*/>/'`
MESSAGE=`git log -n 1 HEAD --format=format:%s%n%b`
MESSAGE=`json_escape "$MESSAGE"`
HASH=`git log -n 1 HEAD --format=format:%H`
JSON="{\"commit\": {\"author\": \"$AUTHOR\", \"message\": $MESSAGE, \"sha\": \"$HASH\"}}"

curl -X POST http://localhost:9000/git-hook/1-eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyIjo0ODc5LCJkIjoiMTY0Nzc1NjYxNTM5MyIsIm9pZCI6MSwidWlkIjoxLCJ0IjoiZ2l0LWhvb2siLCJpYXQiOjE2NDc3NTY2MTV9.kuMPbTc5WG3_DYt2SVsZUuesWR4UHfnzbBe45ANNQ1I \
   -H "Content-Type: application/json" \
   -H "User-Agent: SimpleGitHook" \
   -d "$JSON"  