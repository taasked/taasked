/// <reference types="@sveltejs/kit" />
declare namespace App {
    interface Session {
        user: {
            email: string;
            attributes: {
                dark: boolean;
                oid: number;
            };
            lang: string;
        };
        token: string;
        organization: any;
    }
    interface Locals {
        user: {
            email: string;
            attributes: {
                dark: boolean;
                oid: number;
            };
            lang: string;
        };
        token: string;
    }
}
