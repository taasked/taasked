import { stringid } from '$lib/utils/sid';

/** @type {import('./[id]').RequestHandler} */
export async function get({ params }) {
    const nid = stringid.from(params.slug);

    const response = await fetch(`http://localhost:9000/public/fragment/${nid}`);
    if (!!response.ok) {
        return {
            body: { fragment: await response.json() }
        };
    }

    return {
        status: 404
    };
}
