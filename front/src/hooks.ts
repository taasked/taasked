import type { GetSession, Handle } from '@sveltejs/kit';
import * as cookie from 'cookie';

export const handle: Handle = async ({ event, resolve }) => {
    const cookies = cookie.parse(event.request.headers.get('cookie') || '');
    const tpuc = cookies.tpuc && Buffer.from(cookies.tpuc, 'base64').toString('utf8');
    const data = tpuc ? JSON.parse(tpuc) : null;
    if (!data) {
        return await resolve(event);
    }

    event.locals.token = data.token;
    delete data.token;
    event.locals.user = data;

    return await resolve(event);
};

export const getSession: GetSession = async (request) => {
    const locals: any = request.locals;

    return {
        user: locals.user && {
            email: locals.user.email,
            orgId: locals.user.orgId,
            darkMode: locals.user.darkMode,
            lang: locals.user.lang
        },
        token: locals.token,
        organization: {}
    };
};
