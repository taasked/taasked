import type { LoadInput, LoadOutput } from '@sveltejs/kit/types/internal';

export async function authGuard({ url, fetch, session, params, stuff }: LoadInput): Promise<LoadOutput> {
    if (session.user && session.user.attributes && session.user.attributes.oid && session.token) {
        return {};
    }

    return {
        status: 302,
        redirect: '/auth/signin?redirect=' + encodeURIComponent(url.pathname)
    };
}

export default {
    authGuard
};
