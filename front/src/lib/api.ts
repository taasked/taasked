import type { IAccess } from './interfaces/access';
import type { IFragments } from './interfaces/fragment';
import type { IOrganization } from './interfaces/organization';
import type { IUser } from './interfaces/user';
import { HttpStatus } from './utils/http-status';

const API_URL = 'http://api.taasked.test';

const getURL = () => {
    // if (browser) {
    //     return API_URL;
    // }

    return 'http://api.taasked.test';
};

const headers = (token: string | undefined = undefined): any => {
    const o = {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    };

    if (token) {
        o['Authorization'] = `Bearer ${token}`;
    }
    return o;
};

export interface NormalizedSet<T> {
    [uuid: number]: T;
}

export interface SigninNormalizedResponse {
    result: number;
    entities: {
        user: NormalizedSet<IUser>;
        organization: NormalizedSet<IOrganization>;
        access: NormalizedSet<IAccess>;
    };
}

export interface OrganizationNormalizedResponse {
    result: number;
    entities: {
        fragments: NormalizedSet<IFragments>;
        organization: NormalizedSet<IOrganization>;
    };
}

export class ApiResponse<T> {
    success: boolean = true;
    error: string | null = null;
    status: number;
    data: T;

    get ok(): boolean {
        return !this.error && !!this.success;
    }

    get down(): boolean {
        return this.status === 504;
    }
}

export class ApiErroneousResponse<T> extends ApiResponse<T> {
    constructor(error: string, status: number) {
        super();
        this.success = false;
        this.error = error;
        this.status = status;
    }
}

export class ApiSuccessResponse<T> extends ApiResponse<T> {
    constructor(data: any, status: number) {
        super();
        this.success = true;
        this.data = data;
        this.status = status;
    }
}

const apiGet = async <T>(endpoint: string, token?: string, _fetch: Function = fetch): Promise<ApiResponse<T>> => {
    return await _fetch(`${getURL()}/${endpoint}`, {
        method: 'GET',
        mode: 'cors',
        headers: headers(token)
        // credentials: 'include'
    })
        .catch((e) => {
            return new ApiErroneousResponse('Cant connect to server', 504);
        })
        .then(async (resp: Response) => {
            if (resp.ok) {
                let data = await resp.json();

                return new ApiSuccessResponse(data, resp.status);
            }

            return new ApiErroneousResponse('Server error', resp.status);
        });
};

const apiPost = async <T>(endpoint: string, token: string | undefined, data: any): Promise<ApiResponse<T>> => {
    return await fetch(`${getURL()}/${endpoint}`, {
        method: 'POST',
        mode: 'cors',
        headers: headers(token),
        body: JSON.stringify(data)
        // credentials: 'include'
    })
        .catch((reason) => {
            return new ApiErroneousResponse('Cant connect to server', 504);
        })
        .then(async (resp: Response) => {
            if (resp.ok) {
                let data = await resp.json();

                return new ApiSuccessResponse(data, resp.status);
            }
            return new ApiErroneousResponse('Server error', resp.status);
        });
};

const apiPut = async <T>(endpoint: string, token: string, data: any): Promise<ApiResponse<T>> => {
    return await fetch(`${getURL()}/${endpoint}`, {
        method: 'PUT',
        mode: 'cors',
        headers: headers(token),
        body: JSON.stringify(data)
        // credentials: 'include'
    })
        .catch((reason) => {
            return new ApiErroneousResponse('Cant connect to server', 504);
        })
        .then(async (resp: Response) => {
            if (resp.ok) {
                let data = await resp.json();

                return new ApiSuccessResponse(data, resp.status);
            }
            return new ApiErroneousResponse('Server error', resp.status);
        });
};

const apiPatch = async <T>(endpoint: string, token: string, data: any): Promise<ApiResponse<T>> => {
    return await fetch(`${getURL()}/${endpoint}`, {
        method: 'PATCH',
        mode: 'cors',
        headers: headers(token),
        body: JSON.stringify(data)
        // credentials: 'include'
    })
        .catch((reason) => {
            return new ApiErroneousResponse('Cant connect to server', 504);
        })
        .then(async (resp: Response) => {
            if (resp.ok) {
                let data = resp.status === HttpStatus._204_NO_CONTENT ? null : await resp.json();
                return new ApiSuccessResponse(data, resp.status);
            }
            return new ApiErroneousResponse('Server error', resp.status);
        });
};

const apiDel = async (endpoint: string, token: string): Promise<ApiResponse<boolean>> => {
    return await fetch(`${getURL()}/${endpoint}`, {
        method: 'DELETE',
        mode: 'cors',
        headers: headers(token)
        // credentials: 'include'
    })
        .catch((reason) => {
            return new ApiErroneousResponse('Cant connect to server', 504);
        })
        .then(async (resp: Response) => {
            if (resp.ok) {
                return new ApiSuccessResponse(true, resp.status);
            }
            return new ApiErroneousResponse('Server error', resp.status);
        });
};

export { apiGet, apiPost, apiPut, apiDel, apiPatch };
