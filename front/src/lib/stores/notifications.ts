import type { INotifications, NotificationType } from '$lib/interfaces/notification';
import { writable } from 'svelte/store';

const appNotifications = writable<INotifications>([]);

const resetNotifications = (): void => {
    appNotifications.update(() => []);
};

const notify = (text: string, type: NotificationType) => {
    appNotifications.update((n) => [...n, { text, type }]);
};

export { appNotifications, resetNotifications, notify };
