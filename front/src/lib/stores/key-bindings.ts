import { derived, writable } from 'svelte/store';

export enum KeyBindingCategories {
    Task = 0,
    Fragment = 1,
    FragmentContainer = 2,
    Any = 3,
    All = 10
}

export enum KeyBindingActions {
    ToggleTaskUrgentFlag = 'TOGGLE_TASK_URGENT_FLAG',
    ToggleTaskDoneFlag = 'TOGGLE_TASK_DONE_FLAG',
    TaskSave = 'TASK_SAVE',
    TaskCheckForDeletion = 'TASK_CHECK_FOR_DELETION',
    TaskListMoveUp = 'TASK_LIST_MOVE_UP',
    TaskListClipboardCopy = 'TASK_LIST_CLIPBOARD_COPY',
    ToggleArchiveFlag = 'TOGGLE_ARCHIVE_FLAG',
    TogglePublicFlag = 'TOGGLE_PUBLIC_FLAG',
    TaskListMoveDown = 'TASK_LIST_MOVE_DOWN',
    TaskListFocusPrevious = 'TASK_LIST_FOCUS_PREVIOUS',
    TaskListFocusNext = 'TASK_LIST_FOCUS_NEXT',
    TaskListAddAfter = 'TASK_LIST_ADD_AFTER',
    TaskListAddChild = 'TASK_LIST_ADD_CHILD',
    TaskSelectAll = 'TASK_SELECT_ALL',
    TaskSetAsDescendant = 'TASK_SET_AS_DESCENDANT',
    TaskSetAsAncestor = 'TASK_SET_AS_ANCESTOR',

    FocusDown = 'FOCUS_DOWN',
    FocusUp = 'FOCUS_UP',
    Reset = 'RESET',
    Delete = 'DELETE',
    Return = 'RETURN'
}

export interface IKeyBinding {
    key: string;
    alt?: string;
    action: KeyBindingActions;
    category: KeyBindingCategories;
}

export const defaultKeyBindings: IKeyBinding[] = [
    {
        key: 'cmd+u',
        alt: 'meta+u',
        action: KeyBindingActions.ToggleTaskUrgentFlag,
        category: KeyBindingCategories.Task
    },
    {
        key: 'cmd+enter',
        alt: 'meta+enter',
        action: KeyBindingActions.ToggleTaskDoneFlag,
        category: KeyBindingCategories.Task
    },
    {
        key: 'enter',
        action: KeyBindingActions.TaskSave,
        category: KeyBindingCategories.Task
    },
    {
        key: 'backspace',
        alt: 'cmd+backspace',
        action: KeyBindingActions.TaskCheckForDeletion,
        category: KeyBindingCategories.Task
    },
    {
        key: 'shift+arrowup',
        action: KeyBindingActions.TaskSelectAll,
        category: KeyBindingCategories.Task
    },
    {
        key: 'shift+arrowdown',
        action: KeyBindingActions.TaskSelectAll,
        category: KeyBindingCategories.Task
    },
    {
        key: 'enter',
        action: KeyBindingActions.Return,
        category: KeyBindingCategories.Any
    },
    {
        key: 'backspace',
        action: KeyBindingActions.Delete,
        category: KeyBindingCategories.Any
    },
    {
        key: 'escape',
        action: KeyBindingActions.Reset,
        category: KeyBindingCategories.Any
    },
    {
        key: 'arrowup',
        action: KeyBindingActions.FocusUp,
        category: KeyBindingCategories.Any
    },
    {
        key: 'arrowdown',
        action: KeyBindingActions.FocusDown,
        category: KeyBindingCategories.Any
    },
    {
        key: 'alt+arrowup',
        action: KeyBindingActions.TaskListMoveUp,
        category: KeyBindingCategories.Fragment
    },
    {
        key: 'alt+arrowdown',
        action: KeyBindingActions.TaskListMoveDown,
        category: KeyBindingCategories.Fragment
    },
    {
        key: 'shift+enter',
        action: KeyBindingActions.TaskListAddAfter,
        category: KeyBindingCategories.Fragment
    },
    {
        key: 'alt+shift+enter',
        action: KeyBindingActions.TaskListAddChild,
        category: KeyBindingCategories.Fragment
    },
    {
        key: 'tab',
        action: KeyBindingActions.TaskSetAsDescendant,
        category: KeyBindingCategories.Fragment
    },
    {
        key: 'shift+tab',
        action: KeyBindingActions.TaskSetAsAncestor,
        category: KeyBindingCategories.Fragment
    },
    {
        key: 'shift+cmd+e',
        action: KeyBindingActions.TaskListClipboardCopy,
        category: KeyBindingCategories.Fragment
    },
    {
        key: 'cmd+c',
        action: KeyBindingActions.TaskListClipboardCopy,
        category: KeyBindingCategories.FragmentContainer
    },
    {
        key: 'cmd+h',
        action: KeyBindingActions.ToggleArchiveFlag,
        category: KeyBindingCategories.FragmentContainer
    },
    {
        key: 'shift+cmd+u',
        action: KeyBindingActions.TogglePublicFlag,
        category: KeyBindingCategories.FragmentContainer
    }
];

export const keyBindings = writable<IKeyBinding[]>(defaultKeyBindings);

const getDerivedFunction = (category: KeyBindingCategories) => {
    return (bindings: IKeyBinding[]) => {
        const res = {};
        for (let binding of bindings.filter(
            (b) =>
                b.category === category ||
                b.category === KeyBindingCategories.Any ||
                category === KeyBindingCategories.All
        )) {
            res[binding.key] = binding;
            if (binding.alt) {
                res[binding.alt] = binding;
            }
        }
        return res;
    };
};

export const allKeyBindings = derived<typeof keyBindings, { [key: string]: IKeyBinding }>(
    keyBindings,
    getDerivedFunction(KeyBindingCategories.All)
);

export const taskKeyBindings = derived<typeof keyBindings, { [key: string]: IKeyBinding }>(
    keyBindings,
    getDerivedFunction(KeyBindingCategories.Task)
);

export const listKeyBindings = derived<typeof keyBindings, { [key: string]: IKeyBinding }>(
    keyBindings,
    getDerivedFunction(KeyBindingCategories.Fragment)
);

export const fragmentContainerKeyBindings = derived<typeof keyBindings, { [key: string]: IKeyBinding }>(
    keyBindings,
    getDerivedFunction(KeyBindingCategories.FragmentContainer)
);

export const anyKeyBindings = derived<typeof keyBindings, { [key: string]: IKeyBinding }>(
    keyBindings,
    getDerivedFunction(KeyBindingCategories.Any)
);

export const getBindingKey = (event: KeyboardEvent) => {
    if (!event.key) {
        return '';
    }
    let key = event.key.toLowerCase();
    if (event.metaKey || event.ctrlKey) {
        key = 'cmd+' + key;
    }
    if (event.altKey) {
        key = 'alt+' + key;
    }
    if (event.shiftKey) {
        key = 'shift+' + key;
    }
    return key;
};
