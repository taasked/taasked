import type { ISidebarState } from '$lib/interfaces/sidebar-state';
import { writable } from 'svelte/store';

export const sidebarState = writable<ISidebarState>({ opened: false });
