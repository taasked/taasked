import { DragStateOrigin, type IDragState } from '$lib/interfaces/drag-state';
import { writable } from 'svelte/store';

const initialAppDragState: IDragState = {
    origin: DragStateOrigin.None,
    dragging: null,
    hovering: null
};

const appDragState = writable(initialAppDragState);

const resetDragState = (): void => {
    appDragState.update(() => initialAppDragState);
};

export { initialAppDragState, appDragState, resetDragState };
