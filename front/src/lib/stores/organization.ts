import type { IOrganization } from '$lib/interfaces/organization';
import { writable } from 'svelte/store';

const organization = writable<IOrganization | null>(null);

const resetOrganization = (org: IOrganization | null): void => {
    organization.update(() => org);
};

export { organization, resetOrganization };
