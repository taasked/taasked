import type { IAccessSet } from '$lib/interfaces/access';
import type { IFragments } from '$lib/interfaces/fragment';
import type { IOrganization, IOrganizationSet } from '$lib/interfaces/organization';
import type { IUserSet } from '$lib/interfaces/user';
import type { Writable } from 'svelte/store';
import { derived, writable } from 'svelte/store';

const organizations = writable<IOrganizationSet>();
const users = writable<IUserSet>();
const accesses = writable<IAccessSet>();
const rootFragments = writable<IFragments>();

const personalOrganization = derived<[Writable<IAccessSet>, Writable<IOrganizationSet>], IOrganization>(
    [accesses, organizations],
    (stores) => {
        const $accesses = stores[0];
        const $organizations = stores[1];
        for (let id in $accesses) {
            if ($accesses[id].personal) {
                return $organizations[$accesses[id].orgId];
            }
        }
    }
);

export { users, organizations, accesses, rootFragments, personalOrganization };
