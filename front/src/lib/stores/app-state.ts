import type { IAppState } from '$lib/interfaces/app-state';
import { writable } from 'svelte/store';

const initialAppState: IAppState = {
    language: 'en',
    currentOrganization: null
};

const appState = writable(initialAppState);

const resetAppState = (): void => {
    appState.update(() => initialAppState);
};

const focusState = writable<number | null>(null);

export { appState, initialAppState, resetAppState, focusState };
