import { writable } from 'svelte/store';

interface IPasswordResetStore {
    code: string;
    token: string;
    password: string;
}

const emptyPasswordRequest: IPasswordResetStore = {
    code: '',
    token: '',
    password: ''
};

const passwordRequestStore = writable(emptyPasswordRequest);

const resetPasswordRequest = (): void => {
    passwordRequestStore.update(() => emptyPasswordRequest);
};

export { resetPasswordRequest, passwordRequestStore, emptyPasswordRequest };
