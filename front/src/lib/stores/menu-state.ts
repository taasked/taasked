import type { IMenuState } from '$lib/interfaces/menu-state';
import { writable } from 'svelte/store';

const APP_MENU_STATE_STORAGE_KEY = 'taa-menu';

const initialAppMenuState: IMenuState = {};

const appMenuState = writable(initialAppMenuState);

const resetMenuState = (): void => {
    if (typeof localStorage === 'undefined') {
        return;
    }
    const v = localStorage.getItem(APP_MENU_STATE_STORAGE_KEY);
    if (!!v) {
        appMenuState.set(JSON.parse(v));
    }
};

resetMenuState();

appMenuState.subscribe((state) => {
    if (typeof localStorage === 'undefined') {
        return;
    }
    localStorage.setItem(APP_MENU_STATE_STORAGE_KEY, JSON.stringify(state));
});

export { initialAppMenuState, appMenuState, resetMenuState };
