<script lang="ts">
    import { DragStateOrigin } from '$lib/interfaces/drag-state';
    import { fragmentFactory, refFragmentFactory } from '$lib/interfaces/factories/fragment';
    import {
        FragmentType,
        type IFragment,
        type IFragments,
        type ITaskFragmentAttributes
    } from '$lib/interfaces/fragment';
    import type { KeyboardSvelteEvent } from '$lib/interfaces/keyboard-svelte-event';
    import { focusState } from '$lib/stores/app-state';
    import { appDragState } from '$lib/stores/drag-state';
    import { getBindingKey, KeyBindingActions, listKeyBindings } from '$lib/stores/key-bindings';
    import { fragmenter } from '$lib/utils/fragmenter';
    import { markdown } from '$lib/utils/front/markdown';
    import { createEventDispatcher } from 'svelte';
    import Fragment from './Fragment.svelte';
    import FragmentSeparator from './FragmentSeparator.svelte';

    const dispatch = createEventDispatcher();

    export let id: number;
    export let autokey: string = '';
    export let level: number = 0;
    export let type: FragmentType;
    export let attributes: ITaskFragmentAttributes | any;
    export let orgId: number;
    export let rootId: number;
    export let parentId: number;
    export let fragments: IFragments;

    const saveFragment = async (event) => {
        const cid: number = event.detail.id;
        const fragment = fragments.find((f) => f.id === cid);

        if (!fragment) {
            return;
        }
        if (cid < 0) {
            fragments = await fragmenter.create(fragments, fragment);
        } else if (event.detail.attributes) {
            const attributes: any = event.detail.attributes;
            fragments = await fragmenter.update(fragments, fragment, attributes);
        }

        if (fragment.type === FragmentType.Task) {
            if (fragment.attributes.done || fragment.attributes.wontfix) {
                onDone();
            } else {
                onUndone();
            }
        }
    };

    const moveUp = async (id: number) => {
        fragments = fragmenter.moveup(fragments, id);
    };

    const moveDown = async (id: number) => {
        fragments = fragmenter.movedown(fragments, id);
    };

    const focus = async (event: CustomEvent) => {
        fragmenter.focus(fragments, fragmenter.get(fragments, id));
    };

    const focusUp = async (fromId: number) => {
        const f = fragmenter.previous(fragments, fromId);
        const l = !!f ? f.fragments.length : 0;

        if (!!f && l > 0) {
            $focusState = f.fragments[l - 1].id;
        } else if (!!f) {
            $focusState = f.id;
        } else if ($focusState !== id) {
            $focusState = id;
        } else {
            $focusState = rootId;
        }
    };

    const focusDown = async (fromId: number, fromChild: boolean = false) => {
        const current = fragmenter.get(fragments, fromId);
        const next = fragmenter.next(fragments, fromId);
        if (current.fragments.length && !fromChild) {
            $focusState = current.fragments[0].id;
        } else if (next) {
            $focusState = next.id;
        } else {
            dispatch('focusdown', { id });
        }
    };

    const remove = async (id: number) => {
        const fragment = fragmenter.get(fragments, id);
        const previous = fragmenter.previous(fragments, id);
        const next = fragmenter.next(fragments, id);
        if (!fragment) {
            return;
        }
        fragments = await fragmenter.focus(await fragmenter.remove(fragments, fragment), previous || next);
        dispatch('undone', { id });
    };
    const setAsDescendant = async (id: number) => {
        fragments = await fragmenter.setAsDescendant(fragments, id);
    };

    const setAsAncestor = async (id: number) => {
        if (parentId === null) {
            return;
        }

        const fragment = fragmenter.get(fragments, id);
        fragments = fragmenter.setAsAncestor(fragments, fragment);
        dispatch('unchild', { fragment });
    };

    const onKeyUp = async (event: KeyboardSvelteEvent) => {
        const key = getBindingKey(event.detail.event);
        const binding = $listKeyBindings[key];
        const id = event.detail.id;

        if (!!binding) {
            switch (binding.action) {
                case KeyBindingActions.TaskListMoveUp:
                    return moveUp(id);
                case KeyBindingActions.TaskListMoveDown:
                    return moveDown(id);
                case KeyBindingActions.FocusUp:
                    return focusUp(id);
                case KeyBindingActions.FocusDown:
                    return focusDown(id);
                case KeyBindingActions.Delete:
                    return remove(id);
                case KeyBindingActions.Return:
                    return onEnter(event);
                case KeyBindingActions.TaskSetAsDescendant:
                    return setAsDescendant(id);
                case KeyBindingActions.TaskSetAsAncestor:
                    return setAsAncestor(id);

                default:
                    console.warn(`Binding action ${binding.action} not implemented`);
            }
        }
    };

    const onHover = async (event: CustomEvent) => {
        fragments = await fragmenter.interact(fragments, fragmenter.siblings(fragments, event.detail.id));
    };

    const onFocus = async (event: CustomEvent) => {
        fragments = await fragmenter.interact(fragments, fragmenter.siblings(fragments, event.detail.id));
    };

    const onEnter = async (event: any) => {
        const id = event.detail.id;
        const index = fragments.findIndex((f) => f.id === id);
        const fragment = fragments[index];
        const isBefore = event.detail.lineLength > 0 && event.detail.char === 0;
        if (isBefore) {
            insertChild(index > 0 ? fragments[index - 1].id : null);
            event.preventDefault();
            event.stopPropagation();
        } else if (fragment.fragments.length) {
            unshiftInto(fragment);
            event.preventDefault();
            event.stopPropagation();
        } else if (id) {
            insertChild(id);
            event.preventDefault();
            event.stopPropagation();
        }
    };

    const onUnchild = async (event: CustomEvent) => {
        const fragment = event.detail.fragment;
        fragments = [...(await fragmenter.insert(id, fragments, fragment.parentId, fragment))];
        $focusState = fragment.id;
    };

    const insertChild = async (afterId: number | null) => {
        const newFragment = fragmentFactory(
            orgId,
            rootId,
            id,
            afterId ? fragments.find((f) => afterId === f.id) : null
        );
        fragments = [...(await fragmenter.insert(id, fragments, afterId, newFragment))];
        $focusState = newFragment.id;
    };

    const unshiftInto = async (parent: IFragment) => {
        const newFragment = fragmentFactory(orgId, rootId, parent.id, null);
        fragments = [...(await fragmenter.unshiftInto(fragments, parent, newFragment))];
        $focusState = newFragment.id;
    };

    const onPaste = async (event: CustomEvent) => {
        let afterId = event.detail.id;
        const lines = event.detail.lines;
        const newFragments = markdown.toFragmentList(orgId, id, lines);
        fragments = [...(await fragmenter.add(fragments, newFragments, afterId))];
    };

    const onDone = async () => {
        if (fragmenter.checkAllDone(fragments)) {
            if (!attributes.done) {
                attributes.done = true;
                attributes = attributes;
                fragmenter.ack(id, { attributes });
            }
            dispatch('done', { id });
        } else {
            dispatch('undone', { id });
        }
        fragments = fragments;
    };

    const onUndone = async () => {
        if (type === FragmentType.Task) {
            if (attributes.done) {
                attributes.done = false;
                attributes = attributes;
                fragmenter.ack(id, { attributes });
            }
        }
        dispatch('undone', { id });
        fragments = fragments;
    };

    const drop = async (event, fragment) => {
        if (($appDragState.origin = DragStateOrigin.Menu)) {
            const newFragment = refFragmentFactory(
                $appDragState.dragging,
                orgId,
                rootId,
                id,
                fragments.find((f) => fragment.id === f.id) || null
            );
            fragments = await fragmenter.insert(id, fragments, fragment.id, newFragment);
            $focusState = newFragment.id;
        }
    };
</script>

{#each fragments as fragment}
    <Fragment
        bind:content={fragment.content}
        bind:type={fragment.type}
        bind:key={fragment.key}
        bind:interactive={fragment.interactive}
        bind:parentId={fragment.parentId}
        bind:afterId={fragment.afterId}
        bind:id={fragment.id}
        bind:rootId={fragment.rootId}
        bind:orgId={fragment.orgId}
        bind:fragments={fragment.fragments}
        bind:attributes={fragment.attributes}
        bind:autokey
        {level}
        on:startdrag={(ev) => dispatch('startdrag', ev.detail)}
        on:unchild={onUnchild}
        on:done={onDone}
        on:undone={onUndone}
        on:paste={onPaste}
        on:enter={onEnter}
        on:keyup={onKeyUp}
        on:hover={onHover}
        on:focus={onFocus}
        on:focusup={focus}
        on:focusdown={(event) => focusDown(event.detail.id, true)}
        on:change={saveFragment}
    />
    <FragmentSeparator {level} on:drop={(e) => drop(e, fragment)} />
{/each}
