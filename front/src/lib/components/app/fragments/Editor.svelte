<script lang="ts">
    import { browser } from '$app/env';
    import { FragmentType, type ITaskFragmentAttributes } from '$lib/interfaces/fragment';
    import { focusState } from '$lib/stores/app-state';
    import { allKeyBindings, getBindingKey, KeyBindingActions } from '$lib/stores/key-bindings';
    import { fragmenter } from '$lib/utils/fragmenter';
    import { copy } from '$lib/utils/front/clipboard';
    import { markdown } from '$lib/utils/front/markdown';
    import { cloneWithout, sleep, trim } from '$lib/utils/utils';
    import factory from 'codemirror-ssr';
    import usePlaceholder from 'codemirror-ssr/addon/display/placeholder.js';
    import useOverlay from 'codemirror-ssr/addon/mode/overlay.js';
    import useGfm from 'codemirror-ssr/mode/gfm/gfm.js';
    import useMarkdown from 'codemirror-ssr/mode/markdown/markdown.js';
    import { afterUpdate, createEventDispatcher, onDestroy, onMount } from 'svelte';
    import CodeBlockContainer from './CodeBlockContainer.svelte';

    const dispatch = createEventDispatcher();
    let codemirror;

    if (browser) {
        codemirror = factory();
        usePlaceholder(codemirror);
        useOverlay(codemirror);
        useMarkdown(codemirror);
        useGfm(codemirror);
    }

    export let id: number;
    export let fullsized: boolean = true;
    export let type: FragmentType = FragmentType.Basic;
    export let interactive: boolean = false;
    export let fixed: boolean = false;
    export let autofocus: boolean = false;
    export let attributes: ITaskFragmentAttributes | any;
    export let content: string = `# This is a new document;

What to write?`;

    let editor: CodeMirror.EditorFromTextArea;
    let internal: boolean = false;
    let instanciated: boolean = false;
    let focused: boolean = false;

    $: sid = `frag-editor-${id}`;
    $: shouldFocus = $focusState === id;
    $: wontfix = type === FragmentType.Task && attributes.wontfix;
    $: iscode = type === FragmentType.Code;
    $: isurgent = type === FragmentType.Task && attributes.urgent;

    afterUpdate(async () => {
        if (fixed) {
            return;
        }

        if (shouldFocus) {
            interactive = true;
        }

        if (interactive && !instanciated) {
            instanciateCodeMirror();
        }

        if (!editor) {
            return;
        }

        if (!internal) {
            if (editor.getValue() !== content) {
                editor.setValue(content);
            }
        } else {
            content = editor.getValue().trim();
        }

        if (shouldFocus && !focused) {
            fragmenter.focusCodeMirror(id);
        }

        internal = false;
    });

    onMount(() => {
        if (fixed) {
            return;
        }

        if (!instanciated && interactive) {
            instanciateCodeMirror();
        }
        if (autofocus) {
            editor.focus();
        }
    });

    onDestroy(() => {
        interactive = false;
        if (fixed) {
            return;
        }

        if (typeof document !== 'undefined') {
            const el = document.getElementById(sid);
            if (!!el) {
                el.innerHTML = '';
            }
        }
    });

    const mouseenter = () => {
        if (fixed || instanciated) {
            return;
        }

        instanciateCodeMirror();
        dispatch('hover', { id });
    };

    const getSelectedRange = (expand: number = 0) => {
        if (!editor) {
            return { from: 0, to: 0 };
        }
        const from = cloneWithout(editor.getCursor('from'));
        const to = cloneWithout(editor.getCursor('to'));
        if (expand > 0) {
            from.ch = Math.max(0, from.ch - expand);
            to.ch = to.ch + expand;
        }
        return { from, to, length: to.ch - from.ch, text: editor.getRange(from, to) };
    };

    const replacerFactory = (char: string) => {
        const len = char.length;
        return () => {
            let range = getSelectedRange();
            let str = editor.getSelection();
            if (str.slice(0, len) === char && str.slice(-len) === char) {
                editor.replaceSelection(str.slice(len, -len));
                range.to.ch -= len * 2;
                editor.setSelection(range.from, range.to);
                return;
            }
            let extended = getSelectedRange(len);
            let extstr = extended.text;
            if (extstr.slice(0, len) === char && extstr.slice(-len) === char) {
                editor.replaceRange(str, extended.from, extended.to);
                extended.to.ch -= len * 2;
                editor.setSelection(extended.from, extended.to);
                return;
            }

            editor.replaceSelection(char + str + char, 'around');
        };
    };

    const bolden = replacerFactory('**');
    const emphasize = replacerFactory('_');
    const code = replacerFactory('`');

    const instanciateCodeMirror = async () => {
        if (!!editor || fixed) {
            return;
        }

        instanciated = true;
        const textarea = document.getElementById(sid) as HTMLTextAreaElement;
        editor = codemirror.fromTextArea(textarea, {
            lineWrapping: true,
            inputStyle: 'contenteditable',
            scrollbarStyle: null,
            tabSize: 8, // keep consistent with preview: https://developer.mozilla.org/en-US/docs/Web/CSS/tab-size#formal_definition
            indentUnit: 4, // nested ordered list does not work with 2 spaces

            mode: {
                name: 'gfm',
                tokenTypeOverrides: {
                    emoji: 'emoji'
                }
            },
            lineNumbers: false
        });
        // https://github.com/codemirror/CodeMirror/issues/2428#issuecomment-39315423
        // https://github.com/codemirror/CodeMirror/issues/988#issuecomment-392232020
        editor.addKeyMap({
            Tab: 'indentMore',
            'Shift-Tab': 'indentLess',
            'Ctrl-B': bolden,
            'Cmd-B': bolden,
            'Ctrl-I': emphasize,
            'Cmd-I': emphasize,
            'Ctrl-K': code,
            'Cmd-K': code
        });

        editor.on('focus', (cm, event) => {
            focused = true;
            if ($focusState !== id) {
                $focusState = id;
            }
        });

        editor.on('blur', (cm, event) => {
            if ($focusState === id) {
                $focusState = null;
            }
            dispatch('blur', { id });
            focused = false;
        });

        editor.on('keydown', (cm, event) => {
            const key = getBindingKey(event);
            const cursor = editor.getCursor();
            const line = cursor.line;
            const lineLength = editor.getLine(line).length;
            const lines = editor.lineCount() - 1;

            if (key === 'enter') {
                if (type === FragmentType.Code && line < lines) {
                    return;
                }
                event.preventDefault();
                return dispatch('enter', { event, id, line, lineLength, char: cursor.ch });
            }

            if (key === 'arrowup') {
                if (line === 0) {
                    event.preventDefault();
                    return dispatch('keyup', { event, id, line, lineLength, char: cursor.ch });
                }
                return;
            }

            if (key === 'arrowdown') {
                if (line >= lines) {
                    event.preventDefault();
                    return dispatch('keyup', { event, id, line, lineLength, char: cursor.ch });
                }
                return;
            }

            if (key === 'backspace') {
                if (line === 0 && cursor.ch === 0) {
                    if (type !== FragmentType.Basic) {
                        return dispatch('typed', { type: FragmentType.Basic });
                    }
                    return dispatch('keyup', { event, id, line, lineLength, char: cursor.ch });
                }
            }

            if (key.slice(-3) === 'tab' && line === 0 && type !== FragmentType.Basic) {
                event.preventDefault();
                event.stopPropagation();
                return dispatch('keyup', { event, id, line, lineLength, char: cursor.ch });
            }

            const binding = $allKeyBindings[key];

            if (key.indexOf('backspace') === -1 && !!binding && lineLength) {
                const selection = editor.getSelection();
                event.preventDefault();
                event.stopPropagation();
                if (binding.action === KeyBindingActions.TaskListClipboardCopy && !!selection) {
                    return copy(selection);
                }

                return dispatch('keyup', { event, id, line, lineLength, char: cursor.ch });
            }
        });
        editor.on('beforeChange', function (cm, changeObj) {
            // var typedNewLine =
            //     changeObj.origin == '+input' && typeof changeObj.text == 'object' && changeObj.text.join('') == '';
            // if (typedNewLine) {
            //     dispatch('enter', { id });
            //     return changeObj.cancel();
            // }

            var pastedNewLine =
                changeObj.origin == 'paste' && typeof changeObj.text == 'object' && changeObj.text.length > 1;

            // Multi line past!!!, what we do is:
            // - filter out empty lines
            // - paste only first line
            // - dispatch paste event for handling of
            if (pastedNewLine && type !== FragmentType.Code) {
                const lines = changeObj.text.filter((str) => !!str.trim());
                const newText = lines.splice(0, 1)[0];
                dispatch('paste', { id, lines });
                return changeObj.update(null, null, [newText]);
            }

            return null;
        });
        editor.on('change', (cm) => {
            internal = true;
            let value = cm.getValue();

            if (value.indexOf('o ') === 0 && type === FragmentType.Basic) {
                value = value.slice(2);
                cm.setValue(value);
                dispatch('typed', { type: FragmentType.Task });
            } else if (value.indexOf('```') === 0 && type === FragmentType.Basic) {
                value = value.slice(2);
                cm.setValue(value);
                dispatch('typed', { type: FragmentType.Code });
            } else if (value.indexOf('- ') === 0 && type === FragmentType.Basic) {
                value = value.slice(2).trim();
                cm.setValue(value);
                dispatch('typed', { type: FragmentType.Item });
            } else if (value.indexOf('~') === 0 && !attributes.wontfix && type === FragmentType.Task) {
                value = '~~' + trim(value.trim(), '~') + '~~';
                cm.setValue(value);

                dispatch('wontfix', { wontfix: true });
            } else if (
                type === FragmentType.Task &&
                attributes.wontfix &&
                (value.slice(0, 2) !== '~~' || value.slice(-2) !== '~~')
            ) {
                if (value.lastIndexOf('~~') === value.length - 2) {
                    value = trim(value, '~');
                    cm.setValue(value);
                }
                dispatch('wontfix', { wontfix: false });
            }

            content = value;
        });

        await sleep();
        editor.refresh();
    };
</script>

<div
    id="editor-container"
    class="marked min-h-[1.8rem] leading-6"
    class:w-full={fullsized}
    class:w-64={!fullsized}
    class:flex-none={!fullsized}
    class:taasked-urgent={isurgent && !wontfix}
    class:taasked-code={iscode}
    on:mouseenter={mouseenter}
>
    {#if type === FragmentType.Code}
        <CodeBlockContainer
            >{#if !browser || !instanciated}{@html markdown.parse(content.trim())}{/if}<textarea
                id={sid}
                bind:value={content}
                class="hidden "
            /></CodeBlockContainer
        >
    {:else}
        {#if !browser || !instanciated}<span class="pb-[0.3em]">{@html markdown.parse(content.trim())}</span>{/if}
        <textarea id={sid} bind:value={content} class="hidden " />
    {/if}
</div>

<style>
    @import 'codemirror-ssr/lib/codemirror.css';
</style>
