import { apiPatch, apiPost, apiPut } from '$lib/api';
import {
    FragmentType,
    type IFragment,
    type IFragments,
    type PartialFragmentOrVoidResponse
} from '$lib/interfaces/fragment';
import { cloneWithout, sleep } from './utils';

let apiToken: string;
let debug = true;

let log = (msg: string, o: any = undefined) => {
    if (debug) {
        if (typeof o !== undefined) {
            console.log(msg, o);
        } else {
            console.log(msg);
        }
    }
};

let ackKeys = ['archived', 'content', 'done', 'urgent', 'afterId', 'parentId', 'rootId'];

/**
 * Static "service" that manages fragments and fragments lists and sync them with the server.
 */
export const fragmenter = {
    /**
     * Initializes service w/ API token.
     *
     * @param token
     */
    init: (token: string) => {
        if (!!token) {
            apiToken = token;
        }
    },

    key_of: (key: IFragment | string): string => {
        if (typeof key !== 'string') {
            key = key.key;
        }
        if (!!key) {
            if (key.indexOf('-') > 1) {
                return key.replace('#', '').split('-')[0].toUpperCase();
            }
            return key.replace('#', '').toUpperCase();
        }
        return '';
    },

    /**
     * Notify changes of a fragment in a fragment list (i.e. replace reference to fragment by new reference)
     */
    notify: (fragments: IFragments, fragment: IFragment) => {
        if (!fragments) {
            return fragments;
        }

        const index = fragments.findIndex((frag) => frag.id === fragment.id);
        if (index > -1) {
            fragments[index] = fragment;
            return fragments;
        }

        for (let frag of fragments) {
            if (fragmenter.notify(frag.fragments, fragment)) {
                return fragments;
            }
        }

        return null;
    },

    /**
     * Notify patch changes of a fragment in a fragment list (i.e. replace reference to fragment by new reference)
     */
    patched: (fragments: IFragments, fragment: IFragment, attributes: any) => {
        if (!fragments) {
            return fragments;
        }

        const index = fragments.findIndex((frag) => frag.id === fragment.id);
        if (index > -1) {
            for (let key in attributes) {
                fragments[index][key] = attributes[key];
            }
            return fragments;
        }

        for (let frag of fragments) {
            if (fragmenter.patched(frag.fragments, fragment, attributes)) {
                return fragments;
            }
        }

        return null;
    },

    find: (fragment: IFragment, id: number): IFragment | null => {
        if (fragment.id === id) {
            return fragment;
        }
        for (let f of fragment.fragments) {
            let frag = fragmenter.find(f, id);
            if (!!frag) {
                return frag;
            }
        }
        return null;
    },

    finds: (fragments: IFragments, id: number): IFragment | null => {
        for (let f of fragments) {
            let frag = fragmenter.find(f, id);
            if (!!frag) {
                return frag;
            }
        }
        return null;
    },

    parent: (fragments: IFragments, fragment: IFragment, parent: IFragment | null = null): IFragment | null => {
        for (let f of fragments) {
            if (f.id === fragment.id) {
                return parent;
            } else {
                const p = fragmenter.parent(f.fragments, fragment, f);
                if (!!p) {
                    return p;
                }
            }
        }
        return null;
    },

    /**
     * Require one or more fragment to be ready for interactions
     * (i.e. in current implementation, it will instanciate the
     * code mirror instance dedicated to the fragment).
     *
     * @param fragments
     * @param interactives One or more fragment we want to require
     * interaction for. If an array of fragments is provided, none
     * will be focused. If a unique fragment instance is provided,
     * then the `focus` parameter will be applied.
     * @param focus
     * @returns
     */
    interact: async (
        fragments: IFragments,
        interactives: IFragment | IFragments,
        focus: boolean = true
    ): Promise<IFragments> => {
        if (Array.isArray(interactives)) {
            for (let f of interactives) {
                fragmenter.interact(fragments, f, false);
            }
            return fragments;
        }

        if (!!interactives && !interactives.interactive) {
            interactives.interactive = true;
            if (focus) {
                sleep();
                fragmenter.focusCodeMirror(interactives);
            }
        }
        return fragments;
    },

    /**
     * Focus on the code mirror instance dedicated to the fragment.
     * Suppose that the fragment has been set as interactive.
     *
     * @param fragment
     */
    focusCodeMirror: (fragment: number | IFragment) => {
        const id = typeof fragment === 'number' ? fragment : fragment.id;
        let el = document.querySelector(`#frag-editor-${id}`);
        // @ts-expect-error
        if (!!el && el.nextSibling && el.nextSibling.CodeMirror) {
            // @ts-expect-error
            el = el.nextSibling;

            const editor = (el as any).CodeMirror;

            editor.focus();
            editor.setCursor({ line: 0, ch: editor.getLine(0).length });
        } else {
            console.warn(
                `Fragment editor not found (#frag-editor-${typeof fragment === 'number' ? fragment : fragment.id})`
            );
        }
    },

    /**
     * Focus in a fragment, requires fragment to be interactive
     * if needed (if so there will be a slight delay before focus).
     *
     * @param fragments
     * @param fragment
     * @returns
     */
    focus: async (fragments: IFragments, fragment: IFragment | null, doSleep: boolean = true): Promise<IFragments> => {
        if (!fragment) {
            return fragments;
        }

        if (doSleep) {
            await sleep();
        }

        if (fragment.interactive) {
            fragmenter.focusCodeMirror(fragment);
        } else {
            fragmenter.interact(fragments, fragment);
        }
        return fragments;
    },

    /**
     * Get previous fragment in the list or null if fragment not found or first fragment.
     *
     * @param fragments
     * @param id
     * @returns
     */
    previous: (fragments: IFragments, id: number): IFragment | null => {
        const index = fragments.findIndex((f) => f.id === id);
        const f = fragments[index - 1];
        if (f && (f.type === FragmentType.Reference || f.type === FragmentType.Commit)) {
            return null;
        }

        return f || null;
    },

    /**
     * Get previous fragment in the list or null if fragment not found or first fragment.
     *
     * @param fragments
     * @param id
     * @returns
     */
    next: (fragments: IFragments, id: number): IFragment | null => {
        const index = fragments.findIndex((f) => f.id === id);
        const f = fragments[index + 1];
        if (f && (f.type === FragmentType.Reference || f.type === FragmentType.Commit)) {
            return null;
        }

        return f || null;
    },

    /**
     * Get siblings fragment in the list (previous and next). Returns an array of 0, 1 or 2 fragments
     * depending on the state of the list.
     *
     * @param fragments
     * @param id
     * @returns
     */
    siblings: (fragments: IFragments, id: number): IFragments => {
        const index = fragments.findIndex((f) => f.id === id);
        const result: IFragments = [];
        if (index > 0) {
            result.push(fragments[index - 1]);
        }
        if (fragments[index + 1]) {
            result.push(fragments[index + 1]);
        }
        return result;
    },

    /**
     * Get a fragment object in given list by id.
     *
     * @param fragments
     * @param id
     * @returns
     */
    get: (fragments: IFragments, id: number): IFragment | null => {
        const index = fragments.findIndex((f) => f.id === id);
        return fragments[index] || null;
    },

    /**
     * Recursively get a fragment object in given list by id.
     *
     * @param fragments
     * @param id
     * @returns
     */
    rget: (fragments: IFragments, id: number): IFragment | null => {
        const l: number = fragments.length;
        for (let i = 0; i < l; i += 1) {
            const f = fragments[i];
            if (f.id === id) {
                return f;
            }
            if (f.fragments.length) {
                const sf = fragmenter.rget(f.fragments, id);
                if (sf !== null) {
                    return sf;
                }
            }
        }
        return null;
    },

    /**
     * Sync fragment creation w/ server
     *
     * @param fragments
     * @param fragment
     * @returns `true` if request succeded, false otherwise
     */
    post: async (fragment: IFragment, token: string | undefined = undefined): Promise<boolean> => {
        const data = cloneWithout(fragment, 'id');
        if (data.afterId < 0) {
            data.afterId = null;
        }
        const response = await apiPost<IFragment>('fragment', token || apiToken, data);
        if (response.ok) {
            for (let k in response.data) {
                fragment[k] = response.data[k];
            }
            return true;
        } else {
            // @todo: handle error!!
        }
        return false;
    },

    /**
     * Sync fragment creation w/ server and return provided fragments.
     *
     * @param fragments
     * @param fragment
     * @returns Fragment list
     */
    create: async (fragments: IFragments, fragment: IFragment | any): Promise<IFragments> => {
        await fragmenter.post(fragment, apiToken);
        const next = fragmenter.next(fragments, fragment.id);
        if (!!next && next.id > 0) {
            return await fragmenter.update(fragments, next, { afterId: fragment.id });
        }
        return fragments;
    },

    /**
     * Update given attributes of the fragment and returns the fragment.
     *
     * @param fragment
     * @param attributes
     * @returns
     */
    updateHead: async (fragment: IFragment, attributes: any): Promise<IFragment> => {
        if (!attributes) {
            console.warn(`Required to update fragment with no attribute!!`, fragment);
            return fragment;
        }
        const keys = Object.keys(attributes);
        if (keys.length === 1 && ackKeys.includes(keys[0])) {
            const response = await apiPatch(`fragment/${fragment.id}`, apiToken, attributes);
            if (!response.ok) {
                // @todo: handle error!!
            }
            for (let key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    fragment[key] = attributes[key];
                }
            }
            return fragment;
        }
        const response = await apiPut<IFragment>(`fragment/${fragment.id}`, apiToken, attributes);
        if (!response.ok) {
            // TODO: handle error
        } else {
            for (let key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    fragment[key] = response.data[key];
                }
            }
        }
        return fragment;
    },

    /**
     * Update given attributes of the fragment included in the list and returns the fragment list.
     *
     * @param fragment
     * @param attributes
     * @returns
     */
    update: async (fragments: IFragments, fragment: IFragment, attributes: any): Promise<IFragments> => {
        if (!attributes) {
            console.warn(`Required to update fragment with no attribute!!`, fragment);
            return fragments;
        }
        if (!fragment) {
            console.warn(`Required to update fragment with no fragment!!`, fragment);
            return fragments;
        }
        const keys = Object.keys(attributes);
        if (keys.length === 1 && ackKeys.includes(keys[0])) {
            await fragmenter.acks(fragments, fragment, attributes);
            for (let key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    fragment[key] = attributes[key];
                }
            }
            return fragments;
        }
        if (fragment.id < 0) {
            console.warn(`Required to update fragment that doesnt exist yet!!`, fragment);
            return fragments;
        }

        const response = await apiPut<IFragment>(`fragment/${fragment.id}`, apiToken, attributes);
        if (response.ok) {
            for (let key in attributes) {
                if (attributes.hasOwnProperty(key)) {
                    fragment[key] = response.data[key];
                }
            }
            fragment.id = response.data.id;
        } else {
            // @todo: handle error!!
        }
        return fragments;
    },

    /**
     * Fast update (no body returned, only aknowledgment that request succeded)
     *
     * @param fragment
     * @param attributes
     * @returns
     */
    acks: async (fragments: IFragments, fragment: IFragment, attributes: any): Promise<IFragments> => {
        if (!attributes) {
            console.warn(`Required to update fragment with no attribute!!`, fragment);
            return fragments;
        }
        if (fragment.id < 0) {
            console.warn(`Required to update fragment that doesnt exist yet!!`, fragment);
            return fragments;
        }

        for (let key in attributes) {
            fragment[key] = attributes[key];
        }

        const response = await apiPatch<PartialFragmentOrVoidResponse>(`fragment/${fragment.id}`, apiToken, attributes);

        if (!response.ok) {
            // @todo: handle error!!
        } else if (!!response.data) {
            for (let key in response.data) {
                fragment[key] = response.data[key];
            }
        }

        return fragments;
    },

    /**
     * Fast update (no body returned, only aknowledgment that request succeded)
     *
     * @param fragment
     * @param attributes
     * @returns
     */
    ack: async (id: number, attributes: any): Promise<boolean> => {
        if (id < 0) {
            console.warn(`Required to update fragment that doesnt exist yet!!`, id);
            return false;
        }

        const response = await apiPatch(`fragment/${id}`, apiToken, attributes);
        if (!response.ok) {
            return false;
        }

        return true;
    },

    add: async (fragments: IFragments, newFragments: IFragments, afterId: number | null): Promise<IFragments> => {
        let index = fragments.findIndex((f) => f.id === afterId);
        index = index > -1 ? index : 0;
        for (let fragment of newFragments) {
            fragment.afterId = fragments[index].id;
            await fragmenter.post(fragment, apiToken);
            index += 1;
            fragments.splice(index, 0, fragment);
        }
        return fragments;
    },

    unshift: (fragment: IFragment, newFragment: IFragment): IFragment => {
        const next = fragment.fragments[0];
        if (!!next) {
            next.afterId = newFragment.id;
        }
        fragment.fragments.unshift(newFragment);
        return fragment;
    },

    remove: async (fragments: IFragments, fragment: IFragment): Promise<IFragments> => {
        if (!fragment) {
            return;
        }
        const id = fragment.id;
        const index = fragments.findIndex((f) => f.id === id);
        if (index === -1) {
            return fragments;
        }
        const previous = fragments[index - 1];
        const next = fragments[index + 1];
        fragment.archived = true;
        fragments.splice(index, 1);
        if (id > 0) {
            const response = await apiPut<IFragment>(`fragment/${id}`, apiToken, { archived: true });
            // TODO: handle error
        }
        if (!!next) {
            await fragmenter.acks(fragments, next, { afterId: !!previous ? previous.id : null });
        }
        return fragments;
    },

    setAsDescendant: async (fragments: IFragments, fragment: number | IFragment): Promise<IFragments> => {
        if (typeof fragment === 'number') {
            fragment = fragmenter.get(fragments, fragment);
        }

        const index = fragments.indexOf(fragment);
        if (index > -1) {
            fragments.splice(index, 1);
            const subfragments = fragments[index - 1].fragments;
            let afterId = null;
            if (subfragments[subfragments.length - 1]) {
                afterId = subfragments[subfragments.length - 1].id;
            }
            subfragments.push(fragment);
            if (fragment.id > 0) {
                await fragmenter.acks(subfragments, fragment, { parentId: fragments[index - 1].id, afterId });
            } else {
                await fragmenter.create(fragments, fragment);
            }
            if (fragments[index]) {
                await fragmenter.acks(fragments, fragments[index], {
                    afterId: fragments[index - 1] ? fragments[index - 1].id : null
                });
            }
        }

        return [...fragments];
    },

    setAsAncestor: (fragments: IFragments, fragment: number | IFragment): IFragments => {
        if (typeof fragment === 'number') {
            fragment = fragmenter.get(fragments, fragment);
        }
        // First step: remove fragment from list
        const index = fragments.indexOf(fragment);
        fragments.splice(index, 1);

        return fragments;
    },

    insert: async (
        parentId: number,
        fragments: IFragments,
        afterId: number,
        fragment: IFragment
    ): Promise<IFragments> => {
        // Find previous frag
        const index = !!afterId ? fragments.findIndex((t) => t.id === afterId) : -1;

        fragment.parentId = parentId;
        fragment.afterId = !!afterId && fragments[index] ? fragments[index].id : null;

        // Get current
        const current = fragments[index + 1];

        // Insert old child after frag
        fragments.splice(index + 1, 0, fragment);

        if (fragment.id > 0) {
            fragmenter.acks(fragments, fragment, { parentId, afterId: fragment.afterId });
        } else {
            await fragmenter.create(fragments, fragment);
        }

        if (!!current) {
            current.afterId = fragment.id;
            fragmenter.acks(fragments, current, { afterId: fragment.id });
        }

        return fragments;
    },

    unshiftInto: async (fragments: IFragments, parent: IFragment, fragment: IFragment): Promise<IFragments> => {
        fragment.parentId = parent.id;

        // Get current
        const next = parent.fragments[0];
        parent.fragments.unshift(fragment);
        if (fragment.id > 0) {
            fragmenter.acks(fragments, fragment, { parentId: parent.id, afterId: null });
        } else {
            await fragmenter.create(fragments, fragment);
        }

        if (!!next) {
            await fragmenter.acks(fragments, next, { afterId: fragment.id });
        }

        return fragments;
    },

    moveup: (fragments: IFragments, id: number): IFragments => {
        const index = fragments.findIndex((f) => f.id === id);
        const fragment = fragments[index];

        if (index > 0) {
            fragments[index] = fragments[index - 1];
            fragments[index - 1] = fragment;

            fragments[index].afterId = fragments[index - 1].id;
            fragmenter.ack(fragments[index].id, { afterId: fragments[index].afterId });

            fragments[index - 1].afterId = index > 1 ? fragments[index - 2].id : null;
            fragmenter.ack(fragments[index - 1].id, { afterId: fragments[index - 1].afterId });

            if (!!fragments[index + 1]) {
                fragments[index + 1].afterId = fragments[index].id;
                fragmenter.ack(fragments[index + 1].id, { afterId: fragments[index + 1].afterId });
            }
        }
        fragmenter.focus(fragments, fragmenter.get(fragments, id));

        return fragments;
    },

    movedown: (fragments: IFragments, id: number): IFragments => {
        const index = fragments.findIndex((f) => f.id === id);
        const fragment = fragments[index];

        if (index < fragments.length - 1) {
            fragments[index] = fragments[index + 1];
            fragments[index + 1] = fragment;

            fragments[index].afterId = index > 0 ? fragments[index - 1].id : null;
            fragmenter.ack(fragments[index].id, { afterId: fragments[index].afterId });

            fragments[index + 1].afterId = fragments[index].id;
            fragmenter.ack(fragments[index + 1].id, { afterId: fragments[index + 1].afterId });

            if (!!fragments[index + 2]) {
                fragments[index + 2].afterId = fragments[index + 1].id;
                fragmenter.ack(fragments[index + 2].id, { afterId: fragments[index + 2].afterId });
            }
        }
        fragmenter.focus(fragments, fragmenter.get(fragments, id));

        return fragments;
    },

    movein: async (fragments: IFragments, source: IFragment, target: IFragment): Promise<IFragments> => {
        if (!source || !target) {
            return fragments;
        }

        await fragmenter.unlist(fragments, source);
        await fragmenter.unshiftInto(fragments, target, source);

        return fragments;
    },

    unlist: async (fragments: IFragments, fragment: IFragment): Promise<IFragments> => {
        const index = fragments.indexOf(fragment);
        if (index > -1) {
            return await fragmenter.unlistFrom(fragments, fragment);
        }

        const parent = fragmenter.parent(fragments, fragment);
        await fragmenter.unlistFrom(parent.fragments, fragment);

        return fragments;
    },

    unlistFrom: async (fragments: IFragments, fragment: IFragment): Promise<IFragments> => {
        const index = fragments.indexOf(fragment);
        if (index > -1) {
            fragments.splice(index, 1);
            if (fragments[index]) {
                await fragmenter.ack(fragments[index].id, { afterId: index > 0 ? fragments[index - 1].id : null });
            }
        }
        return fragments;
    },

    checkAllAttributes: (fragments: IFragments, type: FragmentType, key: string, value: any): boolean => {
        for (let f of fragments) {
            if (f.type === type && f.attributes[key] !== value) {
                return false;
            }
            if (!fragmenter.checkAllAttributes(f.fragments, type, key, value)) {
                return false;
            }
        }
        return true;
    },

    checkAllDone: (fragments: IFragments): boolean => {
        for (let f of fragments) {
            if (f.type === FragmentType.Task && !(f.attributes.done || f.attributes.wontfix)) {
                return false;
            }
            if (!fragmenter.checkAllDone(f.fragments)) {
                return false;
            }
        }
        return true;
    },

    /**
     * Recursively get all fragments by type and returns them as a flat list
     */
    getAllByType: (
        fragments: IFragments,
        type: FragmentType,
        result: IFragments | undefined = undefined
    ): IFragments => {
        if (!result) {
            result = [];
        }
        for (let f of fragments) {
            if (f.type === type) {
                result.push(f);
            }
            fragmenter.getAllByType(f.fragments, type, result);
        }
        return result;
    }
};
