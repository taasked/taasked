import { fragmentFactory } from '$lib/interfaces/factories/fragment';
import { FragmentType, type IFragment, type IFragments } from '$lib/interfaces/fragment';
import DOMPurify from 'dompurify';
import { marked } from 'marked';

const renderer = {
    strong(text) {
        return '<strong>**' + text + '**</strong>';
    },
    em(text) {
        return '<em>_' + text + '_</em>';
    },
    codespan(text) {
        return '<code>`' + text + '`</code>';
    },
    del(text) {
        return '<del>~~' + text + '~~</del>';
    },
    link(href: string, title: string, text: string) {
        if (href === 'mailto:' + text) {
            return text;
        }
        return `<span class="cm-link">[${text}]</span><span class="cm-link cm-url">(${href})</span>`;
    },
    paragraph(text: string) {
        return text;
    },
    blockquote(quote: string) {
        return '<span class="cm-quote">>&nbsp;' + quote + '</quote>';
    },

    heading(text, level) {
        return `<h${level} class="font-bold">` + '#'.repeat(level) + ' ' + text + `</h${level}>`;
    },
    list(body: string, ordered: boolean, start: number) {
        const tag = ordered ? 'ol' : 'ul';
        return `<${tag}>${body}</${tag}>`;
    },
    listitem(text: string, task: boolean, checked: boolean) {
        return `<li>${text}</li>`;
    },
    code(code: string, infostring: string, escaped: boolean) {
        return `<pre><code>\`\`\`${infostring}
${code}
\`\`\`</code></pre>`;
    }
};

marked.use({ renderer });

export interface LineTreeNode {
    line: string;
    children: LineTreeNode[];
}

export const markdown = {
    // toLineTree: (src: string[], level: number = 0, indent: number | null = null): LineTreeNode[] => {
    //     if (indent === null) {
    //         indent = markdown.getIndentation(src);
    //     }
    //     let children = [];
    //     const list = [];

    //     for (let line of src) {
    //         let l = /^( {1,})/g.exec(line)[0].length / indent;
    //         if (l > level) {
    //             children.push(line);
    //         } else {
    //             if (children.length) {

    //             }
    //         }
    //     }
    // },
    getIndentation: (src: string[]) => {
        let inCodeBlock = false;
        let indentation: number | null = null;
        for (let line of src) {
            if (inCodeBlock && line === '```') {
                inCodeBlock = false;
                continue;
            }
            if (!inCodeBlock && line.indexOf('```') === 0) {
                inCodeBlock = true;
                continue;
            }
            if (!inCodeBlock && line.indexOf(' ') === 0) {
                if (indentation === null) {
                    return /^( {1,})/g.exec(line)[0].length;
                }
            }
        }
    },
    toFragment: (orgId: number, src: string): IFragment => {
        const fragment = fragmentFactory(orgId);
        fragment.content = src;
        return fragment;
    },
    toFragmentList: (orgId: number, parentId: number, src: string[]): IFragments => {
        const fragments: IFragments = [];
        for (let line of src) {
            fragments.push(fragmentFactory(orgId, parentId, parentId, null, line));
        }
        return fragments;
    },
    toFragmentHead: (orgId: number, src: string): IFragment => {
        const lines = src.split('\n').filter((l) => !!l);
        if (!lines.length) {
            return fragmentFactory(orgId);
        }
        const fragment: IFragment = markdown.toFragment(orgId, lines[0]);
        fragment.fragments = markdown.toFragmentList(orgId, fragment.id, lines.slice(1));
        return fragment;
    },
    /**
     * Parse a line of markdown text and renders into html
     *
     * @param src
     * @returns
     */
    inline: (src: string, options?: marked.MarkedOptions) => {
        return marked.parseInline(src, options).trim();
    },
    /**
     * Parse markdown text and renders into html
     *
     * @param src
     * @returns
     */
    parse: (src: string, options?: marked.MarkedOptions) => {
        return marked.parse(src, options);
    },

    /**
     * Removes all HTML tags
     *
     * @param src
     * @returns
     */
    purify: (src: string) => {
        return DOMPurify.sanitize(src, { ALLOWED_TAGS: ['br'] })
            .replace('&nbsp;', ' ')
            .replace('<br/>', '\n')
            .replace('<br>', '\n');
    },

    /**
     * Turns a fragment into a markdown Github flavoured fragment
     * @param fragment
     * @returns
     */
    taskToMarkdown(fragment: IFragment): string {
        return `${fragment.attributes.done ? '- [x]' : '- [ ]'} ${fragment.content.replace('&nbsp;', ' ').trim()}${
            fragment.attributes.urgent ? ' [!]' : ''
        }`;
    },

    /**
     * Turns an array of fragments into a markdown Github flavoured fragment list
     *
     * @param fragment
     * @returns
     */
    fragmentsToMarkdown(fragments: IFragments, tab: string = '    ', prefix: string = ''): string {
        let result = [];
        for (let fragment of fragments) {
            switch (fragment.type) {
                case FragmentType.Code:
                    result.push(`${fragment.content}`);
                    result.push(``);
                    break;
                case FragmentType.Item:
                    result.push(`${prefix}-   ${fragment.content}`);
                    break;
                case FragmentType.Basic:
                    result.push(`${prefix}${fragment.content}`);
                    break;
                case FragmentType.Task:
                    result.push(`${prefix}${markdown.taskToMarkdown(fragment)}`);
                    break;
            }

            if (fragment.fragments.length) {
                result.push(markdown.fragmentsToMarkdown(fragment.fragments, tab, prefix + tab));
            }
        }
        return result.join('\n');
    },

    /**
     * Turns a fragment list into a markdown Github flavoured fragment list
     *
     * @param fragment
     * @returns
     */
    fragmentRootToMarkdown(fragment: IFragment, tab: string = '  '): string {
        return `# ${fragment.content}

${markdown.fragmentsToMarkdown(fragment.fragments, tab)}`;
    }
};

export interface IImportedList {
    content: string;
    fragments: any[];
}
