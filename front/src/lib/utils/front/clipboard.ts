import { createFakeElement, select } from './content-editable';

/**
 * Put a string in the clipboard
 * @param str
 */
export const copy = (str: string): boolean => {
    const el = createFakeElement(str);
    document.body.appendChild(el);
    select(el);
    try {
        document.execCommand('copy');
        return true;
    } catch (e) {
        return false;
    }
};
