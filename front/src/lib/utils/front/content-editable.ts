import type { IFragment } from '$lib/interfaces/fragment';

export const _id = (id) => {
    return document.getElementById(id);
};

export const setCaretAtEndOf = (id, scrollTop: number | null = null) => {
    const element = _id(id);
    if (!element) {
        return;
    }

    const range = document.createRange();
    const sel = window.getSelection();
    range.selectNodeContents(element);
    range.collapse(false);
    sel.removeAllRanges();
    sel.addRange(range);
    element.focus();
    if (scrollTop !== null) {
        window.scrollTo({ top: scrollTop });
    }
    // @ts-expect-error, see https://developer.mozilla.org/fr/docs/Web/API/Element/scrollIntoViewIfNeeded
    else if (element.scrollIntoViewIfNeeded) {
        // @ts-expect-error
        element.scrollIntoViewIfNeeded(false);
    } else {
        element.scrollIntoView();
    }

    range.detach(); // optimization
};

export const getSelectionText = () => {
    let text = '';
    let activeEl = document.activeElement;
    let activeElTagName = activeEl ? activeEl.tagName.toLowerCase() : null;
    if (
        activeElTagName == 'textarea' ||
        (activeElTagName == 'input' &&
            // @ts-expect-error
            /^(?:text|search|password|tel|url)$/i.test(activeEl.type) &&
            // @ts-expect-error
            typeof activeEl.selectionStart == 'number')
    ) {
        // @ts-expect-error
        text = activeEl.value.slice(activeEl.selectionStart, activeEl.selectionEnd);
    } else if (window.getSelection) {
        text = window.getSelection().toString();
    }
    return text;
};

export const selectAllOf = (id) => {
    const element = _id(id);
    if (!element) {
        return;
    }

    const range = document.createRange();
    const sel = window.getSelection();
    range.selectNodeContents(element);
    sel.removeAllRanges();
    sel.addRange(range);
    element.focus();
    range.detach(); // optimization
};

export const select = (element: HTMLElement) => {
    if (element.nodeName === 'SELECT') {
        element.focus();
        return (element as HTMLSelectElement).value;
    }

    if (element.nodeName === 'INPUT' || element.nodeName === 'TEXTAREA') {
        return selectFromInput(element as HTMLInputElement);
    }

    if (element.hasAttribute('contenteditable')) {
        element.focus();
    }

    var selection = window.getSelection();
    var range = document.createRange();

    range.selectNodeContents(element);
    selection.removeAllRanges();
    selection.addRange(range);

    return toString();
};

export const selectFromInput = (element: HTMLInputElement | HTMLTextAreaElement) => {
    var isReadOnly = element.hasAttribute('readonly');

    if (!isReadOnly) {
        element.setAttribute('readonly', '');
    }

    element.select();
    element.setSelectionRange(0, element.value.length);

    if (!isReadOnly) {
        element.removeAttribute('readonly');
    }

    return element.value;
};

export const getPasteData = (event: ClipboardEvent) => {
    var text = '';
    //@ts-ignore
    if (event.clipboardData || event.originalEvent.clipboardData) {
        //@ts-ignore
        text = (event.originalEvent || event).clipboardData.getData('text/plain');
        //@ts-ignore
    } else if (window.clipboardData) {
        //@ts-ignore
        text = window.clipboardData.getData('Text');
    }
    return text;
};

export const preventableDownKeys = ['ArrowDown', 'ArrowUp', 'Enter', 'Shift', 'Tab'];

export const preventEvents = (event: KeyboardEvent) => {
    if ((event.metaKey && event.key === 'Meta') || preventableDownKeys.includes(event.key)) {
        event.preventDefault();
        event.stopPropagation();
    }
};

/**
 * Creates a fake textarea element with a value.
 * @param {String} value
 * @return {HTMLElement}
 */
export const createFakeElement = (value) => {
    const isRTL = document.documentElement.getAttribute('dir') === 'rtl';
    const fakeElement = document.createElement('textarea');
    // Prevent zooming on iOS
    fakeElement.style.fontSize = '12pt';
    // Reset box model
    fakeElement.style.border = '0';
    fakeElement.style.padding = '0';
    fakeElement.style.margin = '0';
    // Move element out of screen horizontally
    fakeElement.style.position = 'absolute';
    fakeElement.style[isRTL ? 'right' : 'left'] = '-29999px';
    // Move element to the same position vertically
    let yPosition = window.pageYOffset || document.documentElement.scrollTop;
    fakeElement.style.top = `${yPosition}px`;

    fakeElement.setAttribute('readonly', '');
    fakeElement.value = value;

    return fakeElement;
};
