export const scrollIntoView = (id: string) => {
    if (typeof document === 'undefined') {
        return;
    }
    const element = document.getElementById(id);
    if (!element) {
        return;
    }
    // @ts-expect-error
    if (element.scrollIntoViewIfNeeded) {
        // @ts-expect-error
        element.scrollIntoViewIfNeeded(false);
    } else {
        element.scrollIntoView();
    }
};
