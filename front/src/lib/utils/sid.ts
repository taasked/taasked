const _index = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

const _add = 10000;

export const stringid = {
    encode: (n: number): string => {
        n += _add;

        var ret = '';

        for (let i = Math.floor(Math.log(n) / Math.log(_index.length)); i >= 0; i -= 1) {
            ret = ret + _index[Math.floor(n / stringid.bcpow(_index.length, i)) % _index.length];
        }

        return ret.split('').reverse().join('');
    },

    decode: (s: string): number => {
        const str = s.split('').reverse();
        let ret = 0;

        for (let i = 0; i <= str.length - 1; i++) {
            ret = ret + _index.indexOf(str[i]) * stringid.bcpow(_index.length, str.length - 1 - i);
        }

        ret -= _add;

        return ret;
    },

    from: (slug: string): number => {
        if (!slug) {
            return 0;
        }
        return stringid.decode(slug.split('-').pop());
    },

    /**
     * Raise _a to the power _b
     **/
    bcpow: (_a: number, _b: number): number => {
        return Math.floor(Math.pow(_a, _b));
    }
};
