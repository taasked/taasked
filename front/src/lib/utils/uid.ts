let _uid = 0;
let _nuid = 0;

export const uid = () => {
    return (_uid += 1);
};

export const luid = () => {
    return _uid;
};

export const nuid = () => {
    return (_nuid -= 1);
};

export const lnuid = () => {
    return _nuid;
};
