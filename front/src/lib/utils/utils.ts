export const sleep = (ms: number = 50) => {
    return new Promise((fulfil) => setTimeout(fulfil, ms));
};

export const int = (str: string): number => parseInt(str, 10);

export const slugify = (str) => {
    return str
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '') // Remove accents
        .replace(/([^\w]+|\s+)/g, '-') // Replace space and other characters by hyphen
        .replace(/\-\-+/g, '-') // Replaces multiple hyphens by one hyphen
        .replace(/(^-+|-+$)/g, '') // Remove extra hyphens from beginning or end of the string
        .toLowerCase();
};

export const cloneWithout = (o: any, ...keys: string[]) => {
    const o2 = { ...o };
    for (let key of keys) {
        delete o2[key];
    }
    return o2;
};

export const trim = (str: string, char: string) => {
    var start = 0,
        end = str.length;

    while (start < end && str[start] === char) ++start;

    while (end > start && str[end - 1] === char) --end;

    return start > 0 || end < str.length ? str.substring(start, end) : str;
};
