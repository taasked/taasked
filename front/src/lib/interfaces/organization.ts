import type { IFragments } from './fragment';

export enum OrganizationType {
    Private = 1,
    Public = 2
}

export interface IOrganization {
    id: number;
    createdAt: string;
    updatedAt: string;
    name: string;
    type: OrganizationType;
    fragments: IFragments;
}

export interface IOrganizationSet {
    [id: number]: IOrganization;
}
