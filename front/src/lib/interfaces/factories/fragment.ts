import { nuid } from '$lib/utils/uid';
import { slugify } from '$lib/utils/utils';
import { FragmentType, type IFragment } from '../fragment';

export const listFragmentTypes = [FragmentType.Item, FragmentType.Task];

export const getNewFragmentType = (previous: IFragment | null = null): FragmentType => {
    if (!!previous && listFragmentTypes.indexOf(previous.type) > -1) {
        return previous.type;
    }
    return FragmentType.Basic;
};

export const refFragmentFactory = (
    references: IFragment,
    orgId: number,
    rootId: number | null = null,
    parentId: number | null = null,
    previous: IFragment | null = null
): IFragment => {
    const nid = nuid();

    return {
        id: nid,
        type: FragmentType.Reference,
        attributes: { ref: references.id },
        content: slugify(references.content),
        archived: false,
        public: false,
        parentId,
        afterId: !!previous && previous.id > 0 ? previous.id : null,
        rootId,
        orgId,
        fragments: [],
        interactive: true
    };
};

export const fragmentFactory = (
    orgId: number,
    rootId: number | null = null,
    parentId: number | null = null,
    previous: IFragment | null = null,
    content: string = ''
): IFragment => {
    const nid = nuid();
    const attributes: any = {};
    let type = getNewFragmentType(previous);

    // Check
    if (content && /^-\s+\[ \]\s+/g.test(content)) {
        type = FragmentType.Task;
        attributes.done = false;
        attributes.urgent = false;
        content = content.split('[ ]')[1].trim();
    } else if (content && /^-\s+\[[x]{1}\]\s+/g.test(content)) {
        type = FragmentType.Task;
        attributes.done = true;
        attributes.urgent = false;
        content = content.split('[x]')[1].trim();
    }

    return {
        id: nid,
        type,
        attributes,
        content,
        archived: false,
        public: false,
        parentId,
        afterId: !!previous && previous.id > 0 ? previous.id : null,
        rootId,
        orgId,
        fragments: [],
        interactive: true
    };
};
