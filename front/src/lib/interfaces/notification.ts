export enum NotificationType {
    Info = 0,
    Success = 1,
    Warning = 2,
    Error = 3
}

export interface INotification {
    text: string;
    type: NotificationType;
}

export type INotifications = INotification[];
