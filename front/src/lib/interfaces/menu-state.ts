import type { IFragment } from './fragment';

export interface IMenuState {
    [id: number]: { opened: boolean };
}
