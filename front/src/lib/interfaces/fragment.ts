export enum FragmentType {
    Basic = 'md', // A piece of markdown text
    Task = 'task', // A todo task
    Item = 'item', // A list item
    Code = 'code', // A code block
    Link = 'link', // An external link w/ preview
    Commit = 'cmit', // A reference to an external GIT commit
    Reference = 'ref' // A reference to a root fragment
}

export interface Verticals {
    keyword: string;
    score: number;
}

export interface IFragment {
    interactive?: boolean;
    id: number;
    content: string;
    key: string | null;
    attributes: {
        ref?: number;
        done?: boolean;
        wontfix?: boolean;
        urgent?: boolean;
    };
    type: FragmentType;
    archived: boolean;
    public: boolean;
    parentId: number | null;
    afterId: number | null;
    orgId: number;
    rootId: number;
    fragments?: IFragments;
    saving?: boolean;
    meta?: {
        completion?: number;
        total?: number;
    };
    /**
     * Percentage of completed tasks, null if no tasks in sub fragments
     */
    completion?: number | null;

    verticals?: Verticals[];
}

export type IFragments = IFragment[];

export interface ITaskFragmentAttributes {
    done: boolean;
    urgent: boolean;
}

export interface IFragmentTask {
    type: FragmentType.Task;
    attributes: ITaskFragmentAttributes;
}

export interface PartialFragment {
    key?: string;
}

export type PartialFragmentOrVoidResponse = PartialFragment | void;
