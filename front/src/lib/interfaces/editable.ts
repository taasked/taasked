import type { Writable, Readable, Updater } from 'svelte/store';

/** Writable interface for both updating and subscribing. */
export interface Editable<T> extends Readable<T> {
    /**
     * Set value and inform subscribers.
     * @param value to set
     */
    set(this: void, value: T): void;

    get(): T;
    /**
     * Update value using callback and inform subscribers.
     * @param updater callback
     */
    update(this: void, updater: Updater<T>): void;
    /**
     * Update value using callback and inform subscribers.
     * @param updater callback
     */
    edit(prop: string, value: any): void;
}
