export interface IAppState {
    language: 'en';
    currentOrganization: null | number;
}
