import type { IFragment } from './fragment';

export enum DragStateOrigin {
    None = 'none',
    Menu = 'menu',
    Editor = 'editor'
}

export interface IDragState {
    origin: DragStateOrigin;
    dragging: IFragment | null;
    hovering: IFragment | null;
}
