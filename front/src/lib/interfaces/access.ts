export interface IAccess {
    id: number;
    level: number;
    orgId: number;
    personal: boolean;
    createdAt: string;
    updatedAt: string;
    userId: number;
}

export interface IAccessSet {
    [id: number]: IAccess;
}

export interface IToken {
    token: string;
}
