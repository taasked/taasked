export interface IUser {
    id: number;
    email: string;
    createdAt: string;
    updatedAt: string;
    orgUsers: number[];
}

export interface IUserSet {
    [id: number]: IUser;
}

export interface IAuthenticatedUser {
    email: string;
    darkMode: boolean;
    lang: string;
    orgId: number;
}
