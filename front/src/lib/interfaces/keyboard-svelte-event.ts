export interface KeyboardSvelteEvent extends CustomEvent {
    detail: {
        event: KeyboardEvent;
        id: number;
    };
}
