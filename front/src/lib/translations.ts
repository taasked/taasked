import i18n from 'sveltekit-i18n';

export const config = {
    loaders: [
        {
            locale: 'en',
            key: 'common',
            loader: async () => (await import('./i18n/en/common.json')).default
        },
        {
            locale: 'en',
            key: 'validation',
            loader: async () => (await import('./i18n/en/validation.json')).default
        },
        {
            locale: 'en',
            key: 'home',
            loader: async () => (await import('./i18n/en/home.json')).default
        }
    ]
};

export const { t, locale, locales, loading, loadTranslations } = new i18n(config);
