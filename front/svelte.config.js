import adapter from '@sveltejs/adapter-node';
import preprocess from 'svelte-preprocess';
import host from 'vite-plugin-host';

/** @type {import('@sveltejs/kit').Config} */
const config = {
    // Consult https://github.com/sveltejs/svelte-preprocess
    // for more information about preprocessors
    preprocess: preprocess(),

    kit: {
        adapter: adapter({
            envPrefix: {
                host: 'TAASKED_HOST'
            },
            out: '/build'
        }),
        floc: false,
        trailingSlash: 'never',
        csp: {
          directives: {
            'script-src': ['self']
          }
        },
        files: {
            hooks: 'src/hooks'
        },
        vite: {
            plugins: [
                // others
                host()
            ],
        }
    }
};

export default config;
