module.exports = {
    mode: 'jit', // ⚠ Make sure to have this
    content: ['./src/**/*.svelte'],
    darkMode: 'class',
    theme: {
        extend: {
            fontFamily: {
                /* @see http://www.apaddedcell.com/sites/www.apaddedcell.com/files/fonts-article/final/index.html */
                'source-code': ['"Ubuntu Mono"', '"Lucida Console"', '"Andale Mono"', 'monospace']
            },

            // that is animation class
            animation: {
                'fade-in': 'fadeIn 0.2s ease-out',
                'fade-out': 'fadeOut 0.2s ease-out'
            },

            // that is actual animation
            keyframes: (theme) => ({
                fadeIn: {
                    '0%': { opacity: 0 },
                    '100%': { opacity: 1 }
                },
                fadeOut: {
                    '0%': { opacity: 1 },
                    '100%': { opacity: 0 }
                }
            }),

            colors: {
                clementine: {
                    DEFAULT: '#FF9900',
                    50: '#FFE2B8',
                    100: '#FFDAA3',
                    200: '#FFCA7A',
                    300: '#FFBA52',
                    400: '#FFA929',
                    500: '#FF9900',
                    600: '#C77700',
                    700: '#8F5600',
                    800: '#573400',
                    900: '#1F1200'
                },
                apple: {
                    DEFAULT: '#D9FFB1',
                    50: '#FBFFF6',
                    100: '#F7FFEE',
                    200: '#EFFFDF',
                    300: '#E8FFD0',
                    400: '#E0FFC0',
                    500: '#D9FFB1',
                    600: '#BEFF79',
                    700: '#A2FF41',
                    800: '#87FF09',
                    900: '#6AD000'
                },
                pacific: {
                    DEFAULT: '#00B4FF',
                    50: '#B8EAFF',
                    100: '#A3E4FF',
                    200: '#7AD8FF',
                    300: '#52CCFF',
                    400: '#29C0FF',
                    500: '#00B4FF',
                    600: '#008CC7',
                    700: '#00658F',
                    800: '#003D57',
                    900: '#00161F'
                },
                cheery: {
                    DEFAULT: '#A40802',
                    50: '#FD6661',
                    100: '#FD534C',
                    200: '#FC2C24',
                    300: '#F50C03',
                    400: '#CC0A02',
                    500: '#A40802',
                    600: '#7C0602',
                    700: '#530401',
                    800: '#2B0201',
                    900: '#030000'
                }
            }
        }
    }
};
