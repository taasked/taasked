export const WelcomeDocument = `

We're thrilled to have you around here.

What is taasked? It's a **fast**, **low friction** platform for **task management** and **markdown documents** edition.

`;
