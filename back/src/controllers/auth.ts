import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import jwt from 'jsonwebtoken';
import { Op, Sequelize } from 'sequelize';
import { env } from '../env';
import { FragmentType } from '../models/fragment';
import { OrgModel, OrgModelType } from '../models/org';
import { OrgUserModel, OrgUserModelLevel } from '../models/org-user';
import { SessionTokenModelAttributes } from '../models/session-token';
import { UserModel, UserModelCreationAttributes } from '../models/user';
import { AuthenticatedUserType } from '../routes/schemas/authenticated-user';
import {
    PasswordResetRequestResponseType,
    PasswordResetRequestType,
    PasswordResetResolveType
} from '../routes/schemas/password-reset';
import { Controller } from '../tools/controller';
import {
    ErroneousResponse,
    NotFoundResponse
} from '../tools/erroneous-response';
import { compare, hash } from '../tools/hash';
import { HttpStatus } from '../tools/http-status';
import { mail } from '../tools/mailer';
import { randomNumericCode } from '../tools/random-code';

let instance: AuthController;

const TAASKED_PRIVATE_COOKIE_NAME = env.TAASKED_PRIVATE_COOKIE_NAME!;
const TAASKED_PUBLIC_COOKIE_NAME = env.TAASKED_PUBLIC_COOKIE_NAME!;

/**
 * Basic authentication middleware
 *
 * - Check that request has an `Authorization` header providing a `Bearer` token
 * - Check that the corresponding token exists
 *
 * If any of these 2 conditions fails, a `401 Unauthorized` reply is issued.
 * Otherwise, properties `uid` (user ID) and `tid` (token ID) are set onto the
 * request object, and the request is allowed to be performed.
 *
 * @param request
 * @param reply
 * @returns Sent {@link FastifyReply} with `401` status if unauthorized, `void` otherwise
 */
export async function authenticate(
    request: FastifyRequest,
    reply: FastifyReply
) {
    let tokenFromHeader: SessionTokenModelAttributes | null = null;
    let tokenFromCookie: SessionTokenModelAttributes | null = null;

    // Auth header bearer key
    if (request.headers.hasOwnProperty('authorization')) {
        const strToken = request.headers['authorization']!.replace(
            'Bearer ',
            ''
        );
        tokenFromHeader = await instance.authenticateFromHeader(strToken);

        if (!tokenFromHeader) {
            return reply.status(HttpStatus._401_UNAUTHORIZED).send();
        }
    }

    // Cookie
    if (
        request.cookies.hasOwnProperty(TAASKED_PRIVATE_COOKIE_NAME) &&
        request.cookies.hasOwnProperty(TAASKED_PUBLIC_COOKIE_NAME)
    ) {
        const privateCookie = request.unsignCookie(
            request.cookies[TAASKED_PRIVATE_COOKIE_NAME]
        );
        const publicCookie = JSON.parse(
            Buffer.from(
                request.cookies[TAASKED_PUBLIC_COOKIE_NAME],
                'base64'
            ).toString('utf8')
        );

        if (
            !privateCookie ||
            !privateCookie.valid ||
            !privateCookie.value ||
            !publicCookie
        ) {
            return reply.status(HttpStatus._401_UNAUTHORIZED).send();
        }

        tokenFromCookie = await instance.authenticate(privateCookie.value);

        if (!tokenFromCookie) {
            return reply.status(HttpStatus._401_UNAUTHORIZED).send();
        }
    }

    if (!tokenFromCookie && !tokenFromHeader) {
        return reply.status(HttpStatus._401_UNAUTHORIZED).send();
    }

    if (
        tokenFromCookie &&
        tokenFromHeader &&
        tokenFromCookie.twoFAToken !== tokenFromHeader.twoFAToken
    ) {
        return reply.status(HttpStatus._401_UNAUTHORIZED).send();
    }

    const token = tokenFromHeader || tokenFromCookie;
    request.uid = token!.userId;
    request.tid = token!.id;
}

export class AuthController extends Controller {
    constructor(server: FastifyInstance) {
        super(server);
        instance = this;
    }

    /**
     * Get user including accesses and organizations
     */
    private async _get(userId: number): Promise<UserModel | null> {
        return await this.user.findOne({
            include: [
                {
                    model: OrgUserModel,
                    where: {
                        userId: userId
                    },
                    include: [OrgModel]
                }
            ],
            where: { id: userId }
        });
    }

    /**
     * Set session cookies
     */
    private _setCookies(
        reply: FastifyReply,
        token: SessionTokenModelAttributes,
        user: AuthenticatedUserType
    ) {
        reply.cookie(env.TAASKED_PRIVATE_COOKIE_NAME!, token.token, {
            path: '/',
            domain: '.taasked.test',
            maxAge: 20000000,
            signed: true,
            httpOnly: true
        });

        reply.cookie(
            env.TAASKED_PUBLIC_COOKIE_NAME!,
            Buffer.from(
                JSON.stringify({
                    email: user.email,
                    attributes: user.attributes,
                    lang: user.lang,
                    token: token.headerToken
                })
            ).toString('base64'),
            {
                path: '/',
                domain: '.taasked.test',
                maxAge: 20000000
            }
        );

        return user;
    }

    private _clearCookies(reply: FastifyReply) {
        reply.clearCookie(env.TAASKED_PUBLIC_COOKIE_NAME!, {
            path: '/',
            domain: '.taasked.test',
            maxAge: 20000000
        });
        reply.clearCookie(env.TAASKED_PRIVATE_COOKIE_NAME!, {
            path: '/',
            domain: '.taasked.test',
            maxAge: 20000000,
            signed: true,
            httpOnly: true
        });
    }

    private _getAuthenticatedUser(
        user: UserModel,
        token: string
    ): AuthenticatedUserType {
        const access = user.orgUsers?.find((access) => access.personal);
        const attributes = { ...user.attributes };
        if (!attributes.oid) {
            attributes.oid = access?.id || null;
        }

        return {
            email: user.email,
            token: token,
            attributes,
            lang: 'en'
        };
    }

    async access(
        userId: number,
        orgId: number
    ): Promise<OrgUserModel | ErroneousResponse> {
        const record = await this.team.findOne({ where: { userId, orgId } });
        if (!record) {
            return new NotFoundResponse('Access not found');
        }
        return record;
    }

    /**
     * Get first session token object by user ID
     *
     * @param token
     * @returns
     */
    async getByUserId(
        uid: number
    ): Promise<SessionTokenModelAttributes | null> {
        const record = await this.sess.findOne({ where: { userId: uid } });
        return !record ? null : await record.toJSON();
    }

    /**
     * Get a session token object by session token
     *
     * @param token
     * @returns
     */
    async authenticate(
        token: string
    ): Promise<SessionTokenModelAttributes | null> {
        const record = await this.sess.findOne({ where: { token } });
        return !record ? null : await record.toJSON();
    }

    /**
     * Get a session token object by header token
     *
     * @param token
     * @returns
     */
    async authenticateFromHeader(
        token: string
    ): Promise<SessionTokenModelAttributes | null> {
        const record = await this.sess.findOne({
            where: { headerToken: token }
        });
        return !record ? null : await record.toJSON();
    }

    /**
     * Registers a new user
     */
    async register(
        attributes: UserModelCreationAttributes,
        reply: FastifyReply
    ): Promise<AuthenticatedUserType | { token: string } | ErroneousResponse> {
        const existing = await this.user.findOne({
            where: { email: attributes.email }
        });
        if (existing) {
            return new ErroneousResponse(
                'User exists',
                HttpStatus._400_BAD_REQUEST
            );
        }

        let record: UserModel | null = await this.user.create(attributes);
        if (!record) {
            return new ErroneousResponse('User not created');
        }

        // Creates default private organization
        const org = await this.orgs.create({
            name: '',
            type: OrgModelType.Private
        });
        if (!org) {
            return new NotFoundResponse('Organization not created');
        }

        await org.addUser(record, {
            // @ts-ignore
            through: { level: OrgUserModelLevel.Owner, personal: true }
        });

        record = await this._get(record.id);
        if (!record) {
            return new ErroneousResponse('Server error');
        }

        // Creates default private organization
        const fragment = await this.frag.create({
            content: 'Welcome in taasked!',
            archived: false,
            public: false,
            orgId: org.id,
            type: FragmentType.Basic,
            attributes: {}
        });
        if (!fragment) {
            return new NotFoundResponse('Organization not created');
        }

        return await this.signin(attributes, reply);
    }

    /**
     * First step of two factor authentication
     */
    async signin(
        attributes: {
            email: string;
            password: string;
        },
        reply: FastifyReply
    ): Promise<{ token: string } | AuthenticatedUserType | ErroneousResponse> {
        // Get user corresponding to token
        const record = await this.user.findOne({
            include: [
                {
                    model: OrgUserModel,
                    include: [OrgModel]
                }
            ],
            where: { email: attributes.email }
        });
        if (!record) {
            return new ErroneousResponse(
                'Invalid credentials',
                HttpStatus._401_UNAUTHORIZED
            );
        }

        // Check password
        const valid = await compare(attributes.password, record.password);
        if (!valid) {
            return new ErroneousResponse(
                'Invalid credentials',
                HttpStatus._401_UNAUTHORIZED
            );
        }

        const user = await record.toJSON();
        const cookieUser =
            user.orgUsers
                .map((access: OrgUserModel) => `${access.id}-${access.orgId}`)
                .join(';') + user.id;

        // Generates new session credentials
        const data = {
            userId: record.id,
            token: jwt.sign(
                {
                    rand: Math.floor(Math.random() * 10000),
                    time: String(Date.now()),
                    u: cookieUser,
                    type: 'tok'
                },
                env.TAASKED_SESSION_SECRET as string
            ),
            twoFACode: randomNumericCode(),
            headerToken: jwt.sign(
                {
                    rand: Math.floor(Math.random() * 10000),
                    time: String(Date.now()),
                    u: cookieUser,
                    type: '2fa'
                },
                env.TAASKED_SESSION_SECRET as string
            ),
            twoFAToken: jwt.sign(
                {
                    rand: Math.floor(Math.random() * 10000),
                    time: String(Date.now()),
                    u: cookieUser,
                    type: '2fa'
                },
                env.TAASKED_SESSION_SECRET as string
            )
        };

        // Save new credentials
        const token = await this.sess.create(data);
        if (!token) {
            return new ErroneousResponse('Token not generated');
        }

        // In case 2 factor auth is activated, return first-factor `twoFAToken` token.
        // The `twoFACode` should be sent to the email address.
        if (user.twoFAEmail) {
            // TODO: send code by email
            return { token: data.twoFAToken };
        }

        const authUser = this._getAuthenticatedUser(user, token.headerToken);
        return this._setCookies(reply, token, authUser);
    }

    /**
     * Second step of two factor authentication
     */
    async twofa(
        attributes: {
            code: string;
            token: string;
        },
        reply: FastifyReply
    ): Promise<AuthenticatedUserType | ErroneousResponse> {
        // Get token
        const token = await this.sess.findOne({
            where: {
                twoFACode: attributes.code,
                twoFAToken: attributes.token
            }
        });

        if (!token) {
            return new ErroneousResponse(
                'Invalid credentials',
                HttpStatus._401_UNAUTHORIZED
            );
        }

        // Get user
        const record = await this._get(token.userId);
        if (!record) {
            return new ErroneousResponse(
                'Invalid credentials',
                HttpStatus._401_UNAUTHORIZED
            );
        }

        const authUser = this._getAuthenticatedUser(record, token.headerToken);
        return this._setCookies(reply, token, authUser);
    }

    async resetPasswordRequest(
        attributes: PasswordResetRequestType
    ): Promise<PasswordResetRequestResponseType | ErroneousResponse> {
        let code: string = randomNumericCode();
        let token: string = jwt.sign(
            {
                rand: Math.floor(Math.random() * 10000),
                time: String(Date.now()),
                attributes,
                type: 'password-reset-request'
            },
            env.TAASKED_SESSION_SECRET as string
        );

        const existing = await this.user.findOne({
            where: { email: attributes.email }
        });

        // User doesnt exist but we dont want to let this
        // info be public. Lets return a bogus useless token.
        if (!existing) {
            return { token };
        }

        // Delete any older password reset request
        this.pass.destroy({
            where: {
                [Op.or]: [
                    { userId: existing.id },
                    {
                        createdAt: {
                            [Op.lte]: Sequelize.literal(
                                "NOW() - (INTERVAL '20 MINUTE')"
                            )
                        }
                    }
                ]
            }
        });

        // User exists, create new password token
        const record = await this.pass.create({
            userId: existing.id,
            code,
            token
        });
        if (!record) {
            return new ErroneousResponse('Token not generated');
        }

        mail(existing.email, 'auth/password-reset-code', { code: record.code });

        return { token };
    }

    async resetPasswordResolve(
        attributes: PasswordResetResolveType,
        reply: FastifyReply
    ): Promise<{ token: string } | AuthenticatedUserType | ErroneousResponse> {
        const token = await this.pass.findOne({
            where: { code: attributes.code, token: attributes.token }
        });
        if (!token) {
            return new NotFoundResponse('Token not found');
        }

        // Delete any deletable password reset request
        this.pass.destroy({
            where: {
                [Op.or]: [
                    { userId: token.userId },
                    {
                        createdAt: {
                            [Op.lte]: Sequelize.literal(
                                "NOW() - (INTERVAL '20 MINUTE')"
                            )
                        }
                    }
                ]
            }
        });

        const user = await this._get(token.userId);
        if (!user) {
            return new NotFoundResponse('User not found');
        }
        user.password = await hash(attributes.password);
        if (!(await user.save())) {
            return new ErroneousResponse('User not updated');
        }
        return await this.signin(
            { email: user.email, password: attributes.password },
            reply
        );
    }

    /**
     * Signout: delete session token from database
     */
    async signout(
        tokenId: number,
        reply: FastifyReply
    ): Promise<boolean | ErroneousResponse> {
        const result = await this.sess.destroy({ where: { id: tokenId } });
        this._clearCookies(reply);

        if (result === 1) {
            return true;
        }

        return new ErroneousResponse('Unable to delete token');
    }
}
