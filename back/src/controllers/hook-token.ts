import jwt from 'jsonwebtoken';
import { Sequelize } from 'sequelize';
import { env } from '../env';
import { HookTokenModelAttributes } from '../models/hook-token';
import { Controller } from '../tools/controller';
import {
    BadRequestResponse,
    ErroneousResponse,
    NotFoundResponse
} from '../tools/erroneous-response';

export class HookTokenController extends Controller {
    async create(
        userId: number,
        orgId: number
    ): Promise<HookTokenModelAttributes | ErroneousResponse> {
        let token: string = jwt.sign(
            {
                r: Math.floor(Math.random() * 10000),
                d: String(Date.now()),
                oid: orgId,
                uid: userId,
                t: 'git-hook'
            },
            env.TAASKED_SESSION_SECRET as string
        );

        const record = await this.hookToken.create({
            userId,
            orgId,
            token,
            usedAt: null
        });

        if (!record) {
            return new ErroneousResponse('Fragment not created');
        }

        return record.toJSON();
    }

    async check(
        orgId: number,
        token: string
    ): Promise<boolean | ErroneousResponse> {
        if (isNaN(orgId) || !token) {
            return new BadRequestResponse('Token or org id invalid');
        }
        const record = await this.hookToken.update(
            {
                usedAt: Sequelize.fn('NOW')
            },
            {
                where: { orgId, token },
                returning: true
            }
        );

        if (record[0] !== 1) {
            return new NotFoundResponse();
        }

        return true;
    }
}
