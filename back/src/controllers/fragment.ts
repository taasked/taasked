import createDOMPurify from 'dompurify';
import { JSDOM } from 'jsdom';
import { Op, QueryTypes } from 'sequelize';
import {
    FragmentModel,
    FragmentModelAttributes,
    FragmentModelCreationAttributes,
    FragmentRelatives,
    FragmentType,
    PartialFragmentModelAttributes
} from '../models/fragment';
import { OrgModel } from '../models/org';
import { OrgUserModel } from '../models/org-user';
import { FragmentBulkEditionType } from '../routes/schemas/fragment';
import { analyse } from '../tools/analysis';
import { Controller } from '../tools/controller';
import {
    BadRequestResponse,
    ErroneousResponse,
    NotFoundResponse
} from '../tools/erroneous-response';
import {
    find,
    FragmentTree,
    propagateCompletions
} from '../tools/fragment-tree';
import { int, slugify } from '../tools/utils';

const window = new JSDOM('').window;
// @ts-expect-error
const DOMPurify = createDOMPurify(window);

export class FragmentController extends Controller {
    private _sanitize<
        T extends FragmentModelAttributes | FragmentModelCreationAttributes
    >(attributes: T): T | null {
        if (attributes.hasOwnProperty('content')) {
            attributes.content = DOMPurify.sanitize(attributes.content);
        }
        return attributes;
    }

    private async _relatives(
        rootId: number | null,
        parentId: number | null,
        afterId: number | null
    ): Promise<FragmentRelatives> {
        const ids: number[] = [];
        const result: FragmentRelatives = {
            root: null,
            parent: null,
            previous: null
        };
        if (rootId) {
            ids.push(rootId);
        }
        if (parentId) {
            ids.push(parentId);
        }
        if (afterId) {
            ids.push(afterId);
        }
        const records = await this.frag.findAll({ where: { id: ids } });
        for (let record of records) {
            if (record.id === rootId) {
                result.root = record;
            } else if (record.id === parentId) {
                result.parent = record;
            } else if (record.id === afterId) {
                result.previous = record;
            }
        }
        return result;
    }

    // This is definitely not elegant!
    private async _getSequence<
        T extends FragmentModelAttributes | FragmentModelCreationAttributes
    >(sanitized: T, record: FragmentModel): Promise<string> {
        const reg = /(#(?<key>[a-z]{2,3}))/gi.exec(sanitized.content);

        /// If provided in task content, get it
        if (!!reg && reg.length) {
            const key = reg.groups!.key;
            // And strip it
            const re = new RegExp(`\s{0,1}\#${key}[\-]{0,1}\s{0,1}`, 'gi');
            sanitized.content = sanitized.content.replace(re, ' ');
            return key;
        }

        // Get previous key or parent key or root key
        const relatives = await this._relatives(
            sanitized.hasOwnProperty('rootId')
                ? sanitized.rootId || null
                : record.rootId || null,
            sanitized.hasOwnProperty('parentId')
                ? sanitized.parentId || null
                : record.parentId || null,
            sanitized.hasOwnProperty('afterId')
                ? sanitized.afterId || null
                : record.afterId || null
        );
        const prevKey = relatives.previous
            ? relatives.previous.sequence()
            : null;
        if (prevKey) {
            return prevKey;
        }
        const parentKey = relatives.parent ? relatives.parent.sequence() : null;
        if (parentKey) {
            return parentKey;
        }
        const rootKey = relatives.root ? relatives.root.sequence() : null;
        if (rootKey) {
            return rootKey;
        }

        // Default key is org name first 3 chars
        return record.org!.name.slice(0, 3).toUpperCase();
    }

    /**
     * Get an existing fragment based on user id and fragment id.
     * Wont return any fragment the user doesn't have access to.
     *
     * @param userId User ID (to check organization access)
     * @param id Fragment ID
     * @returns Sequelize {@link FragmentModel} instance
     */
    private async _get(
        userId: number,
        id: number
    ): Promise<FragmentModel | null> {
        return await this.frag.findOne({
            where: { id },
            include: [
                {
                    model: OrgModel,
                    required: true,
                    include: [
                        {
                            model: OrgUserModel,
                            required: true,
                            where: {
                                userId
                            }
                        }
                    ]
                }
            ]
        });
    }

    /**
     * Get all existing fragments based on user id and root fragment id.
     * Wont return any fragment the user doesn't have access to.
     *
     * @param userId User ID (to check organization access)
     * @param rootId Fragment ID
     * @returns Sequelize {@link FragmentModel} instance
     */
    private async _all(
        userId: number,
        rootId: number
    ): Promise<FragmentModel[]> {
        return await this.frag.findAll({
            where: {
                [Op.or]: [
                    {
                        id: rootId
                    },
                    {
                        rootId
                    }
                ],
                archived: false
            },
            include: [
                {
                    model: OrgModel,
                    required: true,
                    include: [
                        {
                            model: OrgUserModel,
                            required: true,
                            where: {
                                userId
                            }
                        }
                    ]
                }
            ],
            order: [['createdAt', 'DESC']]
        });
    }

    /**
     * Create a new fragment
     *
     * @param userId User ID submitting the request to make sure user has the rights onto the fragment list/organization
     * @param attributes Fragment fields, supposed valided
     * @returns Raw fragment attributes or erroneous response
     */
    async create(
        userId: number,
        attributes: FragmentModelCreationAttributes
    ): Promise<FragmentModelAttributes | ErroneousResponse> {
        const sanitized = this._sanitize(attributes);
        if (!sanitized) {
            return new BadRequestResponse('Fragment invalid');
        }

        // Check access to organization
        const access = await this.server.auth.access(userId, attributes.orgId);
        if (access instanceof ErroneousResponse) {
            return access;
        }

        if (attributes.type === FragmentType.Task && !!attributes.content) {
            if (!sanitized.attributes) {
                sanitized.attributes = {};
            }
            const tag = await this.server.sequence.get(attributes.orgId);
            sanitized.key = tag;
        }

        const record = await this.frag.create({ ...sanitized });

        if (!record) {
            return new ErroneousResponse('Fragment not created');
        }

        return await record.toJSON();
    }

    /**
     * Get a fragment by user ID and by fragment ID
     *
     * @param userId User ID
     * @param id Fragment ID
     * @returns
     */
    async get(
        userId: number,
        id: number
    ): Promise<FragmentModelAttributes | ErroneousResponse> {
        const records = await this._all(userId, id);
        if (!records || !records.length) {
            return new NotFoundResponse('Fragment not found');
        }
        const tree = new FragmentTree(records, id);
        const res = tree.export();
        if (!res) {
            return new NotFoundResponse('Fragment not found');
        }

        res.verticals = analyse(res);
        return res;
    }
    /**
     * Get a public fragment by fragment ID
     *
     * @param id Fragment ID
     * @returns
     */
    async pub(
        id: number
    ): Promise<FragmentModelAttributes | ErroneousResponse> {
        const records = await this.frag.findAll({
            where: {
                public: true,
                archived: false,
                id
            },
            include: [{ model: FragmentModel, as: 'fragments' }],
            order: [['createdAt', 'DESC']]
        });
        if (!records || !records.length) {
            return new NotFoundResponse('Fragment not found');
        }
        const tree = new FragmentTree(records, id);
        const res = tree.export();
        if (!res) {
            return new NotFoundResponse('Fragment not found');
        }

        res.verticals = analyse(res);
        return res;
    }

    /**
     * Get a fragment by user ID (to check organization access) and by fragment ID
     *
     * @param userId User ID
     * @param id Fragment ID
     * @returns
     */
    async children(
        userId: number,
        id: number,
        archived: boolean = false
    ): Promise<FragmentModelAttributes[] | ErroneousResponse> {
        const records = await this.frag.findAll({
            where: {
                rootId: null,
                archived
            },
            include: [
                {
                    model: OrgModel,
                    required: true,
                    include: [
                        {
                            model: OrgUserModel,
                            required: true,
                            where: {
                                userId
                            }
                        }
                    ]
                }
            ],
            order: [['createdAt', 'DESC']]
        });
        if (!records) {
            return new NotFoundResponse('Fragment not found');
        }

        const tree = new FragmentTree(records);
        return await this.server.frag.completion(tree.flat(id));
    }

    async completion(
        fragments: FragmentModelAttributes[]
    ): Promise<FragmentModelAttributes[]> {
        if (!fragments.length) {
            return fragments;
        }

        const orgId: number = fragments[0].orgId;
        const query = `
            WITH tasks AS (
                SELECT (CASE WHEN (
                                        f.attributes->>'done'='true' OR 
                                        f.attributes->>'wontfix'='true')
                                THEN 1 ELSE 0 END) as done, 
                            f."rootId" as rid 
                    FROM fragments f
                    WHERE f.type='${FragmentType.Task}' 
                        AND f.archived IS FALSE
                        AND f."rootId" IS NOT NULL
                        AND (SELECT rf.archived FROM fragments rf WHERE rf.id = f."rootId") IS FALSE
                        AND f."orgId"= :orgId
            )
            SELECT rid as id,
                    sum(done) as done, 
                    count(rid) as total
                FROM tasks GROUP BY rid;
        `.replace(/\n/gi, '');
        const result: { id: number; done: string; total: string }[] =
            await this.db.query(query, {
                replacements: { orgId },
                type: QueryTypes.SELECT
            });
        for (let line of result) {
            const fragment = find(fragments, line.id);
            if (!!fragment) {
                const done = int(line.done);
                const total = int(line.total);

                fragment.meta = {
                    completion: Math.ceil((done * 100) / total),
                    done: done,
                    total: total
                };
            }
        }
        propagateCompletions(fragments);
        return fragments;
    }

    async updateAll(
        userId: number,
        request: FragmentBulkEditionType
    ): Promise<FragmentModelAttributes[] | ErroneousResponse> {
        // @TODO later: use sequelize bulkCreate to have a more efficient process
        const result: FragmentModelAttributes[] = [];
        for (let fragment of request.changes) {
            // @ts-expect-error
            const response = await this.update(userId, fragment.id, fragment);
            if (response instanceof ErroneousResponse) {
                return response;
            }
            result.push(response);
        }
        return result;
    }

    /**
     * Update a fragment faster without returning anything but the server-side changed/created fields
     *
     * @param userId User ID
     * @param id Fragment ID
     * @param attributes Attributes
     * @returns
     */
    async ack(
        userId: number,
        id: number,
        attributes: any
    ): Promise<PartialFragmentModelAttributes | void | ErroneousResponse> {
        const result = await this._update(userId, id, attributes);
        if (result instanceof ErroneousResponse) {
            return result;
        }
        const partial: PartialFragmentModelAttributes = {};
        if (result.key) {
            partial.key = result.key;
        }

        if (!!attributes.content && result.content !== attributes.content) {
            partial.content = result.content;
        }
        return partial;
    }
    /**
     * Update a fragment
     *
     * @param userId User ID
     * @param id Fragment ID
     * @param attributes Attributes
     * @returns
     */
    async update(
        userId: number,
        id: number,
        attributes: any
    ): Promise<FragmentModelAttributes | ErroneousResponse> {
        const result = await this._update(userId, id, attributes);
        return result instanceof ErroneousResponse ? result : result.toJSON();
    }

    private async _update(
        userId: number,
        id: number,
        attributes: any
    ): Promise<FragmentModel | ErroneousResponse> {
        const sanitized = this._sanitize(attributes);
        if (!sanitized) {
            return new BadRequestResponse('Fragment invalid');
        }

        let record = await this._get(userId, id);
        let tree: FragmentModel[] | null = null;

        if (!record) {
            return new NotFoundResponse('Fragment not found');
        }

        const error = this.validate([
            // Cant set fragment as a child of itself
            [
                sanitized.parentId === record.id,
                'Fragment parent update disallowed'
            ],
            // Cant set fragment position after itself
            [
                sanitized.afterId === record.id,
                'Fragment position update disallowed'
            ],
            // Cant archive a fragment that is already archived, and vice versa
            [
                sanitized.archived === record.archived,
                'Fragment archive status update disallowed'
            ]
        ]);

        if (!!error) {
            return error;
        }

        /// Validates parent and after ids
        // if (sanitized.afterId || sanitized.parentId) {
        //     const tree = new FragmentTree(
        //         await this._all(userId, record.rootId!)
        //     );
        //     // Validates that afterId is in list
        //     if (!!sanitized.afterId && !tree.contains(sanitized.afterId)) {
        //         return new BadRequestResponse(
        //             'After fragment is not part of the list'
        //         );
        //     }

        //     // Validates that parentId is in list
        //     if (!!sanitized.parentId && !tree.contains(sanitized.parentId)) {
        //         return new BadRequestResponse(
        //             'Parent fragment is not part of the list'
        //         );
        //     }
        // }

        // Set task key
        if (
            record.type === FragmentType.Task &&
            !!sanitized.content &&
            !record.key
        ) {
            if (!sanitized.attributes) {
                sanitized.attributes = {};
            }
            const key = await this._getSequence(sanitized, record);
            const keyId = await this.server.sequence.get(
                record.orgId,
                key.toUpperCase()
            );
            sanitized.key = keyId;
        }

        const hasContentUpdate =
            sanitized.hasOwnProperty('content') &&
            sanitized.content !== record.content;

        // Save
        record = await record.update(sanitized, { returning: true });

        /// Update references
        if (!!hasContentUpdate) {
            await this.frag.update(
                { content: slugify(record.content) },
                {
                    where: {
                        attributes: {
                            ref: record.id
                        }
                    }
                }
            );
        }

        return record;
    }

    async delete(uid: number, id: number): Promise<ErroneousResponse | void> {
        const record = await this._get(uid, id);
        if (!record) {
            return new NotFoundResponse('Fragment not found');
        }

        // Cant delete a fragment that is not archived
        if (!record.archived) {
            return new BadRequestResponse(
                'Cant delete a fragment that is not archived'
            );
        }

        return await record.destroy();
    }
}
