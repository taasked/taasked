import { Controller } from '../tools/controller';
import { ErroneousResponse } from '../tools/erroneous-response';

export class SequenceController extends Controller {
    async get(orgId: number, key: string = ''): Promise<string | null> {
        const record = await this.sequences.findOne({
            where: { oid: orgId, key }
        });

        if (!record) {
            await this.sequences.create({ oid: orgId, key, ouid: 1 });
            return this.format(key);
        }

        await record.increment({ ouid: 1 });

        return this.format(key, record.ouid);
    }

    format(key: string = '', id: number = 1): string {
        return !!key ? `#${key}-${id}` : `#${id}`;
    }
}
