import { FragmentModel } from '../models/fragment';
import {
    OrgModel,
    OrgModelAttributes,
    OrgModelCreationAttributes,
    OrgModelType
} from '../models/org';
import { OrgUserModel, OrgUserModelLevel } from '../models/org-user';
import { Controller } from '../tools/controller';
import {
    ErroneousResponse,
    NotFoundResponse
} from '../tools/erroneous-response';
import { FragmentTree } from '../tools/fragment-tree';

export class OrgController extends Controller {
    async create(
        userId: number,
        body: OrgModelCreationAttributes
    ): Promise<OrgModelAttributes | ErroneousResponse> {
        const user = await this.user.findOne({
            where: { id: userId }
        });
        if (!user) {
            return new NotFoundResponse('User not found');
        }
        const org = await this.orgs.create(body);
        if (!org) {
            return new NotFoundResponse('Organization not created');
        }

        await org.addUser(user, {
            // @ts-ignore
            through: {
                level: OrgUserModelLevel.Owner,
                personal: body.type === OrgModelType.Private
            }
        });

        return org.toJSON();
    }

    async get(
        userId: number,
        orgId: number,
        archived: boolean = false
    ): Promise<OrgModelAttributes | ErroneousResponse> {
        let access = await this.team.findOne({
            include: [OrgModel],
            where: {
                userId,
                orgId
            }
        });

        if (!access) {
            return new NotFoundResponse('Organization Access not found');
        }

        const record = await this.orgs.findOne({
            include: [
                {
                    model: FragmentModel,
                    where: { rootId: null, archived },
                    required: false,
                    attributes: [
                        'id',
                        'orgId',
                        'rootId',
                        'parentId',
                        'afterId',
                        'content',
                        'public',
                        'type'
                    ],
                    order: [['createdAt', 'ASC']]
                }
            ],
            where: {
                id: access.org!.id
            }
        });
        if (!record) {
            return new NotFoundResponse('Organization not found');
        }
        const data = record.toJSON();

        const list = new FragmentTree(record.fragments!);
        data.fragments = list.flat(null);
        await this.server.frag.completion(data.fragments);

        return data;
    }

    async getPersonal(
        userId: number
    ): Promise<OrgModelAttributes | ErroneousResponse> {
        let access = await this.team.findOne({
            include: [OrgModel],
            where: {
                userId,
                personal: true,
                level: OrgUserModelLevel.Owner
            }
        });

        if (!access || !access.org) {
            return new NotFoundResponse('Organization Access not found');
        }

        const record = await this.orgs.findOne({
            include: [{ model: FragmentModel, where: { parentId: null } }],
            where: {
                id: access.org!.id
            }
        });
        if (!record) {
            return new NotFoundResponse('Organization not found');
        }
        return record.toJSON();
    }

    async all(
        userId: number
    ): Promise<OrgModelAttributes[] | ErroneousResponse> {
        const records = await this.orgs.findAll({
            include: [
                { model: OrgUserModel, required: true, where: { userId } }
            ]
        });
        if (!records) {
            return new NotFoundResponse('Organizations not found');
        }
        return records.map((record) => record.toJSON());
    }
}
