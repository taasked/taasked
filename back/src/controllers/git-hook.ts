import {
    MarkTaskAsDoneActions,
    MarkTaskAsUndoneActions,
    MarkTaskAsWontfixActions,
    PublishFragmentActions,
    SetReferenceToFragmentActions,
    UnpublishFragmentActions
} from '../data/git-hook-verbs';
import { FragmentModel, FragmentType } from '../models/fragment';
import { OrgModel } from '../models/org';
import { Controller } from '../tools/controller';
import { GitCommitBasicInfos, GitHookHelper } from '../tools/git-hook.helper';

// @todo: properly log
export class GitHookController extends Controller {
    async apply(
        orgId: number,
        data: any,
        userAgent: string | undefined
    ): Promise<void> {
        if (Array.isArray(data)) {
            for (let item of data) {
                this.apply(orgId, item, userAgent);
            }
            return;
        }

        // Attempt get commit message
        const commit = GitHookHelper.extractCommitData(data, userAgent);
        if (!commit) {
            console.log('No commit found');
            return;
        }

        // Extract references
        const references = GitHookHelper.extractFragmentReferences(commit);
        for (let ref of references) {
            for (let key of ref.keys) {
                this.applyAction(orgId, commit, ref.action, key);
            }
        }
    }

    async applyAction(
        orgId: number,
        commit: GitCommitBasicInfos,
        action: string,
        key: string
    ): Promise<void> {
        console.log('Will attempt to', action, key, 'in org', orgId);

        const fragment = await this.frag.findOne({
            where: { orgId, key }
        });

        if (!fragment) {
            console.log('no fragment found');
            return;
        }

        let actionFound = false;

        if (MarkTaskAsDoneActions.includes(action)) {
            if (
                fragment.type === FragmentType.Task &&
                !fragment.attributes.done
            ) {
                fragment.attributes = { ...fragment.attributes, done: true };
                await fragment.save();
                actionFound = true;
            }
        }

        if (MarkTaskAsUndoneActions.includes(action)) {
            if (
                fragment.type === FragmentType.Task &&
                !!fragment.attributes.done
            ) {
                fragment.attributes = { ...fragment.attributes, done: false };
                await fragment.save();
                actionFound = true;
            }
        }

        if (MarkTaskAsWontfixActions.includes(action)) {
            if (
                fragment.type === FragmentType.Task &&
                !fragment.attributes.wontfix
            ) {
                fragment.content = `~~${fragment.content}~~`;
                fragment.attributes = { ...fragment.attributes, wontfix: true };
                await fragment.save();
                actionFound = true;
            }
        }

        if (PublishFragmentActions.includes(action)) {
            if (!fragment.public) {
                fragment.public = true;
                await fragment.save();
                actionFound = true;
            }
        } else if (UnpublishFragmentActions.includes(action)) {
            if (fragment.public) {
                fragment.public = false;
                await fragment.save();
                actionFound = true;
            }
        }

        if (SetReferenceToFragmentActions.includes(action)) {
            actionFound = true;
        }

        if (!!actionFound) {
            this.registerCommit(fragment, commit);
        } else {
            console.log('Nothing found to do with', orgId, key, action);
        }
    }

    async registerCommit(
        fragment: FragmentModel,
        commit: GitCommitBasicInfos
    ): Promise<boolean> {
        /**
         * Attempt find author
         */
        let userId = null;
        if (!!commit.author) {
            const user = await this.user.findOne({
                where: { email: commit.author },
                include: [
                    {
                        model: OrgModel,
                        required: true,
                        where: {
                            id: fragment.orgId
                        }
                    }
                ],
                attributes: ['id']
            });

            if (!!user) {
                userId = user.id;
            }
        }

        /**
         * Save commit into fragment
         */
        const record = await this.frag.create({
            orgId: fragment.orgId,
            rootId: fragment.rootId,
            parentId: fragment.id,
            content: commit.message,
            archived: false,
            public: false,
            userId,
            type: FragmentType.Commit,
            attributes: {
                sha: commit.sha,
                ts: commit.ts,
                url: commit.url
            }
        });

        return !!record;
    }
}
