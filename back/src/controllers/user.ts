import { OrgModel } from '../models/org';
import {
    IUserAttributes,
    IUserPatchedAttributes,
    UserModelAttributes
} from '../models/user';
import { Controller } from '../tools/controller';
import {
    ErroneousResponse,
    NotFoundResponse
} from '../tools/erroneous-response';

export class UserController extends Controller {
    async get(uid: number): Promise<UserModelAttributes | ErroneousResponse> {
        const record = await this.user.findOne({
            where: { id: uid },
            include: OrgModel
        });

        if (!record) {
            return new NotFoundResponse('User not found');
        }

        return await record.toJSON();
    }

    async patch(
        uid: number,
        attributes: IUserPatchedAttributes
    ): Promise<IUserAttributes | ErroneousResponse> {
        const record = await this.user.findOne({
            where: { id: uid }
        });

        if (!record) {
            return new NotFoundResponse('User not found');
        }

        attributes = {
            ...record.attributes,
            ...attributes
        };

        const result = await record.update({
            attributes: attributes as IUserAttributes
        });

        if (!result) {
            return new ErroneousResponse('Not updated');
        }

        return attributes as IUserAttributes;
    }
}
