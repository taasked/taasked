import { fastify, FastifyInstance } from 'fastify';
import cookie, { FastifyCookieOptions } from 'fastify-cookie';
import { AuthController } from './controllers/auth';
import { FragmentController } from './controllers/fragment';
import { GitHookController } from './controllers/git-hook';
import { HookTokenController } from './controllers/hook-token';
import { OrgController } from './controllers/org';
import { SequenceController } from './controllers/sequence';
import { UserController } from './controllers/user';
import { env } from './env';
import { fragmentModelFactory } from './models/fragment';
import { hookTokenModelFactory } from './models/hook-token';
import { orgModelFactory } from './models/org';
import { orgUserModelFactory } from './models/org-user';
import { passwordTokenModelFactory } from './models/password-token';
import { sequenceModelFactory } from './models/sequence';
import { sessionTokenModelFactory } from './models/session-token';
import { userModelFactory } from './models/user';
import { associationFactory } from './models/_associations';
import { db } from './tools/db';

export let instance: FastifyInstance | undefined;

export const appInstance = (): FastifyInstance => {
    return instance!;
};

const appFactory = (listen: boolean = true) => {
    const server = fastify({
        logger: env.TAASKED_DEBUG === 'true',
        // Some routes handles tokens that are larger than the default
        // 100 max params length
        maxParamLength: 256
    });

    instance = server;

    // Prepare request scoped variables
    server.decorateRequest('uid', null); // User ID
    server.decorateRequest('tid', null); // User session token ID

    // Initialize database config
    server.decorate('db', db(server));

    // Declare and initialize all models
    server.decorate('models', {
        user: userModelFactory(server),
        sess: sessionTokenModelFactory(server),
        orgs: orgModelFactory(server),
        team: orgUserModelFactory(server),
        pass: passwordTokenModelFactory(server),
        frag: fragmentModelFactory(server),
        hookToken: hookTokenModelFactory(server),
        sequences: sequenceModelFactory(server)
    });

    // Declare model associations
    associationFactory(server);

    // Instanciate controllers
    server.decorate('auth', new AuthController(server));
    server.decorate('user', new UserController(server));
    server.decorate('orgs', new OrgController(server));
    server.decorate('hookToken', new HookTokenController(server));
    server.decorate('hook', new GitHookController(server));
    server.decorate('frag', new FragmentController(server));
    server.decorate('sequence', new SequenceController(server));

    // Helmet (sets the right headers for security robustness)
    server.register(require('fastify-helmet'));
    // Handles CORS
    server.register(require('fastify-cors'), {
        credentials: false,
        origin: '*'
    });
    // Handles cookies
    server.register(cookie, {
        secret: env.TAASKED_COOKIE_SECRET
    } as FastifyCookieOptions);
    // CSRF protection
    server.register(require('fastify-csrf'), { cookieOpts: { signed: true } });

    // Declare routes
    server.register(require('./routes/ping'), { prefix: '/ping' });
    server.register(require('./routes/auth'), { prefix: '/auth' });
    server.register(require('./routes/user'), { prefix: '/user' });
    server.register(require('./routes/org'), { prefix: '/organization' });
    server.register(require('./routes/git-hook'), { prefix: '/git-hook' });
    server.register(require('./routes/fragment'), { prefix: '/fragment' });

    if (listen) {
        // Start server
        server.listen(env.TAASKED_SERVER_PORT!, (err, address) => {
            if (err) {
                server.log.error(err);
                process.exit(1);
            }
        });
    }

    return server;
};

export default appFactory;
