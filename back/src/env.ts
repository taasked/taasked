let __required = false;

const envFilename = (): string => {
    switch (process.env.NODE_ENV) {
        case 'test':
            return '.test.env';
        case 'docker':
            return '.docker.env';
        default:
            return '.env';
    }
};

if (!__required) {
    const path = envFilename();

    console.log('Using env file', process.env.NODE_ENV, path);

    require('dotenv').config({
        path
    });
    __required = true;
}

const REQUIRED_KEYS = ['TAASKED_SESSION_SECRET'];

export const env = process.env;
