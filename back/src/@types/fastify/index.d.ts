import 'fastify';
import { Sequelize } from 'sequelize/dist';
import { AuthController } from '../../controllers/auth';
import { FragmentController } from '../../controllers/fragment';
import { GitHookController } from '../../controllers/git-hook';
import { HookTokenController } from '../../controllers/hook-token';
import { OrgController } from '../../controllers/org';
import { SequenceController } from '../../controllers/sequence';
import { UserController } from '../../controllers/user';
import { FragmentModel } from '../../models/fragment';
import { HookTokenModel } from '../../models/hook-token';
import { OrgModel } from '../../models/org';
import { OrgUserModel } from '../../models/org-user';
import { PasswordTokenModel } from '../../models/password-token';
import { SequenceModel } from '../../models/sequence';
import { SessionTokenModel } from '../../models/session-token';
import { TaskModel } from '../../models/task';
import { UserModel } from '../../models/user';

declare module 'fastify' {
    export interface FastifyInstance<
        HttpServer = Server,
        HttpRequest = IncomingMessage,
        HttpResponse = ServerResponse
    > {
        auth: AuthController;
        user: UserController;
        orgs: OrgController;
        frag: FragmentController;
        hookToken: HookTokenController;
        hook: GitHookController;
        sequence: SequenceController;

        db: Sequelize;

        models: {
            user: typeof UserModel;
            sess: typeof SessionTokenModel;
            orgs: typeof OrgModel;
            team: typeof OrgUserModel;
            pass: typeof PasswordTokenModel;
            frag: typeof FragmentModel;
            sequences: typeof SequenceModel;
            hookToken: typeof HookTokenModel;
        };
    }

    export interface FastifyRequest<
        RouteGeneric extends RouteGenericInterface = RouteGenericInterface,
        RawServer extends RawServerBase = RawServerDefault,
        RawRequest extends RawRequestDefaultExpression<RawServer> = RawRequestDefaultExpression<RawServer>,
        ContextConfig = ContextConfigDefault
    > {
        uid: number | null;
        tid: number | null;
    }
}

export {};
