declare global {
    namespace NodeJS {
        interface ProcessEnv {
            NODE_ENV: 'development' | 'test' | 'docker' | 'production';
            TAASKED_DEBUG: string;
            TAASKED_DEBUG_SQL: string;
            TAASKED_SERVER_PORT: string;
            TAASKED_DB_HOST: string;
            TAASKED_DB_PORT: string;
            TAASKED_DB_USER: string;
            TAASKED_DB_PASS: string;
            TAASKED_DB_NAME: string;
            TAASKED_SESSION_SECRET: string;
            TAASKED_TOKEN_SECRET: string;
            TAASKED_COOKIE_SECRET: string;
            TAASKED_PRIVATE_COOKIE_NAME: string;
            TAASKED_PUBLIC_COOKIE_NAME: string;
            TAASKED_FROM_EMAIL: string;
            TAASKED_SENDGRID_API_KEY: string;
        }
    }
}

export {};
