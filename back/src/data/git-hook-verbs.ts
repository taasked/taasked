export const MarkTaskAsDoneActions: string[] = [
    'close',
    'closes',
    'closed',
    'closing',
    'fix',
    'fixed',
    'fixes',
    'fixing',
    'resolve',
    'resolves',
    'resolved',
    'resolving',
    'implement',
    'implements',
    'implemented',
    'implementing'
];

export const MarkTaskAsUndoneActions: string[] = [
    'open',
    'reopen',
    'reopens',
    'reopening'
];

export const MarkTaskAsWontfixActions: string[] = ['wontfix'];

export const SetReferenceToFragmentActions: string[] = [
    'addresses',
    're',
    'references',
    'ref',
    'refs',
    'see'
];

export const PublishFragmentActions: string[] = [
    'publish',
    'publishes',
    'publishing'
];

export const UnpublishFragmentActions: string[] = [
    'unpublish',
    'unpublishes',
    'unpublishing'
];
