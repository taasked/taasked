export const int = (str: string): number => parseInt(str, 10);

export const sortByKey = <T>(
    array: T[],
    key: string,
    asc: boolean = true
): T[] => {
    array.sort((a: T, b: T) => {
        // @ts-ignore
        if (a[key] < b[key]) {
            return asc ? -1 : 1;
        }
        // @ts-ignore
        if (a[key] > b[key]) {
            return asc ? 1 : -1;
        }
        return 0;
    });

    return array;
};

export const contains = <T>(arr: T[], value: T): boolean => {
    return arr.indexOf(value) > -1;
};

// @todo v2: put this in shared lib w/ front
export const slugify = (str: string) => {
    return str
        .normalize('NFD')
        .replace(/[\u0300-\u036f]/g, '') // Remove accents
        .replace(/([^\w]+|\s+)/g, '-') // Replace space and other characters by hyphen
        .replace(/\-\-+/g, '-') // Replaces multiple hyphens by one hyphen
        .replace(/(^-+|-+$)/g, '') // Remove extra hyphens from beginning or end of the string
        .toLowerCase();
};
