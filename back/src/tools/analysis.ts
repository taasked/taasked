// @ts-expect-error
import * as compendium from 'compendium-js';
import { FragmentModelAttributes, FragmentType } from '../models/fragment';
import { slugify, sortByKey } from './utils';

export const analyse = (
    list: FragmentModelAttributes
): { score: number; keyword: string }[] => {
    let str = list.content + '. ' + fragmentsToString(list.fragments!);

    const analyses = compendium.analyse(str);
    const keywords: {
        [keyword: string]: {
            score: number;
            keyword: string;
        };
    } = {};

    let previous: any;
    for (let analysis of analyses) {
        for (let token of analysis.tokens) {
            let norm = token.norm;
            if (
                (token.attr.is_noun || token.attr.is_verb) &&
                // Bug compendium: stuff such as ## and ** are returned as NOUN
                !/^[#$`_~*]+$/g.test(norm)
            ) {
                let score = 1;
                if (previous && previous.attr.is_noun) {
                    norm = previous.norm + ' ' + norm;
                    score = 2;
                }

                if (keywords.hasOwnProperty(norm)) {
                    keywords[norm].score += score;
                } else {
                    keywords[norm] = { score: 1, keyword: slugify(norm) };
                }
            }
            previous = token;
        }
    }

    return sortByKey(
        Object.values(keywords).filter((keyword) => keyword.score > 1),
        'score',
        false
    );
};

export const fragmentsToString = (
    fragments: FragmentModelAttributes[]
): string => {
    let str = '';
    for (let fragment of fragments) {
        if (fragment.type !== FragmentType.Code) {
            str += fragment.content + '. ';
        }
        if (fragment.fragments && fragment.fragments.length) {
            str += fragmentsToString(fragment.fragments);
        }
    }
    return str;
};
