import { FastifyInstance } from 'fastify';
import { Sequelize } from 'sequelize';
import { env } from '../env';

export const db = (server: FastifyInstance): Sequelize => {
    const DB_NAME = env.TAASKED_DB_NAME;
    const DB_HOST = env.TAASKED_DB_HOST;
    const DB_PORT = env.TAASKED_DB_PORT;
    const DB_USER = env.TAASKED_DB_USER;
    const DB_PASS = env.TAASKED_DB_PASS;
    const DB_URL = `postgres://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}`;

    server.log.info(`🟠 Will connect to database at 👉 ${DB_URL}`);

    return new Sequelize(DB_URL, {
        logging:
            env.TAASKED_DEBUG_SQL === 'true' && env.TAASKED_DEBUG === 'true'
                ? server.log.info.bind(server.log)
                : false
    });
};

export default db;
