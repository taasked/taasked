import { HttpStatus } from './http-status';

export class ErroneousResponse {
    status: HttpStatus;
    message: string;

    constructor(
        message: string = 'Invalid request',
        status: HttpStatus = HttpStatus._500_INTERNAL_SERVER_ERROR
    ) {
        this.message = message;
        this.status = status;
    }
}

export class NotFoundResponse extends ErroneousResponse {
    constructor(message: string = 'Not found') {
        super(message, HttpStatus._404_NOT_FOUND);
    }
}

export class BadRequestResponse extends ErroneousResponse {
    constructor(message: string = 'Bad request') {
        super(message, HttpStatus._400_BAD_REQUEST);
    }
}
