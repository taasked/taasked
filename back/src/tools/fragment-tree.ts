import { FragmentModelAttributes } from '../models/fragment';
import { contains } from './utils';

export interface IFragmentPosition {
    id: number;
    rootId: number | null;
    afterId: number | null;
    parentId: number | null;
    fragments?: IFragmentPosition[];
    toJSON?(): FragmentModelAttributes;
}

/**
 * Node in a {@link FragmentTree}
 */
export class FragmentTreeNode {
    fragment: IFragmentPosition;
    next: FragmentTreeNode | undefined;
    children: FragmentTreeNode[] = [];

    constructor(fragment: IFragmentPosition) {
        this.fragment = fragment;
    }

    /**
     * Get id of the underlying fragment
     */
    get id() {
        return this.fragment.id;
    }

    /**
     * Get afterId of the underlying fragment
     */
    get afterId() {
        return this.fragment.afterId;
    }

    /**
     * Get parentId of the underlying fragment
     */
    get parentId() {
        return this.fragment.parentId;
    }

    /**
     * Get a JSON serializable object representing the underlying fragment
     *
     * @returns The fragment attributes as a JSON-serializable object
     */
    toJSON(): FragmentModelAttributes {
        if (typeof this.fragment.toJSON === 'function') {
            return this.fragment.toJSON();
        }
        return this.fragment as unknown as FragmentModelAttributes;
    }
}

export const find = (
    fragmentTree: FragmentModelAttributes[],
    id: number
): FragmentModelAttributes | null => {
    for (let f of fragmentTree) {
        if (f.id === id) {
            return f;
        }
        const child = find(f.fragments!, id);
        if (!!child) {
            return child;
        }
    }
    return null;
};

export const propagateCompletion = (fragment: FragmentModelAttributes) => {
    let total = fragment.meta && fragment.meta.total ? fragment.meta.total : 0;
    let done = !!total ? fragment.meta.done : 0;
    if (fragment.fragments?.length) {
        const result = propagateCompletions(fragment.fragments);
        done += result.done;
        total += result.total;
    }
    fragment.meta = {
        done,
        total,
        completion: Math.ceil((done * 100) / total)
    };
};
export const propagateCompletions = (fragments: FragmentModelAttributes[]) => {
    let done = 0;
    let total = 0;

    for (let fragment of fragments) {
        propagateCompletion(fragment);
        if (fragment.meta && fragment.meta.total) {
            done += fragment.meta.done;
            total += fragment.meta.total;
        }
    }

    return { done, total };
};

/**
 * Helper class to generate fragment trees from flat fragment selections.
 */
export class FragmentTree<T extends IFragmentPosition> {
    nodes: { [id: number]: FragmentTreeNode } = {};
    head: FragmentTreeNode | undefined;

    constructor(fragments: T[], headId: number | undefined = undefined) {
        this._walk(fragments, headId);
        this._link();
    }

    private _walk(fragments: T[], headId: number | undefined = undefined) {
        for (let f of fragments) {
            const node = new FragmentTreeNode(f);
            if (node.id === headId) {
                this.head = node;
            }
            if (!!f.fragments && f.fragments.length) {
                this._walk(f.fragments as T[]);
            }
            this.nodes[node.id] = node;
        }
    }

    private _link() {
        for (let id in this.nodes) {
            const node = this.nodes[id];
            if (!!node.afterId && this.nodes.hasOwnProperty(node.afterId)) {
                this.nodes[node.afterId].next = node;
            }
            if (!!node.parentId && this.nodes.hasOwnProperty(node.parentId)) {
                this.nodes[node.parentId].children.push(node);
            }
        }
    }

    export(): FragmentModelAttributes | undefined {
        const ids: number[] = [];
        if (!!this.head) {
            return this.exportHead(this.head, ids);
        }
    }

    exportHead(head: FragmentTreeNode, ids: number[]): FragmentModelAttributes {
        const result = head.toJSON();
        ids.push(head.id);
        result.fragments = this.exportChildren(head, ids);
        return result;
    }

    exports(parentId: number | null = null): FragmentModelAttributes[] {
        const result: FragmentModelAttributes[] = [];
        const ids: number[] = [];
        for (let id in this.nodes) {
            const node = this.nodes[id];
            if (node.parentId === parentId) {
                result.push(this.exportHead(node, ids));
            }
        }
        return result;
    }

    flat(parentId: number | null = null): FragmentModelAttributes[] {
        let candidates = [];
        let ids = [];
        let result = [];

        for (let id in this.nodes) {
            if (
                this.nodes[id].parentId === parentId &&
                this.nodes[id].afterId === null
            ) {
                candidates.push(this.nodes[id]);
            }
        }

        if (candidates.length === 0) {
            return this.exports(parentId);
        }

        if (candidates.length > 1) {
            console.warn('Many heads found!');
        }

        let node: FragmentTreeNode | undefined = candidates[0];
        while (node) {
            ids.push(node.id);
            result.push(this.exportHead(node, ids));
            node = node.next;
        }

        for (let id in this.nodes) {
            if (
                this.nodes[id].parentId === parentId &&
                ids.indexOf(this.nodes[id].id) === -1
            ) {
                result.push(this.exportHead(this.nodes[id], ids));
            }
        }

        return result;
    }

    exportChildren(
        node: FragmentTreeNode,
        ids: number[] = []
    ): FragmentModelAttributes[] {
        const head = this.nodeHeadOf(node);
        const children = node.children;
        let result = this.exportFrom(head, ids);
        if (!!children) {
            for (let child of children) {
                if (!contains(ids, child.id)) {
                    result = result.concat(this.exportFrom(child, ids));
                    break;
                }
            }
            for (let child of children) {
                if (!contains(ids, child.id)) {
                    result.push(this.exportHead(child, ids));
                }
            }
        }
        return result;
    }

    exportFrom(
        head: FragmentTreeNode | undefined,
        ids: number[] = []
    ): FragmentModelAttributes[] {
        let result = [];
        const children = head?.children;
        while (head) {
            if (!contains(ids, head.id)) {
                const child = head.toJSON();
                child.fragments = this.exportChildren(head, ids);
                result.push(child);
                ids.push(child.id);
                head = head.next;
            } else {
                head = undefined;
            }
        }
        if (!!children) {
            for (let child of children) {
                if (!contains(ids, child.id)) {
                    result = result.concat(this.exportFrom(child, ids));
                    break;
                }
            }
        }
        return result;
    }

    contains(id: number): boolean {
        return this.nodes.hasOwnProperty(id);
    }

    headOf(id: number | undefined | null): FragmentTreeNode | null {
        if (!id) {
            return null;
        }

        if (!this.contains(id)) {
            return null;
        }

        const node = this.nodes[id];
        return this.nodeHeadOf(node);
    }

    nodeHeadOf(node: FragmentTreeNode): FragmentTreeNode {
        const nodes = node.children;

        for (let child of nodes) {
            if (!child.afterId) {
                return child;
            }
        }

        return nodes[0];
    }

    tailOf(id: number | undefined | null): FragmentTreeNode | null {
        if (!id) {
            return null;
        }

        if (!this.contains(id)) {
            return null;
        }

        const node = this.nodes[id];
        return this.nodeTailOf(node);
    }

    nodeTailOf(node: FragmentTreeNode): FragmentTreeNode {
        let head = this.nodeHeadOf(node);
        let ids: number[] = [];
        while (head && head.next) {
            if (ids.indexOf(head.id) > -1) {
                return head;
            }
            ids.push(head.id);
            head = head.next;
        }
        return head;
    }
}
