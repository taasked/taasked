export type GitCommitBasicInfos = {
    sha: string;
    url: string;
    ts: Date;
    message: string;
    /**
     * Author email,
     * if corresponds to a team member email, resulting commit fragment
     * will be affected to user
     */
    author: string;
};

export type GitMessageReference = { action: string; keys: string[] };

/**
 *
 */
export const GitReferencesRegexp =
    /([a-zA-Z]+)( +#(([a-z]{1,3}-){0,1}[0-9]+))+/gi;

export const GitHookHelper = {
    extractCommitData: (
        data: any,
        userAgent: string | undefined
    ): GitCommitBasicInfos | null => {
        if (!userAgent || userAgent === 'SimpleGitHook') {
            return GitHookHelper.extractSimpleCommitData(data);
        } else if (userAgent.indexOf('Gitlab') === 0) {
            return GitHookHelper.extractGitlabCommitData(data);
        }
        return null;
    },

    extractSimpleCommitData: (data: any): GitCommitBasicInfos | null => {
        if (!data || !data.hasOwnProperty('commit')) {
            return null;
        }
        const commit = data.commit;
        return {
            sha: commit.sha,
            message: commit.message,
            url: '',
            author: commit.author,
            ts: new Date()
        };
    },
    extractGitlabCommitData: (data: any): GitCommitBasicInfos | null => {
        if (!data) {
            return null;
        }
        if (!data.sha) {
            return null;
        }
        if (!data.commit) {
            return null;
        }

        const commit = data.commit;

        if (typeof commit.message !== 'string') {
            return null;
        }
        if (typeof commit.url !== 'string') {
            return null;
        }
        if (!commit.author || typeof commit.author.email !== 'string') {
            return null;
        }

        return {
            sha: data.sha,
            message: commit.message,
            ts: new Date(commit.author.date),
            url: commit.url,
            author: commit.author.email
        };
    },

    extractFragmentReferences: (
        commit: GitCommitBasicInfos
    ): GitMessageReference[] => {
        const matches = commit.message.match(GitReferencesRegexp);

        const results: GitMessageReference[] = [];
        if (!matches) {
            return results;
        }

        for (let match of matches) {
            const values = match.split(' #');
            const action = values[0].toLowerCase().trim();
            const keys = [];

            for (let i = 1; i < values.length; i += 1) {
                keys.push('#' + values[i]);
            }

            if (keys.length) {
                results.push({ keys, action });
            }
        }

        return results;
    }
};
