/**
 * Returns a random numeric code. In TEST env, returned code will always be `000000`
 *
 * @param len Lenght of code
 * @returns Code as a string as it may contains leading zeros
 */
export const randomNumericCode = (len: number = 6): string => {
    // @todo: Finally find the correct way to mockup that fuckin function when running jest tests
    if (process.env.NODE_ENV === 'test') {
        return '000000';
    }
    let code = '';
    while ((len -= 1) > -1) {
        code += Math.floor(Math.random() * 10).toString();
    }
    if (code === '000000') {
        return randomNumericCode(len);
    }

    return code;
};

export default randomNumericCode;
