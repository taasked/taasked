import { env } from '../env';
import fs from 'fs';
import ejs from 'ejs';

import sgMail from '@sendgrid/mail';

sgMail.setApiKey(env.TAASKED_SENDGRID_API_KEY!);

export interface ITemplateEmail {
    text: string;
    html: string;
    subject: string;
}

export interface IRenderedEmail {
    text: string;
    html: string;
    subject: string;
}

const templates: { [id: string]: ITemplateEmail } = {};

export const init = (
    template: string,
    language: string = 'en'
): ITemplateEmail => {
    return {
        subject: fs
            .readFileSync(
                `${process.cwd()}/emails/${language}/${template}/subject.ejs`
            )
            .toString()
            .trim(),
        text: fs
            .readFileSync(
                `${process.cwd()}/emails/${language}/${template}/text.ejs`
            )
            .toString()
            .trim(),
        html: fs
            .readFileSync(
                `${process.cwd()}/emails/${language}/${template}/html.ejs`
            )
            .toString()
            .trim()
    };
};

export const render = async (
    template: string,
    attributes: any = {},
    language: string = 'en'
): Promise<IRenderedEmail | null> => {
    const id = `${language}/${template}`;
    if (!templates.hasOwnProperty(id)) {
        templates[id] = await init(template, language);
    }
    return {
        subject: ejs.render(templates[id].subject, attributes),
        html: ejs.render(templates[id].html, attributes),
        text: ejs.render(templates[id].text, attributes)
    };
};

export const send = async (
    to: string,
    content: IRenderedEmail
): Promise<boolean> => {
    sgMail.send({
        to,
        from: env.TAASKED_FROM_EMAIL!,
        text: content.text,
        html: content.html,
        subject: content.subject
    });
    return false;
};

export const mail = async (
    to: string,
    template: string,
    attributes: any = {},
    language: string = 'en'
): Promise<boolean> => {
    const content = await render(template, attributes, language);
    if (!content) {
        return false;
    }
    return await send(to, content);
};
