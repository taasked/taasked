import { FastifyInstance } from 'fastify';
import { Sequelize } from 'sequelize/types';
import { FragmentModel } from '../models/fragment';
import { HookTokenModel } from '../models/hook-token';
import { OrgModel } from '../models/org';
import { OrgUserModel } from '../models/org-user';
import { PasswordTokenModel } from '../models/password-token';
import { SequenceModel } from '../models/sequence';
import { SessionTokenModel } from '../models/session-token';
import { UserModel } from '../models/user';
import { ErroneousResponse } from './erroneous-response';
import { HttpStatus } from './http-status';

export type ThrowableMessage = [boolean, string];

export class Controller {
    server: FastifyInstance;
    user: typeof UserModel;
    sess: typeof SessionTokenModel;
    team: typeof OrgUserModel;
    orgs: typeof OrgModel;
    pass: typeof PasswordTokenModel;
    frag: typeof FragmentModel;
    hookToken: typeof HookTokenModel;
    sequences: typeof SequenceModel;
    db: Sequelize;

    constructor(server: FastifyInstance) {
        this.server = server;
        this.db = server.db;
        this.user = server.models.user;
        this.sess = server.models.sess;
        this.team = server.models.team;
        this.orgs = server.models.orgs;
        this.frag = server.models.frag;
        this.pass = server.models.pass;
        this.sequences = server.models.sequences;
        this.hookToken = server.models.hookToken;
    }

    /**
     * Returns a {@link ErroneousResponse} for the first {@link ThrowableMessage} which value is `true`.
     *
     * @param throwableMessages
     * @param status
     */
    validate(
        throwableMessages: ThrowableMessage[],
        status: HttpStatus = HttpStatus._400_BAD_REQUEST
    ): ErroneousResponse | null {
        for (let throwableMessage of throwableMessages) {
            if (!!throwableMessage[0]) {
                return new ErroneousResponse(throwableMessage[1], status);
            }
        }
        return null;
    }
}
