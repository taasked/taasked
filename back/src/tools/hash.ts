import bcrypt from 'bcrypt';

export const hash = async (
    str: string,
    saltOrRounds: number = 10
): Promise<string> => {
    return await bcrypt.hash(str, saltOrRounds);
};

export const compare = async (str1: string, str2: string): Promise<boolean> => {
    return await bcrypt.compare(str1, str2);
};
