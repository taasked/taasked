import { Static, Type } from '@sinclair/typebox';

export const AuthenticatedUser = Type.Object({
    email: Type.String({ format: 'email' }),
    attributes: Type.Object({
        dark: Type.Boolean(),
        oid: Type.Union([Type.Integer({ minimum: 1 }), Type.Null()])
    }),
    lang: Type.String({ minLength: 2 }),
    token: Type.Optional(Type.String())
});

export type AuthenticatedUserType = Static<typeof AuthenticatedUser>;
