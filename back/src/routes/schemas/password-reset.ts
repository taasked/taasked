import { Static, Type } from '@sinclair/typebox';

export const PasswordResetRequest = Type.Object({
    email: Type.String({ format: 'email' })
});

export const PasswordResetRequestResponse = Type.Object({
    token: Type.String()
});

export const PasswordResetResolve = Type.Object({
    token: Type.String(),
    code: Type.String({ minLength: 6, maxLength: 6 }),
    password: Type.String({ minLength: 6 })
});

export type PasswordResetRequestType = Static<typeof PasswordResetRequest>;
export type PasswordResetResolveType = Static<typeof PasswordResetResolve>;
export type PasswordResetRequestResponseType = Static<
    typeof PasswordResetRequestResponse
>;
