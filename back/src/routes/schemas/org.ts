import { Static, Type } from '@sinclair/typebox';
import { OrgModelType } from '../../models/org';

export const Org = Type.Object({
    id: Type.Integer(),
    name: Type.String(),
    type: Type.Number()
});

export type OrgType = Static<typeof Org>;

export const OrgCreation = Type.Object({
    name: Type.String({ minLength: 1 }),
    type: Type.Number({ default: OrgModelType.Public })
});

export type OrgCreationType = Static<typeof OrgCreation>;
