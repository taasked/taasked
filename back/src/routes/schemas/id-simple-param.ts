import { Static, Type } from '@sinclair/typebox';

export const IdSimpleParam = Type.Object({
    id: Type.String()
});

export type IdSimpleParamType = Static<typeof IdSimpleParam>;
