import { Static, Type } from '@sinclair/typebox';
import { FragmentType } from '../../models/fragment';
import { IdSimpleParam } from './id-simple-param';

const fragmentTypeList = [
    Type.Literal(FragmentType.Basic),
    Type.Literal(FragmentType.Link),
    Type.Literal(FragmentType.Task),
    Type.Literal(FragmentType.Item),
    Type.Literal(FragmentType.Code),
    Type.Literal(FragmentType.Reference)
];

const TypedFragmentType = Type.Union(fragmentTypeList, {
    default: FragmentType.Basic
});

export const FragmentCreation = Type.Object({
    content: Type.String(),
    attributes: Type.Any({ default: {} }),
    type: TypedFragmentType,
    orgId: Type.Number({ minimum: 1 }),
    public: Type.Optional(Type.Boolean({ default: false })),
    archived: Type.Optional(Type.Boolean({ default: false })),
    rootId: Type.Optional(
        Type.Union([Type.Null(), Type.Number({ minimum: 1 })])
    ),
    afterId: Type.Optional(
        Type.Union([Type.Null(), Type.Number({ minimum: 1 })])
    ),
    parentId: Type.Optional(
        Type.Union([Type.Null(), Type.Number({ minimum: 1 })])
    )
});

export type FragmentCreationType = Static<typeof FragmentCreation>;

export const FragmentEdition = Type.Object({
    content: Type.Optional(Type.String()),
    archived: Type.Optional(Type.Boolean()),
    public: Type.Optional(Type.Boolean()),
    attributes: Type.Optional(Type.Any()),
    type: Type.Optional(Type.Union(fragmentTypeList)),
    orgId: Type.Optional(Type.Number({ minimum: 1 })),
    rootId: Type.Optional(
        Type.Union([Type.Null(), Type.Number({ minimum: 1 })])
    ),
    afterId: Type.Optional(
        Type.Union([Type.Null(), Type.Number({ minimum: 1 })])
    ),
    parentId: Type.Optional(
        Type.Union([Type.Null(), Type.Number({ minimum: 1 })])
    )
});

export type FragmentEditionType = Static<typeof FragmentEdition>;

export const FragmentBulkEdition = Type.Object({
    changes: Type.Array(Type.Union([IdSimpleParam, FragmentEdition]))
});

export type FragmentBulkEditionType = Static<typeof FragmentBulkEdition>;
