import { Static, Type } from '@sinclair/typebox';

export const TwoFAToken = Type.Object({
    token: Type.String()
});

export type TwoFATokenType = Static<typeof TwoFAToken>;
