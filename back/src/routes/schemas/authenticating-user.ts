import { Static, Type } from '@sinclair/typebox';

export const AuthenticatingUser = Type.Object({
    email: Type.String({ format: 'email' }),
    password: Type.String({ minLength: 6 })
});

export const TwoFAAuthenticatingUser = Type.Object({
    code: Type.String({ minLength: 6 }),
    token: Type.String({ minLength: 6 })
});

export type AuthenticatingUserType = Static<typeof AuthenticatingUser>;

export type TwoFAAuthenticatingUserType = Static<
    typeof TwoFAAuthenticatingUser
>;
