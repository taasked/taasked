import { Static, Type } from '@sinclair/typebox';

export const RegisteringUser = Type.Object({
    email: Type.String({ format: 'email' }),
    password: Type.String({ minLength: 6 }),
    twoFAEmail: Type.Boolean({ default: false })
});

export type RegisteringUserType = Static<typeof RegisteringUser>;
