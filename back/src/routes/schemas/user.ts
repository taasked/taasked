import { Static, Type } from '@sinclair/typebox';

export const User = Type.Object({
    id: Type.Integer(),
    email: Type.String({ format: 'email' }),
    password: Type.String()
});

export type UserType = Static<typeof User>;

export const UserAttributes = Type.Object({
    dark: Type.Optional(Type.Boolean()),
    oid: Type.Optional(Type.Union([Type.Number(), Type.Null()]))
});

export type UserAttributesType = Static<typeof UserAttributes>;
