import { Static, Type } from '@sinclair/typebox';

export const HookToken = Type.Object({
    id: Type.Integer(),
    orgId: Type.Integer(),
    userId: Type.Integer(),
    token: Type.String()
});

export type HookTokenType = Static<typeof HookToken>;

export const GitHookRequestParams = Type.Object({
    token: Type.String()
});

export type GitHookRequestParamsType = Static<typeof GitHookRequestParams>;

export const GitHook = Type.Object({
    commit: Type.Object({
        message: Type.String()
    })
});

export type GitHookType = Static<typeof GitHook>;
