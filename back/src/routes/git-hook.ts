import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import { RouterRequiredOptions } from '../interfaces/router-required-options';
import { ErroneousResponse } from '../tools/erroneous-response';
import { HttpStatus } from '../tools/http-status';
import { GitHookRequestParamsType, GitHookType } from './schemas/hook-token';

export default fp(
    async (
        server: FastifyInstance,
        options: FastifyPluginOptions & RouterRequiredOptions
    ) => {
        server.post<{
            Reply: void | ErroneousResponse;
            Body: GitHookType;
            Params: GitHookRequestParamsType;
        }>(`${options.prefix}/:token`, async (request, reply) => {
            const fullToken = request.params.token.split('-');
            const orgId = parseInt(fullToken[0], 10);
            const token = fullToken.splice(1).join('-');
            const check = await server.hookToken.check(orgId, token);

            if (check instanceof ErroneousResponse) {
                return reply.status(check.status).send();
            }

            await server.hook.apply(
                orgId,
                request.body,
                request.headers['user-agent']
            );

            reply.status(HttpStatus._202_ACCEPTED).send();
        });

        server.log.info('✅ Git hooks routes setuped');
    }
);
