import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import { authenticate } from '../controllers/auth';
import { RouterRequiredOptions } from '../interfaces/router-required-options';
import { ErroneousResponse } from '../tools/erroneous-response';
import { HttpStatus } from '../tools/http-status';
import { UserAttributesType, UserType } from './schemas/user';

export default fp(
    async (
        server: FastifyInstance,
        options: FastifyPluginOptions & RouterRequiredOptions
    ) => {
        /**
         * Get authenticated user profile
         */
        server.get<{
            Reply: UserType | ErroneousResponse;
        }>(
            `${options.prefix}/me`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const result = await server.user.get(request.uid!);

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );
        /**
         * Get authenticated user profile
         */
        server.patch<{
            Body: UserAttributesType;
            Reply: UserAttributesType | ErroneousResponse;
        }>(
            `${options.prefix}/me`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const result = await server.user.patch(
                    request.uid!,
                    request.body
                );

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._202_ACCEPTED).send(result);
            }
        );

        server.log.info('✅ User routes setuped');
    }
);
