import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import { authenticate } from '../controllers/auth';
import { env } from '../env';
import { RouterRequiredOptions } from '../interfaces/router-required-options';
import { ErroneousResponse } from '../tools/erroneous-response';
import { HttpStatus } from '../tools/http-status';
import { TwoFATokenType } from './schemas/2fa-token';
import { AuthenticatedUserType } from './schemas/authenticated-user';
import {
    AuthenticatingUserType,
    TwoFAAuthenticatingUser,
    TwoFAAuthenticatingUserType
} from './schemas/authenticating-user';
import {
    PasswordResetRequest,
    PasswordResetRequestResponseType,
    PasswordResetRequestType,
    PasswordResetResolve,
    PasswordResetResolveType
} from './schemas/password-reset';
import {
    RegisteringUser,
    RegisteringUserType
} from './schemas/registering-user';

export default fp(
    async (
        server: FastifyInstance,
        options: FastifyPluginOptions & RouterRequiredOptions
    ) => {
        /**
         * Register a new user
         */
        server.post<{
            Body: RegisteringUserType;
            Reply: AuthenticatedUserType | TwoFATokenType | ErroneousResponse;
        }>(
            `${options.prefix}/register`,
            {
                schema: { body: RegisteringUser }
            },
            async (request, reply) => {
                const result = await server.auth.register(request.body, reply);

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._201_CREATED).send(result);
            }
        );

        /**
         * Sign an user in (first factor auth). Depending on user settings,
         * it can return the user profile + auth token if 2 factor auth is
         * not activated, or a two factor client side token in case of 2 factor
         * authentication.
         */
        server.post<{
            Body: AuthenticatingUserType;
            Reply: TwoFATokenType | AuthenticatedUserType | ErroneousResponse;
        }>(
            `${options.prefix}/signin`,
            {
                schema: { body: RegisteringUser }
            },
            async (request, reply) => {
                const result = await server.auth.signin(
                    { ...request.body },
                    reply
                );

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        /**
         * Sign an user in (second factor auth)
         */
        server.post<{
            Body: TwoFAAuthenticatingUserType;
            Reply: AuthenticatedUserType | TwoFATokenType | ErroneousResponse;
        }>(
            `${options.prefix}/2fa`,
            {
                schema: { body: TwoFAAuthenticatingUser }
            },
            async (request, reply) => {
                const result = await server.auth.twofa(
                    { ...request.body },
                    reply
                );

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        /**
         * Request password reset
         */
        server.post<{
            Body: PasswordResetRequestType;
            Reply: PasswordResetRequestResponseType | ErroneousResponse;
        }>(
            `${options.prefix}/reset-password-request`,
            {
                schema: { body: PasswordResetRequest }
            },
            async (request, reply) => {
                const result = await server.auth.resetPasswordRequest({
                    ...request.body
                });
                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._202_ACCEPTED).send(result);
            }
        );

        /**
         * Request password resolve
         */
        server.post<{
            Body: PasswordResetResolveType;
            Reply: TwoFATokenType | AuthenticatedUserType | ErroneousResponse;
        }>(
            `${options.prefix}/reset-password-resolve`,
            {
                schema: { body: PasswordResetResolve }
            },
            async (request, reply) => {
                const result = await server.auth.resetPasswordResolve(
                    {
                        ...request.body
                    },
                    reply
                );

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._202_ACCEPTED).send(result);
            }
        );

        /**
         * Sign an user out. Supposed the user have been succesfully
         * authenticated and that request contains a `tid` property that
         * is the token id.
         */
        server.delete(
            `${options.prefix}`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const result = await server.auth.signout(request.tid!, reply);
                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }
                reply.status(HttpStatus._202_ACCEPTED).send();
            }
        );

        server.log.info('✅ Auth routes setuped');
    }
);
