import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import { authenticate } from '../controllers/auth';
import { RouterRequiredOptions } from '../interfaces/router-required-options';
import {
    FragmentModelAttributes,
    PartialFragmentModelAttributes
} from '../models/fragment';
import { ErroneousResponse } from '../tools/erroneous-response';
import { HttpStatus } from '../tools/http-status';
import { int } from '../tools/utils';
import {
    FragmentCreation,
    FragmentCreationType,
    FragmentEdition,
    FragmentEditionType
} from './schemas/fragment';
import { IdSimpleParamType } from './schemas/id-simple-param';

export default fp(
    async (
        server: FastifyInstance,
        options: FastifyPluginOptions & RouterRequiredOptions
    ) => {
        /**
         * Get fragment by ID
         */
        server.get<{
            Reply: FragmentModelAttributes | ErroneousResponse;
            Params: IdSimpleParamType;
        }>(
            `${options.prefix}/:id`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const id = int(request.params.id);
                if (isNaN(id)) {
                    return reply.status(HttpStatus._404_NOT_FOUND).send();
                }

                const result = await server.frag.get(request.uid!, id);

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        /**
         * Get public fragment by ID
         */
        server.get<{
            Reply: FragmentModelAttributes | ErroneousResponse;
            Params: IdSimpleParamType;
        }>(`/public${options.prefix}/:id`, async (request, reply) => {
            const id = int(request.params.id);
            if (isNaN(id)) {
                return reply.status(HttpStatus._404_NOT_FOUND).send();
            }

            const result = await server.frag.pub(id);

            if (result instanceof ErroneousResponse) {
                return reply.status(result.status).send(result);
            }

            reply.status(HttpStatus._200_OK).send(result);
        });

        /**
         * Get sub root fragments by ID
         */
        server.get<{
            Reply: FragmentModelAttributes[] | ErroneousResponse;
            Params: IdSimpleParamType;
        }>(
            `${options.prefix}/:id/children`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const id = int(request.params.id);
                if (isNaN(id)) {
                    return reply.status(HttpStatus._404_NOT_FOUND).send();
                }

                const result = await server.frag.children(request.uid!, id);

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        /**
         * Create new fragment
         */
        server.post<{
            Body: FragmentCreationType;
            Reply: FragmentModelAttributes | ErroneousResponse;
        }>(
            `${options.prefix}`,
            {
                preHandler: authenticate,
                schema: { body: FragmentCreation }
            },
            async (request, reply) => {
                const result = await server.frag.create(request.uid!, {
                    archived: false,
                    public: false,
                    userId: request.uid!,
                    ...request.body
                });

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._201_CREATED).send(result);
            }
        );

        /**
         * Update fragment
         */
        server.put<{
            Params: IdSimpleParamType;
            Body: FragmentEditionType;
            Reply: FragmentModelAttributes | ErroneousResponse;
        }>(
            `${options.prefix}/:id`,
            {
                preHandler: authenticate,
                schema: { body: FragmentEdition }
            },
            async (request, reply) => {
                const id = int(request.params.id);
                if (isNaN(id)) {
                    return reply.status(HttpStatus._404_NOT_FOUND).send();
                }

                const result = await server.frag.update(request.uid!, id, {
                    ...request.body
                });

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        /**
         * Update fragment and just acknowledge
         */
        server.patch<{
            Params: IdSimpleParamType;
            Body: FragmentEditionType;
            Reply: PartialFragmentModelAttributes | void | ErroneousResponse;
        }>(
            `${options.prefix}/:id`,
            {
                preHandler: authenticate,
                schema: { body: FragmentEdition }
            },
            async (request, reply) => {
                const id = int(request.params.id);
                if (isNaN(id)) {
                    return reply.status(HttpStatus._404_NOT_FOUND).send();
                }

                const result = await server.frag.ack(request.uid!, id, {
                    ...request.body
                });

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                if (!!result) {
                    reply.status(HttpStatus._202_ACCEPTED).send(result);
                } else {
                    reply.status(HttpStatus._204_NO_CONTENT).send();
                }
            }
        );

        /**
         * Delete fragment
         */
        server.delete<{
            Params: IdSimpleParamType;
            Reply: void | ErroneousResponse;
        }>(
            `${options.prefix}/:id`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const id = int(request.params.id);
                if (isNaN(id)) {
                    return reply.status(HttpStatus._404_NOT_FOUND).send();
                }

                const result = await server.frag.delete(request.uid!, id);

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._202_ACCEPTED).send();
            }
        );

        server.log.info('✅ Fragment routes setuped');
    }
);
