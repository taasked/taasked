import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import { authenticate } from '../controllers/auth';
import { RouterRequiredOptions } from '../interfaces/router-required-options';
import { ErroneousResponse } from '../tools/erroneous-response';
import { HttpStatus } from '../tools/http-status';
import { int } from '../tools/utils';
import { HookTokenType } from './schemas/hook-token';
import { IdSimpleParamType } from './schemas/id-simple-param';
import { OrgCreation, OrgCreationType, OrgType } from './schemas/org';

export default fp(
    async (
        server: FastifyInstance,
        options: FastifyPluginOptions & RouterRequiredOptions
    ) => {
        /**
         * Get authenticated user personal organization
         */
        server.get<{
            Reply: OrgType | ErroneousResponse;
        }>(
            `${options.prefix}/me`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const result = await server.orgs.getPersonal(request.uid!);

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );
        /**
         * Get user organizations
         */
        server.get<{
            Reply: OrgType[] | ErroneousResponse;
        }>(
            `${options.prefix}`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const result = await server.orgs.all(request.uid!);

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        /**
         * Get organization
         */
        server.get<{
            Reply: OrgType | ErroneousResponse;
            Params: IdSimpleParamType;
        }>(
            `${options.prefix}/:id`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const result = await server.orgs.get(
                    request.uid!,
                    int(request.params.id)
                );

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        /**
         * Create organization
         */
        server.post<{
            Body: OrgCreationType;
            Reply: OrgType | ErroneousResponse;
        }>(
            `${options.prefix}`,
            {
                preHandler: authenticate,
                schema: { body: OrgCreation }
            },
            async (request, reply) => {
                const result = await server.orgs.create(
                    request.uid!,
                    request.body
                );

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._201_CREATED).send(result);
            }
        );

        /**
         * Get organization new hook token
         */
        server.post<{
            Reply: HookTokenType | ErroneousResponse;
            Params: IdSimpleParamType;
        }>(
            `${options.prefix}/:id/git-hook`,
            {
                preHandler: authenticate
            },
            async (request, reply) => {
                const result = await server.hookToken.create(
                    request.uid!,
                    int(request.params.id)
                );

                if (result instanceof ErroneousResponse) {
                    return reply.status(result.status).send(result);
                }

                reply.status(HttpStatus._200_OK).send(result);
            }
        );

        server.log.info('✅ Organization routes setuped');
    }
);
