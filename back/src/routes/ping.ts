import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import fp from 'fastify-plugin';
import { RouterRequiredOptions } from '../interfaces/router-required-options';
import { HttpStatus } from '../tools/http-status';

export default fp(
    async (
        server: FastifyInstance,
        options: FastifyPluginOptions & RouterRequiredOptions
    ) => {
        server.get(`${options.prefix}`, async (request, reply) => {
            reply.status(HttpStatus._202_ACCEPTED).send();
        });

        server.log.info('✅ Ping routes setuped');
    }
);
