import { FastifyInstance } from 'fastify';
import {
    Association,
    DataTypes,
    HasManyAddAssociationMixin,
    HasManyCountAssociationsMixin,
    HasManyCreateAssociationMixin,
    HasManyGetAssociationsMixin,
    HasManyHasAssociationMixin,
    Model,
    Optional
} from 'sequelize';
import { FragmentModel, FragmentModelAttributes } from './fragment';
import { UserModel } from './user';

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    type: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
};

/**
 * Organization interface
 */
export interface OrgModelAttributes {
    /**
     * Organization ID
     */
    id: number;
    /**
     * Organization name
     */
    name: string;
    /**
     * Organization type, see {@link OrgModelType}
     */
    type: number;

    fragments?: FragmentModelAttributes[];
}

export interface OrgModelCreationAttributes
    extends Optional<OrgModelAttributes, 'id'> {}

/**
 * Type of organization
 */
export enum OrgModelType {
    Private = 1,
    Public = 2
}

/**
 * Organization sequelize model
 */
export class OrgModel
    extends Model<OrgModelAttributes, OrgModelCreationAttributes>
    implements OrgModelAttributes
{
    declare id: number;
    declare name: string;
    declare type: OrgModelType;
    declare readonly createdAt: Date;
    declare readonly updatedAt: Date;
    declare getUsers: HasManyGetAssociationsMixin<UserModel>;
    declare addUser: HasManyAddAssociationMixin<UserModel, number>;
    declare hasUser: HasManyHasAssociationMixin<UserModel, number>;
    declare countUsers: HasManyCountAssociationsMixin;
    declare createUser: HasManyCreateAssociationMixin<UserModel>;
    declare readonly users?: UserModel[];
    declare readonly fragments?: FragmentModel[];

    declare static associations: {
        users: Association<OrgModel, UserModel>;
        fragments: Association<OrgModel, FragmentModel>;
    };
}

export function orgModelFactory(server: FastifyInstance): typeof OrgModel {
    server.log.info(`🟠 Initializing organization model...`);

    OrgModel.init(schema, {
        sequelize: server.db,
        modelName: 'org'
    });

    server.log.info(`✅ Initialized organization model.`);
    return OrgModel;
}
