import { FastifyInstance } from 'fastify';
import {
    Association,
    BelongsToGetAssociationMixin,
    DataTypes,
    Model,
    Optional
} from 'sequelize';
import { OrgModel } from './org';

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ouid: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    oid: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    key: {
        type: DataTypes.STRING,
        allowNull: false
    }
};

export interface SequenceModelAttributes {
    id: number;
    key: string;
    oid: number;
    ouid: number;
}

export interface SequenceModelCreationAttributes
    extends Optional<SequenceModelAttributes, 'id'> {}

export class SequenceModel
    extends Model<SequenceModelAttributes, SequenceModelCreationAttributes>
    implements SequenceModelAttributes
{
    declare id: number;
    declare ouid: number;
    declare key: string;
    declare oid: number;

    declare readonly createdAt: Date;
    declare readonly updatedAt: Date;
    declare getOrg: BelongsToGetAssociationMixin<OrgModel>;
    declare readonly org?: OrgModel;

    declare static associations: {
        org: Association<SequenceModel, OrgModel>;
    };
}

export function sequenceModelFactory(
    server: FastifyInstance
): typeof SequenceModel {
    server.log.info(`🟠 Initializing sequence model...`);

    SequenceModel.init(schema, {
        sequelize: server.db,
        modelName: 'sequence'
    });

    server.log.info(`✅ Initialized sequence model.`);
    return SequenceModel;
}
