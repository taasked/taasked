import { FastifyInstance } from 'fastify';
import { DataTypes, Model, Optional } from 'sequelize';

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    token: {
        type: DataTypes.STRING(1024),
        allowNull: false
    },
    usedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
};

export interface HookTokenModelAttributes {
    id: number;

    token: string;
    usedAt: Date;
    orgId: number;
    userId: number;
}

export interface HookTokenModelCreationAttributes
    extends Optional<HookTokenModelAttributes, 'id'> {}

/**
 * Hook reset token sequelize model
 */
export class HookTokenModel extends Model {
    declare id: number;
    declare token: string;
    declare orgId: number;
    declare userId: number;
    declare usedAt: Date;
}

export function hookTokenModelFactory(
    server: FastifyInstance
): typeof HookTokenModel {
    server.log.info(`🟠 Initializing hook token model...`);

    HookTokenModel.init(schema, {
        sequelize: server.db,
        modelName: 'hook_token'
    });

    server.log.info(`✅ Initialized hook token model.`);
    return HookTokenModel;
}
