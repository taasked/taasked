import { FastifyInstance } from 'fastify';
import {
    Sequelize,
    Model,
    DataTypes,
    Optional,
    BelongsToGetAssociationMixin,
    Association
} from 'sequelize';
import { OrgModel } from './org';
import { UserModel } from './user';

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    level: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    personal: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    }
};

/**
 * Organisation/user relationship interface
 */
export interface OrgUserModelAttributes {
    /**
     * Relationship ID
     */
    id: number;
    /**
     * User level into organization (see {@link OrgUserModelLevel})
     */
    level: OrgUserModelLevel;
    /**
     * Is the org the user personal one
     */
    personal: boolean;

    orgId?: number;

    userId?: number;
}

export interface OrgUserModelCreationAttributes
    extends Optional<OrgUserModelAttributes, 'id'> {}

export enum OrgUserModelLevel {
    Readonly = 0,
    Regular = 1,
    Owner = 9
}

export class OrgUserModel
    extends Model<OrgUserModelAttributes, OrgUserModelCreationAttributes>
    implements OrgUserModelAttributes
{
    declare id: number;
    declare level: OrgUserModelLevel;
    declare personal: boolean;
    declare orgId: number;
    declare userId: number;

    declare readonly createdAt: Date;
    declare readonly updatedAt: Date;
    declare getUser: BelongsToGetAssociationMixin<UserModel>;
    declare getOrg: BelongsToGetAssociationMixin<OrgModel>;
    declare readonly user?: UserModel;
    declare readonly org?: OrgModel;

    declare static associations: {
        user: Association<OrgUserModel, UserModel>;
        org: Association<OrgUserModel, OrgModel>;
    };
}

export function orgUserModelFactory(
    server: FastifyInstance
): typeof OrgUserModel {
    server.log.info(`🟠 Initializing organization users model...`);

    OrgUserModel.init(schema, {
        sequelize: server.db,
        modelName: 'orgUser'
    });

    server.log.info(`✅ Initialized organization users model.`);
    return OrgUserModel;
}
