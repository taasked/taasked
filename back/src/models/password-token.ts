import { FastifyInstance } from 'fastify';
import { DataTypes, Model, Optional } from 'sequelize';

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    code: {
        type: DataTypes.STRING,
        allowNull: false
    },
    token: {
        type: DataTypes.STRING(1024),
        allowNull: false
    }
};

export interface PasswordTokenModelAttributes {
    id: number;
    /**
     * Code is sent by email
     */
    code: string;
    /**
     * Token is sent to REST API
     */
    token: string;
}

export interface PasswordTokenModelCreationAttributes
    extends Optional<PasswordTokenModelAttributes, 'id'> {}

/**
 * Password reset token sequelize model
 */
export class PasswordTokenModel extends Model {
    declare id: number;
    declare code: string;
    declare token: string;
    declare userId: number;
}

export function passwordTokenModelFactory(
    server: FastifyInstance
): typeof PasswordTokenModel {
    server.log.info(`🟠 Initializing password token model...`);

    PasswordTokenModel.init(schema, {
        sequelize: server.db,
        modelName: 'password_token'
    });

    server.log.info(`✅ Initialized password token model.`);
    return PasswordTokenModel;
}
