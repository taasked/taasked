import { FastifyInstance } from 'fastify';
import { DataTypes, Model, Optional } from 'sequelize';

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    twoFACode: {
        type: DataTypes.STRING,
        allowNull: false
    },
    twoFAToken: {
        type: DataTypes.STRING(1024),
        allowNull: false
    },
    // Token used in private cookie
    token: {
        type: DataTypes.STRING(1024),
        allowNull: false
    },
    // Token used in auth header
    headerToken: {
        type: DataTypes.STRING(1024),
        allowNull: false
    }
};

export interface SessionTokenModelAttributes {
    id: number;
    twoFACode: string;
    twoFAToken: string;
    headerToken: string;
    token: string;
    userId: number;
}

export interface SessionTokenModelCreationAttributes
    extends Optional<SessionTokenModelAttributes, 'id'> {}

/**
 * Session token sequelize model
 */
export class SessionTokenModel extends Model {
    declare id: number;
    declare twoFACode: string;
    declare twoFAToken: string;
    declare headerToken: string;
    declare token: string;
    declare userId: number;
}

export function sessionTokenModelFactory(
    server: FastifyInstance
): typeof SessionTokenModel {
    server.log.info(`🟠 Initializing session token model...`);

    SessionTokenModel.init(schema, {
        sequelize: server.db,
        modelName: 'session_token'
    });

    server.log.info(`✅ Initialized session token model.`);
    return SessionTokenModel;
}
