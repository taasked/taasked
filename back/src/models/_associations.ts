import { FastifyInstance } from 'fastify';
import { FragmentModel } from './fragment';
import { HookTokenModel } from './hook-token';
import { OrgModel } from './org';
import { OrgUserModel } from './org-user';
import { PasswordTokenModel } from './password-token';
import { SequenceModel } from './sequence';
import { SessionTokenModel } from './session-token';
import { UserModel } from './user';

export function associationFactory(server: FastifyInstance) {
    // Users can have many session tokens, tokens are for one user only
    SessionTokenModel.belongsTo(UserModel);
    UserModel.hasMany(SessionTokenModel);

    // Password tokens stuff
    PasswordTokenModel.belongsTo(UserModel);
    UserModel.hasMany(PasswordTokenModel);

    // Users can be part of many orgs, and orgs have many users
    UserModel.belongsToMany(OrgModel, { through: OrgUserModel });
    OrgModel.belongsToMany(UserModel, { through: OrgUserModel });
    OrgUserModel.belongsTo(OrgModel);
    OrgUserModel.belongsTo(UserModel);
    OrgModel.hasMany(OrgUserModel);
    UserModel.hasMany(OrgUserModel);

    // Orgs can have many git hooks tokens, an we keep track of the user who created git hook tokens
    HookTokenModel.belongsTo(OrgModel);
    OrgModel.hasMany(HookTokenModel);
    HookTokenModel.belongsTo(UserModel);
    UserModel.hasMany(HookTokenModel);

    // Org can have many sequences
    OrgModel.hasMany(SequenceModel, { foreignKey: 'oid' });
    SequenceModel.belongsTo(OrgModel, { foreignKey: 'oid' });

    // Orgs have many fragments, each fragment belongs only to the org
    OrgModel.hasMany(FragmentModel);
    FragmentModel.belongsTo(OrgModel);

    // Users have many fragments, each fragment belongs to the user who created it
    UserModel.hasMany(FragmentModel);
    FragmentModel.belongsTo(UserModel);

    // Fragment are grouped in root fragments
    FragmentModel.belongsTo(FragmentModel, {
        constraints: false,
        as: 'root'
    });

    //
    FragmentModel.hasMany(FragmentModel, {
        constraints: false,
        as: 'fragments',
        foreignKey: { name: 'rootId' }
    });

    // Fragment can also optionally belong to a parent fragment
    FragmentModel.belongsTo(FragmentModel, {
        constraints: false,
        as: 'parent'
    });

    // Fragment are ordered
    FragmentModel.belongsTo(FragmentModel, { constraints: false, as: 'after' });

    server.log.info('✅ Models association done');
}
