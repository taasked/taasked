import { hash } from 'bcrypt';
import { FastifyInstance } from 'fastify';
import {
    Association,
    CreateOptions,
    DataTypes,
    HasManyAddAssociationMixin,
    HasManyCountAssociationsMixin,
    HasManyCreateAssociationMixin,
    HasManyGetAssociationsMixin,
    HasManyHasAssociationMixin,
    Model,
    Optional
} from 'sequelize';
import { OrgUserModel } from './org-user';
//import { Org } from './org';

const PROTECTED_ATTRIBUTES: string[] = ['password'];

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    email: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false
    },
    twoFAEmail: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        default: false
    },
    attributes: {
        type: DataTypes.JSONB,
        allowNull: false,
        default: { dark: false, oid: null }
    },
    level: DataTypes.INTEGER
};

/**
 * Hash password before inserting record
 */
const beforeValidate = async (user: any, options: CreateOptions) => {
    if (!user.attributes) {
        user.attributes = { dark: false, oid: null };
    }
};
const beforeCreate = async (user: any, options: CreateOptions) => {
    user.password = await hash(user.password, 10);
};

export interface IUserAttributes {
    dark: boolean;
    oid: number | null;
}

export interface IUserPatchedAttributes {
    dark?: boolean;
    oid?: number | null;
}

export interface UserModelAttributes {
    id: number;
    email: string;
    password: string;
    token?: string;
    attributes: IUserAttributes;
    twoFAEmail: boolean;
}

export interface UserModelCreationAttributes
    extends Optional<Optional<UserModelAttributes, 'id'>, 'attributes'> {}

/**
 * User level (guest == unauth, user == authenticated, admin)
 */
export enum UserLevel {
    Guest = 0,
    User = 1,
    Admin = 100
}

/**
 * Avoid exposing password hash and other
 * protected attributes in JSON representation of record
 */
export class UserModel
    extends Model<UserModelAttributes, UserModelCreationAttributes>
    implements UserModelAttributes
{
    declare id: number;
    declare email: string;
    declare password: string;
    declare token: string;
    declare attributes: IUserAttributes;
    declare twoFAEmail: boolean;
    declare readonly createdAt: Date;
    declare readonly updatedAt: Date;

    declare getOrgs: HasManyGetAssociationsMixin<OrgUserModel>;
    declare addOrg: HasManyAddAssociationMixin<OrgUserModel, number>;
    declare hasOrg: HasManyHasAssociationMixin<OrgUserModel, number>;
    declare countOrgs: HasManyCountAssociationsMixin;
    declare createOrg: HasManyCreateAssociationMixin<OrgUserModel>;
    declare readonly orgUsers?: OrgUserModel[];

    declare static associations: {
        orgUsers: Association<UserModel, OrgUserModel>;
    };

    toJSON() {
        // hide protected fields
        let attributes: any = Object.assign({}, this.get());
        for (let a of PROTECTED_ATTRIBUTES) {
            delete attributes[a];
        }
        return attributes;
    }
}

export function userModelFactory(server: FastifyInstance): typeof UserModel {
    server.log.info(`🟠 Initializing user model...`);

    UserModel.init(schema, {
        hooks: {
            beforeValidate,
            beforeCreate
        },
        sequelize: server.db,
        modelName: 'user'
    });

    server.log.info(`✅ Initialized user model.`);

    return UserModel;
}
