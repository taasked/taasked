import { FastifyInstance } from 'fastify';
import { Association, DataTypes, Model, Optional } from 'sequelize';
import { OrgModel } from './org';

/**
 * Org is considered protected attribute as
 * we dont want to expose systematically organizations for each task list
 */
const PROTECTED_ATTRIBUTES: string[] = ['org'];

const schema = {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: DataTypes.STRING(4),
        allowNull: false
    },
    key: {
        type: DataTypes.STRING(16),
        allowNull: true
    },
    content: {
        type: DataTypes.TEXT,
        allowNull: false,
        default: ''
    },
    attributes: {
        type: DataTypes.JSONB,
        allowNull: false,
        default: {}
    },
    orgId: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    /**
     * ID of user who created the fragment.
     *
     * Fragments created by system have a `null` userId.
     */
    userId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        default: null
    },
    parentId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        default: null
    },
    afterId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        default: null
    },
    rootId: {
        type: DataTypes.INTEGER,
        allowNull: true,
        default: null
    },
    archived: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        default: false
    },
    public: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        default: false
    }
};

export enum FragmentType {
    Basic = 'md', // A piece of markdown text
    Task = 'task', // A todo task
    Item = 'item', // A list item
    Code = 'code', // A code block
    Link = 'link', // An external link w/ preview
    Commit = 'cmit', // A reference to an external GIT commit
    Reference = 'ref' // A reference to a root fragment
}

export interface FragmentModelAttributes {
    id: number;
    key: string | null;
    type: FragmentType;
    content: string;
    archived: boolean;
    public: boolean;
    orgId: number;
    userId: number | null;
    attributes: any;
    parentId?: number | null;
    afterId?: number | null;
    rootId?: number | null;
    fragments?: FragmentModelAttributes[];
    verticals?: { score: number; keyword: string }[];
    meta?: any;
}

export interface PartialFragmentModelAttributes {
    key?: string;
    content?: string;
}

export interface FragmentRelatives {
    root: FragmentModel | null;
    parent: FragmentModel | null;
    previous: FragmentModel | null;
}

export interface FragmentModelCreationAttributes
    extends Optional<Optional<FragmentModelAttributes, 'id'>, 'key'> {}

export class FragmentModel
    extends Model<FragmentModelAttributes, FragmentModelCreationAttributes>
    implements FragmentModelAttributes
{
    declare id: number;
    declare type: FragmentType;
    declare key: string | null;
    declare content: string;
    declare attributes: any;
    declare meta: any;
    declare archived: boolean;
    declare public: boolean;
    declare orgId: number;
    declare userId: number | null;

    declare parentId: number | null;
    declare afterId: number | null;
    declare rootId: number | null;

    declare readonly fragments?: FragmentModel[];
    declare readonly parent?: FragmentModel;
    declare readonly after?: FragmentModel;
    declare readonly root?: FragmentModel;
    declare readonly org?: OrgModel;

    sequence(): string | null {
        if (this.key && this.key.indexOf('-') > -1) {
            return this.key.replace('#', '').split('-')[0];
        }
        return null;
    }

    toJSON() {
        // hide protected fields
        let attributes: any = Object.assign({}, this.get());
        for (let a of PROTECTED_ATTRIBUTES) {
            delete attributes[a];
        }
        if (!attributes.hasOwnProperty('fragments')) {
            attributes.fragments = [];
        }
        if (!attributes.hasOwnProperty('meta')) {
            attributes.meta = {};
        }
        return attributes;
    }

    declare static associations: {
        fragments: Association<FragmentModel, FragmentModel>;
        org: Association<FragmentModel, OrgModel>;
    };
}

export function fragmentModelFactory(
    server: FastifyInstance
): typeof FragmentModel {
    server.log.info(`🟠 Initializing fragment model...`);

    FragmentModel.init(schema, {
        sequelize: server.db,
        modelName: 'fragment'
    });

    server.log.info(`✅ Initialized fragment model.`);
    return FragmentModel;
}
