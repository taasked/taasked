import appFactory from './server';
const args = process.argv;

if (args.length < 3) {
    console.error('No model provided');
    process.exit(1);
}
const model = args[2];
const server = appFactory(false);
const db = server.db;

if (model === 'all') {
    db.drop({ cascade: true })
        .then(() => db.sync())
        .then(() => {
            console.log('🟢 Database synced!');
        })
        .catch((error: any) => {
            console.error(error);
            console.error('🔴 An error occured while syncing the database');
            process.exit(1);
        })
        .then(() => db.close())
        .then(() => {
            process.exit(0);
        });
    
}
// @ts-expect-error
else if (!server.models[model]) {
    console.error('Model does not exist');
    process.exit(1);
} else {

    // @ts-expect-error
    server.models[model]
        .drop()
        // @ts-expect-error
        .then(() => server.models[model].sync())
        .then(() => {
            console.log(`🟢 Model ${model} synced!`);
        })
        .catch((error: any) => {
            console.error(error);
            console.error(`🔴 An error occured while syncing the model ${model}`);
            process.exit(1);
        })
        .then(() => db.close())
        .then(() => {
            process.exit(0);
        });

}