import { GitHookHelper } from '../src/tools/git-hook.helper';
import { COMMIT_DATA } from './data/commit-1';

it('should extract commit data without error', () => {
    COMMIT_DATA.commit.message = 'Ref #400';
    const commit = GitHookHelper.extractCommitData(COMMIT_DATA);
    expect(commit).toBeTruthy();
});

it('should extract commit data without error', () => {
    COMMIT_DATA.commit.message = 'Ref #400';
    const commit = GitHookHelper.extractCommitData(COMMIT_DATA);
    expect(commit?.message).toBe(COMMIT_DATA.commit.message);
    expect(commit?.author).toBe(COMMIT_DATA.commit.author.email);
    expect(commit?.sha).toBe(COMMIT_DATA.sha);
    expect(commit?.url).toBe(COMMIT_DATA.commit.url);
});

it('should extract commit reference without error', () => {
    COMMIT_DATA.commit.message = 'Ref #400';
    const commit = GitHookHelper.extractCommitData(COMMIT_DATA);
    const references = GitHookHelper.extractFragmentReferences(commit!);
    expect(references.length).toBe(1);
    expect(references[0].id).toBe(400);
    expect(references[0].action).toBe('ref');
});

it('should extract commit reference w/ spaces without error', () => {
    COMMIT_DATA.commit.message = 'Ref  #400';
    const commit = GitHookHelper.extractCommitData(COMMIT_DATA);
    const references = GitHookHelper.extractFragmentReferences(commit!);
    expect(references.length).toBe(1);
    expect(references[0].id).toBe(400);
    expect(references[0].action).toBe('ref');
});
