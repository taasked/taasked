import path from 'path';
import { OrgModelType } from '../src/models/org';
import { HttpStatus } from '../src/tools/http-status';
import { default as orgSchema } from './schemas/organization';
import { default as userSchema } from './schemas/user';
import { default as userSchemaWithToken } from './schemas/user.token';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

let email = `${path.basename(__filename)}@example.com`;
let authToken: string;
let orgId: number;

it(`should accept user registration with valid credentials (email ${email})`, () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email,
            password: 'xxx0123',
            twoFAEmail: false
        })
        .expect('status', HttpStatus._201_CREATED);
});

it('should accept user signin with valid credentials', () => {
    return frisby
        .post(`${SERVER_URL}/auth/signin`, { email, password: 'xxx0123' })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', userSchemaWithToken)
        .then((data: any) => (authToken = data.json.token));
});

it('should have created an organization for the new user', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._200_OK)
        .then((data: any) => {
            const json: any = data.json;
            expect(json.orgs.length).toBe(1);
            expect(typeof json.orgs[0].id).toBe('number');
            orgId = json.orgs[0].id;
        });
});

it('created organization after registration is private', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', userSchema)
        .then((data: any) => {
            const json: any = data.json;
            const org = json.orgs[0];

            expect(org.type).toBe(OrgModelType.Private);
        });
});

it('created organization after registration has a welcome fragment', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/organization/me`)
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', orgSchema)
        .then((data: any) => {
            const org: any = data.json;
            expect(org.fragments.length).toBe(1);
        });
});

it('should allow access to organization endpoint', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/organization/${orgId}`)
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', orgSchema);
});

it('shouldnt allow access other organization endpoint user has no access to', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/organization/1`)
        .expect('status', HttpStatus._404_NOT_FOUND);
});

export {};
