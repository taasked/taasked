const frisby = require('frisby');
const Joi = frisby.Joi;

export default Joi.object({
    id: Joi.number(),
    title: Joi.string(),
    description: Joi.string().allow(''),
    done: Joi.boolean(),
    urgent: Joi.boolean(),
    archived: Joi.boolean(),
    createdAt: Joi.string(),
    updatedAt: Joi.string(),
    taskListId: Joi.number(),
    afterId: Joi.number().allow(null),
    parentId: Joi.number().allow(null),
    tasks: Joi.array().optional()
});
