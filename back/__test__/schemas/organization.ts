const frisby = require('frisby');
const Joi = frisby.Joi;

export default Joi.object({
    id: Joi.number(),
    name: Joi.string().allow(''),
    type: Joi.number(),
    createdAt: Joi.string(),
    updatedAt: Joi.string(),
    fragments: Joi.array()
});
