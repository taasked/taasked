const frisby = require('frisby');
const Joi = frisby.Joi;

export default Joi.object({
    email: Joi.string().required(),
    lang: Joi.string().required(),
    token: Joi.string().required(),
    orgId: Joi.number().required(),
    attributes: Joi.object()
});
