const frisby = require('frisby');
const Joi = frisby.Joi;

export default Joi.object({
    token: Joi.string().required()
});
