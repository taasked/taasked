const frisby = require('frisby');
const Joi = frisby.Joi;

export default Joi.object({
    id: Joi.number(),
    email: Joi.string(),
    level: Joi.number().allow(null),
    twoFAEmail: Joi.boolean(),
    createdAt: Joi.string(),
    updatedAt: Joi.string(),
    orgs: Joi.array(),
    attributes: Joi.object()
});
