const frisby = require('frisby');
const Joi = frisby.Joi;

export default Joi.object({
    id: Joi.number(),
    type: Joi.string().valid('md', 'task', 'link', 'ref', 'item', 'cmit'),
    content: Joi.string().allow(''),
    archived: Joi.boolean(),
    public: Joi.boolean(),
    createdAt: Joi.string(),
    updatedAt: Joi.string(),
    orgId: Joi.number(),
    afterId: Joi.number().allow(null),
    parentId: Joi.number().allow(null),
    rootId: Joi.number().allow(null),
    userId: Joi.number().allow(null),
    fragments: Joi.array(),
    verticals: Joi.array(),
    attributes: Joi.object(),
    meta: Joi.object()
});
