import path from 'path';
import { HttpStatus } from '../src/tools/http-status';
import { default as fragmentSchema } from './schemas/fragment';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

it(`should allow getting a public fragment`, () => {
    return frisby
        .get(
            // @ts-ignore
            `${SERVER_URL}/public/fragment/${global.process.env.__PUBLIC_FRAGMENT_ID__}`
        )
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.fragments.length).toBe(1);
        });
});

it(`should allow getting a private fragment`, () => {
    return frisby
        .get(
            // @ts-ignore
            `${SERVER_URL}/public/fragment/${global.process.env.__PRIVATE_FRAGMENT_ID__}`
        )
        .expect('status', HttpStatus._404_NOT_FOUND);
});

export {};
