import path from 'path';
import { FragmentType } from '../src/models/fragment';
import { HttpStatus } from '../src/tools/http-status';
import { default as fragmentSchema } from './schemas/fragment';
import { default as userSchemaWithToken } from './schemas/user.token';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

let email = `${path.basename(__filename)}@example.com`;
let authToken: string;
let orgId: number;
let fragmentId: number;
let referenceId: number;

it(`should accept user registration with valid credentials (email ${email})`, () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email,
            password: 'xxx0123',
            twoFAEmail: false
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', userSchemaWithToken)
        .then((data: any) => {
            authToken = data.json.token;
            expect(typeof data.json.orgId).toBe('number');
            orgId = data.json.orgId;
        });
});

it('should allow creating a new fragment', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, { content: 'Hello', orgId })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            fragmentId = data.json.id;
            expect(data.json.content).toBe('Hello');
        });
});

it('should allow creating a reference fragment', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, {
            content: 'hello',
            orgId,
            type: FragmentType.Reference,
            attributes: { ref: fragmentId }
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            referenceId = data.json.id;
            expect(data.json.content).toBe('hello');
            expect(data.json.attributes.ref).toBe(fragmentId);
        });
});

it('should allow updating the referenced fragment', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, { content: 'test' })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.content).toBe('test');
        });
});

it('should have updated the reference content', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/fragment/${referenceId}`)
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.content).toBe('test');
        });
});

export {};
