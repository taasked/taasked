import path from 'path';
import { HttpStatus } from '../src/tools/http-status';
import { default as tokenSchema } from './schemas/token';
import { default as schemaWithoutToken } from './schemas/user';
import { default as schemaWithToken } from './schemas/user.token';

const frisby = require('frisby');
const Joi = frisby.Joi;
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

let email = `${path.basename(__filename)}@example.com`;
let twoFACode: string = '000000';
let twoFAToken: string;
let authToken: string;

const twoFATokenSchema = Joi.object({
    token: Joi.string().required()
});

it('shouldnt accept user registration with no params', () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {})
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt accept user registration with invalid email', () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, { email: 'xxx', password: 'xxx' })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt accept user registration with empty password', () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email: 'xxx@example.com',
            password: ''
        })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it(`should accept user registration with valid credentials (email ${email})`, () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email,
            password: 'xxx0123',
            twoFAEmail: true
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', tokenSchema);
});

it('shouldnt accept user signin with invalid credentials', () => {
    return frisby
        .post(`${SERVER_URL}/auth/signin`, { email, password: 'xxxx0123' })
        .expect('status', HttpStatus._401_UNAUTHORIZED);
});

it('should accept 2FA first factor user signin with valid credentials', () => {
    return frisby
        .post(`${SERVER_URL}/auth/signin`, { email, password: 'xxx0123' })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', twoFATokenSchema)
        .then((data: any) => {
            twoFAToken = data.json.token;
        });
});

it('shouldnt allow user final signin with invalid 2FA code', () => {
    return frisby
        .post(`${SERVER_URL}/auth/2fa`, { code: 'abcdef', token: twoFAToken })
        .expect('status', HttpStatus._401_UNAUTHORIZED);
});

it('shouldnt allow user final signin with invalid 2FA token', () => {
    return frisby
        .post(`${SERVER_URL}/auth/2fa`, { code: twoFACode, token: 'dzdze' })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow user final signin with valid 2FA code + token', () => {
    return frisby
        .post(`${SERVER_URL}/auth/2fa`, { code: twoFACode, token: twoFAToken })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypes', schemaWithToken)
        .then((data: any) => {
            authToken = data.json.token;
        });
});

it('shouldnt allow accessing private endpoint without token', () => {
    return frisby
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._401_UNAUTHORIZED);
});

it('shouldnt allow accessing private endpoint with scrambled auth header', () => {
    return frisby
        .setup({ request: { headers: { Authorization: 'blabla' } } })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._401_UNAUTHORIZED);
});

it('shouldnt allow accessing private endpoint with invalid token', () => {
    return frisby
        .setup({
            request: {
                headers: {
                    Authorization:
                        'Bearer eznjiduezaugyedgyejazkhduiaezlheguyiadezukd'
                }
            }
        })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._401_UNAUTHORIZED);
});

it('should allow accessing private endpoint with token', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypes', schemaWithoutToken);
});

it('shouldnt accept new user registration with already registered email', () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, { email, password: 'xxxx012d3' })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow to signout', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .delete(`${SERVER_URL}/auth`)
        .expect('status', HttpStatus._202_ACCEPTED);
});

it('shouldnt allow to use auth token after signout', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._401_UNAUTHORIZED);
});

export {};
