const server = (global as any).app;

module.exports = async () => {
    //await server.db.drop();
    server.log.info(`🔵 TEST ENV 🔵 DB dropped`);
    await server.db.close();
    server.log.info(`🔵 TEST ENV 🔵 DB closed`);
    server.close();
};

export {};
