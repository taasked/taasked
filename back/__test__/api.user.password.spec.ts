import path from 'path';
import { HttpStatus } from '../src/tools/http-status';
import token from './schemas/token';
import { default as schemaWithToken } from './schemas/user.token';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

let email = `${path.basename(__filename)}@example.com`;
let password = 'abc123$';
let authToken: string;
let resetToken: string;

it(`should accept user registration with valid credentials (email ${email})`, () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email,
            password: 'xxx0123',
            twoFAEmail: false
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', schemaWithToken)
        .then((data: any) => {
            authToken = data.json.token;
            expect(typeof data.json.orgId).toBe('number');
        });
});

it('should allow to signout', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .delete(`${SERVER_URL}/auth`)
        .expect('status', HttpStatus._202_ACCEPTED);
});

it('shouldnt allow to require password reset with no body', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-request`, {})
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt allow to require password reset with no email', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-request`, { yop: 'plop' })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt allow to require password reset with wrong email ', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-request`, {
            email: 'plop@'
        })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow to require password reset with right email format even if user does not exist (by design non exposure of existing users)', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-request`, {
            email: 'plop@plop.com'
        })
        .expect('status', HttpStatus._202_ACCEPTED)
        .expect('jsonTypesStrict', token);
});

it('should allow to require password reset with right format', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-request`, { email })
        .expect('status', HttpStatus._202_ACCEPTED)
        .expect('jsonTypesStrict', token)
        .then((data: any) => {
            resetToken = data.json.token;
        });
});

it('shouldnt allow to resolve password reset with no body', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-resolve`, {})
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt allow to resolve password reset with wrong code', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-resolve`, {
            code: '123456',
            token: resetToken,
            password: 'hezuiedhziudhize'
        })
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt allow to resolve password reset with wrong token', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-resolve`, {
            code: '000000',
            token: 'jdenzkl',
            password: 'dzadazdazdzadaz'
        })
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt allow to resolve password reset with invalid password', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-resolve`, {
            code: '000000',
            token: 'jdenzkl',
            password: ''
        })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow to resolve password reset with valid password', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-resolve`, {
            code: '000000',
            token: resetToken,
            password: password + password
        })
        .expect('status', HttpStatus._202_ACCEPTED)
        .expect('jsonTypesStrict', schemaWithToken)
        .then((data: any) => {
            authToken = data.json.token;
            expect(typeof data.json.orgId).toBe('number');
        });
});

it('should allow to signout', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .delete(`${SERVER_URL}/auth`)
        .expect('status', HttpStatus._202_ACCEPTED);
});

it('shouldnt allow to reuse token and code', () => {
    return frisby
        .post(`${SERVER_URL}/auth/reset-password-resolve`, {
            code: '000000',
            token: resetToken,
            password: password + password
        })
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt accept user signin with old credentials', () => {
    return frisby
        .post(`${SERVER_URL}/auth/signin`, {
            email,
            password: password
        })
        .expect('status', HttpStatus._401_UNAUTHORIZED);
});

it('should accept user signin with updated credentials', () => {
    return frisby
        .post(`${SERVER_URL}/auth/signin`, {
            email,
            password: password + password
        })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', schemaWithToken)
        .then((data: any) => (authToken = data.json.token));
});

export {};
