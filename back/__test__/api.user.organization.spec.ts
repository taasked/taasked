import path from 'path';
import { HttpStatus } from '../src/tools/http-status';
import { default as orgSchema } from './schemas/organization';
import { default as schemaWithToken } from './schemas/user.token';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

let email = `${path.basename(__filename)}@example.com`;
let organization = `my-org`;
let authToken: string;
let orgId: string;

it(`should accept user registration with valid credentials (email ${email})`, () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email,
            password: 'xxx0123',
            twoFAEmail: false
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', schemaWithToken)
        .then((data: any) => {
            authToken = data.json.token;
            expect(typeof data.json.orgId).toBe('number');
        });
});

it('shouldnt allow to create a new organization with empty name', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/organization`, { name: '' })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt allow to create a new organization with no name', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/organization`, {})
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow to create a new organization', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/organization`, { name: organization })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', orgSchema)
        .then((data: any) => {
            orgId = data.json.id;
            console.log(data.json);
        });
});

it('should list organization in personal endpoint', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/organization`)
        .expect('status', HttpStatus._200_OK)
        .then((data: any) => {
            expect(data.json.map((org: any) => org.id)).toContain(orgId);
        });
});

it('should allow access to organization endpoint', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/organization/${orgId}`)
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', orgSchema);
});

export {};
