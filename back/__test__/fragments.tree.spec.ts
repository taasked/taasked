import { FragmentTree } from '../src/tools/fragment-tree';

const fragments = [
    { id: 1, afterId: null, rootId: null, parentId: null },
    { id: 2, afterId: null, rootId: 1, parentId: 1 },
    { id: 3, afterId: 2, rootId: 1, parentId: 1 },
    { id: 4, afterId: null, rootId: 1, parentId: 3 },
    { id: 6, afterId: 5, rootId: 1, parentId: 3 },
    { id: 5, afterId: 4, rootId: 1, parentId: 3 }
];

it('should allow instanciating a tree', () => {
    const instance = new FragmentTree(fragments);
    expect(instance).toBeTruthy();
});

it('should allow export a tree', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.export();
    expect(result).toBeTruthy();
});

it('should export a correct tree', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.export();
    expect(result!.id).toBe(1);
    expect(result!.fragments!.length).toBe(2);
    expect(result!.fragments![0].id).toBe(2);
    expect(result!.fragments![1].id).toBe(3);
    expect(result!.fragments![1].fragments!.length).toBe(3);
    expect(result!.fragments![1].fragments![0].id).toBe(4);
    expect(result!.fragments![1].fragments![1].id).toBe(5);
    expect(result!.fragments![1].fragments![1].afterId).toBe(4);
    expect(result!.fragments![1].fragments![2].id).toBe(6);
    expect(result!.fragments![1].fragments![2].afterId).toBe(5);
});

it('should allow get head of node children', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.headOf(1);
    expect(result!.id).toBe(2);
});

it('should allow check tree contains node', () => {
    const instance = new FragmentTree(fragments, 1);
    let result;

    result = instance.contains(1);
    expect(result).toBe(true);
    result = instance.contains(6);
    expect(result).toBe(true);
    result = instance.contains(42);
    expect(result).toBe(false);
});

it('should allow get tail of node children', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.tailOf(1);
    expect(result!.id).toBe(3);
});

it('should allow get head of deep node children', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.headOf(3);
    expect(result!.id).toBe(4);
});

it('should allow get tail of deep node children', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.tailOf(3);
    expect(result!.id).toBe(6);
});

it('should handle get head of undefined (should return null)', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.headOf(undefined);
    expect(result).toBe(null);
});

it('should handle get tail of undefined (should return null)', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.tailOf(undefined);
    expect(result).toBe(null);
});

it('should handle get head of null (should return null)', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.headOf(null);
    expect(result).toBe(null);
});

it('should handle get tail of null (should return null)', () => {
    const instance = new FragmentTree(fragments, 1);
    const result = instance.tailOf(null);
    expect(result).toBe(null);
});

it('should allow export a list of children', () => {
    const instance = new FragmentTree(fragments);
    const result = instance.exports();

    expect(result!.length).toBe(1);
    expect(result![0].id).toBe(1);
});
