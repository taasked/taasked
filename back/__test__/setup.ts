import { FragmentType } from '../src/models/fragment';
import { OrgModelType } from '../src/models/org';
import { OrgUserModelLevel } from '../src/models/org-user';

const appModule = require('../src/server');

// @ts-ignore
global.app = appModule.default();

module.exports = async () => {
    const server = (global as any).app;

    try {
        await server.db.drop();
        server.log.info(`🔵 TEST ENV 🔵 DB dropped`);
    } catch (e) {
        console.log(e);
    }
    await server.db.sync();
    server.log.info(`🔵 TEST ENV 🔵 DB synced`);

    const user = await server.models.user.create({
        email: 'base-user@example.com',
        password: 'dznajidzanidza',
        twoFAEmail: false
    });
    server.log.info(`🔵 TEST ENV 🔵 Test user created (#${user.id})`);

    const org = await server.models.orgs.create({
        name: '',
        type: OrgModelType.Private
    });
    server.log.info(`🔵 TEST ENV 🔵 Org created: (#${org.id})`);

    const access = await server.models.team.create({
        userId: user.id,
        orgId: org.id,
        level: OrgUserModelLevel.Owner,
        personal: true
    });
    server.log.info(`🔵 TEST ENV 🔵 Access created: (#${access.id})`);

    const fragment = await server.frag.create(user.id, {
        content: 'Fragment base',
        type: FragmentType.Basic,
        archived: false,
        public: false,
        orgId: org.id,
        attributes: {}
    });
    server.log.info(`🔵 TEST ENV 🔵 Fragment created: (#${fragment.id})`);
    // @ts-ignore
    global.process.env.__PRIVATE_FRAGMENT_ID__ = fragment.id;

    const publicFragment = await server.frag.create(user.id, {
        content: 'Fragment base public',
        type: FragmentType.Basic,
        archived: false,
        public: true,
        orgId: org.id,
        attributes: {}
    });
    server.log.info(`🔵 TEST ENV 🔵 Fragment created: (#${publicFragment.id})`);
    // @ts-ignore
    global.process.env.__PUBLIC_FRAGMENT_ID__ = publicFragment.id;

    const record = await server.frag.create(user.id, {
        content: 'Fragment child public',
        type: FragmentType.Basic,
        archived: false,
        public: false,
        rootId: publicFragment.id,
        parentId: publicFragment.id,
        afterId: null,
        orgId: org.id,
        attributes: {}
    });
};

export {};
