import path from 'path';
import { HttpStatus } from '../src/tools/http-status';
import { default as orgSchema } from './schemas/organization';
import { default as schemaWithToken } from './schemas/user.token';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

let email = `${path.basename(__filename)}@example.com`;
let organization = `my-org`;
let authToken: string;
let orgId: string;

it(`should accept user registration with valid credentials (email ${email})`, () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email,
            password: 'xxx0123',
            twoFAEmail: false
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', schemaWithToken)
        .then((data: any) => {
            authToken = data.json.token;
            expect(typeof data.json.orgId).toBe('number');
        });
});

it('should allow to update an attribute', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .patch(`${SERVER_URL}/user/me`, { dark: true })
        .expect('status', HttpStatus._202_ACCEPTED);
});

it('should have updated the attribute', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._200_OK)
        .then((data: any) => {
            const json: any = data.json;
            const attrs = json.attributes;

            expect(attrs.dark).toBe(true);
        });
});

it('should allow to update again the attribute', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .patch(`${SERVER_URL}/user/me`, { dark: false })
        .expect('status', HttpStatus._202_ACCEPTED);
});

it('should have updated again the attribute', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/user/me`)
        .expect('status', HttpStatus._200_OK)
        .then((data: any) => {
            const json: any = data.json;
            const attrs = json.attributes;

            expect(attrs.dark).toBe(false);
        });
});

export {};
