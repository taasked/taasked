import path from 'path';
import { HttpStatus } from '../src/tools/http-status';
import { default as fragmentSchema } from './schemas/fragment';
import { default as userSchemaWithToken } from './schemas/user.token';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${process.env.TAASKED_SERVER_PORT}`;

let email = `${path.basename(__filename)}@example.com`;
let authToken: string;
let orgId: number;
let fragmentId: number;

it(`should accept user registration with valid credentials (email ${email})`, () => {
    return frisby
        .post(`${SERVER_URL}/auth/register`, {
            email,
            password: 'xxx0123',
            twoFAEmail: false
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', userSchemaWithToken)
        .then((data: any) => {
            authToken = data.json.token;
            expect(typeof data.json.orgId).toBe('number');
            orgId = data.json.orgId;
        });
});

it('shouldnt allow creating a new fragment without org id', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, { content: 'Hello' })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt allow creating a new fragment without content and with wrong org id', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, { orgId: 1 })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('shouldnt allow creating a new fragment with content and with wrong org id', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, { content: 'Hello', orgId: 1 })
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt allow creating a new fragment without content', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, { orgId })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow creating a new fragment with empty content', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, { orgId, content: '' })
        .expect('status', HttpStatus._201_CREATED);
});

it('should allow creating a new fragment', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, { content: 'Hello', orgId })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            fragmentId = data.json.id;
            expect(data.json.content).toBe('Hello');
        });
});

it('should allow creating a new fragment with a content', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .post(`${SERVER_URL}/fragment`, {
            content: 'Hello',
            orgId
        })
        .expect('status', HttpStatus._201_CREATED)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            fragmentId = data.json.id;
            expect(data.json.content).toBe('Hello');
        });
});

it('shouldnt allow getting a fragment user has no rights on', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/fragment/1`)
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('should allow getting a fragment', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/fragment/${fragmentId}`)
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema);
});

it('should allow updating a fragment content', () => {
    const content = 'Hello2';
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, { content })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.content).toBe(content);
        });
});

it('should allow updating a fragment content', () => {
    const content = 'Hello2';
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, { content })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.content).toBe(content);
        });
});

it("shouldnt allow unarchiving a fragment that isn't archived", () => {
    const archived = false;
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, { archived })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow archiving a fragment', () => {
    const archived = true;
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, { archived })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.archived).toBe(archived);
        });
});

it('shouldnt allow archiving a fragment that is archived', () => {
    const archived = true;
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, { archived })
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow unarchiving a fragment', () => {
    const archived = false;
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, { archived })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.archived).toBe(archived);
        });
});

it('shouldnt allow deleting a fragment that isnt archived', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .delete(`${SERVER_URL}/fragment/${fragmentId}`)
        .expect('status', HttpStatus._400_BAD_REQUEST);
});

it('should allow update fragment attributes all at once', () => {
    const content = 'Yop';
    const archived = true;

    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/${fragmentId}`, {
            content,
            archived
        })
        .expect('status', HttpStatus._200_OK)
        .expect('jsonTypesStrict', fragmentSchema)
        .then((data: any) => {
            expect(data.json.content).toBe(content);
            expect(data.json.archived).toBe(archived);
        });
});

it('should allow deleting a fragment that is archived', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .delete(`${SERVER_URL}/fragment/${fragmentId}`)
        .expect('status', HttpStatus._202_ACCEPTED);
});

it('shouldnt allow deleting a fragment that is deleted', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .delete(`${SERVER_URL}/fragment/${fragmentId}`)
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt get a fragment that is deleted', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/fragment/${fragmentId}`)
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt get a fragment with a string id', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/fragment/undefined`)
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt get a fragment children with a string id', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .get(`${SERVER_URL}/fragment/undefined/children`)
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt edit a fragment children with a string id', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .put(`${SERVER_URL}/fragment/undefined`, { content: 'Yop' })
        .expect('status', HttpStatus._404_NOT_FOUND);
});

it('shouldnt delete a fragment children with a string id', () => {
    return frisby
        .setup({
            request: { headers: { Authorization: 'Bearer ' + authToken } }
        })
        .delete(`${SERVER_URL}/fragment/undefined`)
        .expect('status', HttpStatus._404_NOT_FOUND);
});

export {};
