import { env } from '../src/env';
import { HttpStatus } from '../src/tools/http-status';

const frisby = require('frisby');
const SERVER_URL = `http://localhost:${env.TAASKED_SERVER_PORT}`;

it(`should return ${HttpStatus._202_ACCEPTED}`, () => {
    return frisby
        .get(`${SERVER_URL}/ping`)
        .expect('status', HttpStatus._202_ACCEPTED);
});
