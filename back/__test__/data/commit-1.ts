export const COMMIT_DATA = {
    url: 'https://bitbucket.org/taasked/taasked/commits/6183b368f3089cf4856ce622e3573c719eef4a60',
    sha: '6183b368f3089cf4856ce622e3573c719eef4a60',
    node_id:
        'MDY6Q29tbWl0NmRjYjA5YjViNTc4NzVmMzM0ZjYxYWViZWQ2OTVlMmU0MTkzZGI1ZQ==',
    html_url:
        'https://github.com/octocat/Hello-World/commit/6dcb09b5b57875f334f61aebed695e2e4193db5e',
    comments_url:
        'https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e/comments',
    commit: {
        url: 'https://bitbucket.org/taasked/taasked/commits/6183b368f3089cf4856ce622e3573c719eef4a60',
        author: {
            name: 'Xavier Laumonier',
            email: 'xlaumonier@gmail.com',
            date: '2011-04-14T16:00:49Z'
        },
        committer: {
            name: 'Xavier Laumonier',
            email: 'xlaumonier@gmail.com',
            date: '2011-04-14T16:00:49Z'
        },
        message:
            'See #400: commit user associated to commit fragment if known in system',
        tree: {
            url: 'https://api.github.com/repos/octocat/Hello-World/tree/6dcb09b5b57875f334f61aebed695e2e4193db5e',
            sha: '6dcb09b5b57875f334f61aebed695e2e4193db5e'
        },
        comment_count: 0
    },
    parents: [
        {
            url: 'https://api.github.com/repos/octocat/Hello-World/commits/6dcb09b5b57875f334f61aebed695e2e4193db5e',
            sha: '6dcb09b5b57875f334f61aebed695e2e4193db5e'
        }
    ]
};
