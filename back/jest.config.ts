import type { Config } from '@jest/types';

const config: Config.InitialOptions = {
    testEnvironment: 'node',
    verbose: true,
    //collectCoverage: true,
    globalSetup: '<rootDir>/__test__/setup.ts',
    globalTeardown: '<rootDir>/__test__/teardown.ts'
};

export default config;
