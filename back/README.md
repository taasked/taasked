# Twotick REST API

### Tech stack

-   Web server framework: `fastify` (+ `fastify-plugin`)
-   ORM: `sequelize` on a `postgresql` database
-   Tests: `jest` + `frisby` + `joi`

### Project hierarchy

-   `__test__`: Tests
-   `src`
    -   `@types`: Additions to Typescript definitions
        -   `fastify`: Definitions for decorators
        -   `node`: ENV vars definitions
    -   `controllers`: Called by routes, implements business rules, including advanced access rights
    -   `interfaces`: Some utility Typescript interfaces
    -   `models`: Sequelize models definitions
    -   `routes`: Fastify routes by feature: performs authentication and request validation, then transmit validated data to controllers for business logic actions
        -   `schemas`: Fastify routes input (and potentially output) validation schemas
    -   `tools`: Utility functions and classes

### About testing strategy

For now the REST API test suite is run as a global integration test.

At initialization, a basic data set (user, org, access, fragment) is inserted in DB. The created resources are later on used to test access rights.

Each test suite is then responsible to create its own context, including registering a user, creating an organization, etc... then testing a particular feature.

At the test teardown phase, the test DB is emptied (all tables dropped).

This process means that the test database is initialized only once during the setup phase, and then the test suites are run _in bands_ i.e. in serie, each one of them running onto the database state defined by the previous test suites run. Given the nature of Jest (running first previuously failing test suites), it may lead to uncontrolled test environments (e.g. some test is succesfull in a given test suites order but fails if run in another test suites order). However, it has the benefit to run tests onto a somewhat fuzzy database state, potentially detecting issues that would not have been detected by insufficient test coverage onto a particular feature. Conclusion is that there is definitely room for improvement here.

### Docker

Build:

```
docker build . -t taasked-back
```

Run:

```
docker run --rm --name=svelte-docker -p 9000:9001 taasked-back
```
