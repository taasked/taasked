# taasked

Blazing fast, low friction, open source todo lists and markdown documents.

**This is a Work In Progress project.**

## Features

-   Todo lists, markdown documents
-   Two-Factor auth (email only)

## Server side tech stack

-   Full typescript
-   Fastify (+cookie, cors, csrf, helmet...)
-   Full input validation using Type.js
-   Sequelize ORM on PostgreSQL database
-   Tests using Jest + frisby + Joi (w/ test isolation, i.e. clean DB / app state for each test suite)
-   Emails using ejs templates

See `/back/README.md` for specific informations

## Frontend tech stack

-   Full typescript
-   Svelte + sveltekit framework
    -   SSR + CSR
    -   PWA for offline use
-   Tailwind CSS

See `/front/README.md` for specific informations.

## Good practices

-   Separation of concerns
    -   Separated front / back for easier scaling later on
    -   On the back end:
        -   MVC like structure
        -   Multiple validation layers
            -   Thanks to Typescript during development & build
            -   On incoming HTTP request level w/ Type.js & at the ORM level w/ Sequelize at runtime
    -   On the front end:
        -   Heavy componentization
        -   SSR where it makes sense (ongoing work)
        -   guards/interfaces/stores/utils
-   Full REST API integration test coverage
-   Full internalization (i18n/l10n)
-   Env based configuration
-   Dockerization (ongoing work)

## Installation

The back and front ends are configured to work with a local `taasked.test` domain name. Requires [ergo](https://github.com/cristianoliveira/ergo) for running the provided local proxy script in `services/local-proxy.sh`. Otherwise local proxy has to be configured depending on your environment.

1. Install node dependencies in `back/`
2. Install node dependencies in `front/`
3. Create `.env` files in back and front (see [Configuration](#configuration) section below)
4. Start database service (`cd services/database && docker-compose up -d`)
5. Start local proxy (`cd services && ./local-proxy.sh`)
6. Start back end (`cd back && npm run dev`)
7. Start front end (`cd front && npm run dev`)
8. Visit `http://taasked.test`

## Configuration

```sh
NODE_ENV=local|test|production|...

# Debug mode
TAASKED_DEBUG=true
# Logs SQL requests
TAASKED_DEBUG_SQL=false

# Database host
TAASKED_DB_HOST=localhost
# Database port
TAASKED_DB_PORT=5432
# Database username
TAASKED_DB_USER=postgres
# Database password
TAASKED_DB_PASS=postgres
# Database name
TAASKED_DB_NAME=taasked

# Fastify REST API server port
TAASKED_SERVER_PORT=9000

# Private (httpOnly) cookie identifier
TAASKED_PRIVATE_COOKIE_NAME=tprc
# Public cookie identifier
TAASKED_PUBLIC_COOKIE_NAME=tpuc

# Secret key used for session token generation
TAASKED_SESSION_SECRET=fKYWrrx9c0bdwxKcM33OCC72h8V7O01cnuwbSjnWE7HN85K2afjfqhrC1BvC
# Secret key used for auth token generation
TAASKED_TOKEN_SECRET=l1uZqSeugG3cOpt79i9flY5vF7uxUsNs7bGmPeVWGShJZU1OVAAd5VsgC8Qr
# Secret key used for cookir token generation
TAASKED_COOKIE_SECRET=3w9HZuHrMCVJTQrFonl8tuEg8EHi4bipuJtPtg9B

# Email communications FROM address
TAASKED_FROM_EMAIL=xlaumonier@gmail.com

# Third party

# Sendgri API key
TAASKED_SENDGRID_API_KEY=GWuf5DPS9hbCSCWvlKNg9AZVCMBUvcXHmP4qxur5
```
